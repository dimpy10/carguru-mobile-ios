//
//  AppDelegate.m
//  Real Ads
//
//  Created by De Papier on 4/21/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeVC.h"
#import "RevealViewController.h"
#import "SWRevealViewController.h"
#import "PayPalMobile.h"
#import "Global.h"

@interface AppDelegate ()
@end

@implementation AppDelegate
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"LaunchedOnce"])
    {
        [Util setObject:@"" forKey:KEY_COUNTRY];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [Util setObject:@"" forKey:@"COUNTRYID"];
        [[NSUserDefaults standardUserDefaults] setInteger : 0 forKey:@"SelectedButton"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"LaunchedOnce"];
        [[NSUserDefaults standardUserDefaults] setObject : @"" forKey:@"SelectedID"];
        [[NSUserDefaults standardUserDefaults] setObject : [@"Select Advertisement" localized] forKey:@"SelectedItem"];




    }
   
    NSArray *value = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    NSLog(@"Value = %@", value.firstObject);
  
    if([value.firstObject isEqualToString:@"en"]) {
    }else if([value.firstObject isEqualToString:@"ar"]) {
    }else {
        NSArray *array = [NSArray arrayWithObject:@"en"];
        [[NSUserDefaults standardUserDefaults] setObject:array forKey:@"AppleLanguages"];
    }
    

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    gArrBookMark = [[NSMutableArray alloc]init];
    [PayPalMobile initializeWithClientIdsForEnvironments:@{
                                                           PayPalEnvironmentSandbox : kPayPalClientId}];

    [self performSelectorInBackground:@selector(getDataSetting) withObject:nil];
     NSString* thumbLogin_already = [Util stringForKey:KEY_LOGIN];
    login_already = [Util stringForKey:KEY_LOGIN];
    if ([thumbLogin_already isEqualToString:@"1"]) {
        gUser = [Util getObjectWithDecode:KEY_SAVE_gUSER];
        [ModelManager loginbyDefaultWithUserName:gUser.usUserName password:[Util objectForKey:@"password"] withSuccess:^(id userInfo) {
            gUser = userInfo;
            [self performSelectorInBackground:@selector(getArrBookMarks) withObject:nil];
        } failure:^(NSString *err) {
        }];
    }else if([thumbLogin_already isEqualToString:@"2"]){
        gUser = [Util getObjectWithDecode:KEY_SAVE_gUSER];
        [ModelManager loginFbWithName:gUser.usUserName email:gUser.usEmail fbId:gUser.usFbId andImage:gUser.usImage withsuccess:^(id userInfo) {
            [self performSelectorInBackground:@selector(getArrBookMarks) withObject:nil];
        } failure:^(NSString *err) {
    }];
    }
    
    HomeVC *frontController = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
    RevealViewController *rearViewController = [[RevealViewController alloc] initWithNibName:@"RevealViewController" bundle:nil];
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc] initWithRearViewController:rearViewController frontViewController:frontController];

    mainRevealController.rearViewRevealWidth = self.window.frame.size.width - 50;
    mainRevealController.rearViewRevealOverdraw = 0;
    mainRevealController.bounceBackOnOverdraw = NO;
    mainRevealController.stableDragOnOverdraw = YES;
    [mainRevealController setFrontViewPosition:FrontViewPositionLeft];
    
    UINavigationController* naviVC = [[UINavigationController alloc] initWithRootViewController:mainRevealController];
    [naviVC setNavigationBarHidden:YES animated:NO];
    self.window.rootViewController = naviVC;
    [self.window makeKeyAndVisible];
    return YES;
    
}

-(void)getDataSetting{
    [ModelManager getSettingAppWithSuccess:^(NSDictionary *dicSuccess) {
        strTerm = dicSuccess[@"termCondition"];
        strPrivacy = dicSuccess[@"privacy"];
        strFAQ = dicSuccess[@"faq"];
    } failure:^(NSString *err) {
        
    }];
    
}

-(void)getArrBookMarks{
    if ([login_already isEqualToString:@"1"] || [ login_already isEqualToString:@"2"]) {
        [ModelManager getBookmarksAdsByUserId:gUser.usId andPage:[NSString stringWithFormat:@""] sortType:@"" sortBy:@"" withSuccess:^(NSDictionary *dicReturn) {
        } failure:^(NSString *error) {
            
        }];
    }
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}



- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
    application.applicationIconBadgeNumber = 0;
}
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {

        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:sourceApplication
                                                           annotation:annotation];

    
}

@end
