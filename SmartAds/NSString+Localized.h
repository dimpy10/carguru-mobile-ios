//
//  NSString+Localized.h
//  SmartAds
//
//  Created by Rakesh Kumar on 01/07/18.
//  Copyright © 2018 Mr Lemon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (localized)
-(NSString *)localized;
-(BOOL)isBlank;
-(NSString *)stringByStrippingWhitespace;
@end
