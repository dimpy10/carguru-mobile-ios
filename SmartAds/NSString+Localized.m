//
//  NSString+Localized.m
//  SmartAds
//
//  Created by Rakesh Kumar on 01/07/18.
//  Copyright © 2018 Mr Lemon. All rights reserved.
//

#import "NSString+Localized.h"

@implementation NSString (localized)

- (NSString *)localized {
    NSArray *value = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    NSLog(@"Value = %@", value.firstObject);
    NSString *path = [[NSBundle mainBundle] pathForResource: value.firstObject ofType:@"lproj"];
    NSBundle *bundle = [NSBundle bundleWithPath:path];
    return [bundle localizedStringForKey:self value:@"" table:nil];
    //NSLocalizedStringWithDefaultValue(self, @"", bundle, @"", @"");
}

-(BOOL)isBlank {
    if([[self stringByStrippingWhitespace] isEqualToString:@""])
        return YES;
    return NO;
}

-(NSString *)stringByStrippingWhitespace {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}


@end
