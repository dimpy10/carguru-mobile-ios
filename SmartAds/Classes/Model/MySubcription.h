//
//  MySubcription.h
//  Real Ads
//
//  Created by Mac on 5/18/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MySubcription : NSObject
@property (strong,nonatomic) NSString *duration;
@property (strong,nonatomic) NSString *endDate;
@property (strong,nonatomic) NSString *adsId;
@property (strong,nonatomic) NSString *payment;
@property (strong,nonatomic) NSString *paidDate;
@property (strong,nonatomic) NSString *value;
@property (strong,nonatomic) NSString *image;
@property (strong,nonatomic) NSString *titleAds;

@end
