//
//  User.m
//  Real Ads
//
//  Created by De Papier on 4/22/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "User.h"
@implementation User
@synthesize usId;
@synthesize usUserName;
@synthesize usEmail;
@synthesize usName;
@synthesize usPhone;
@synthesize usAddress;
@synthesize usSkype;
@synthesize usImage;
@synthesize usType ;
@synthesize usSex;
@synthesize usIsverified;
@synthesize usWebsite;
@synthesize usIndividual;
@synthesize usFbId;
@synthesize usDateCreated;
@synthesize usAdsNumber;

- (id) initWithCoder: (NSCoder *)coder
{
    self = [super init];
    if (self != nil)
    {
        self.usId = [coder decodeObjectForKey:@"estateId"];
        self.usUserName = [coder decodeObjectForKey:@"image"];
        self.usEmail = [coder decodeObjectForKey:@"title"];
        self.usName = [coder decodeObjectForKey:@"type"];
        self.usPhone = [coder decodeObjectForKey:@"city"];
        self.usAddress = [coder decodeObjectForKey:@"estateDescription"];
        self.usSkype = [coder decodeObjectForKey:@"condition"];
        self.usImage = [coder decodeObjectForKey:@"price"];
        self.usType = [coder decodeObjectForKey:@"address"];
        self.usSex = [coder decodeObjectForKey:@"latitude"];
        self.usIsverified = [coder decodeObjectForKey:@"longitude"];
        self.usWebsite = [coder decodeObjectForKey:@"forRent"];
        self.usIndividual = [coder decodeObjectForKey:@"forSale"];
        self.usFbId = [coder decodeObjectForKey:@"status"];
        self.usDateCreated = [coder decodeObjectForKey:@"bedRoom"];
        self.usAdsNumber = [coder decodeObjectForKey:@"bathRoom"];

    }
    return self;
}

- (void) encodeWithCoder: (NSCoder *)coder
{
    //[coder encodeObject:estateId forKey:@"locationId"];
    [coder encodeObject:usId forKey:@"estateId"];
    [coder encodeObject:usUserName forKey:@"image"];
    [coder encodeObject:usEmail forKey:@"title"];
    [coder encodeObject:usName forKey:@"type"];
    [coder encodeObject:usPhone forKey:@"city"];
    [coder encodeObject:usAddress forKey:@"estateDescription"];
    [coder encodeObject:usSkype forKey:@"condition"];
    [coder encodeObject:usImage forKey:@"price"];
    [coder encodeObject:usType forKey:@"address"];
    [coder encodeObject:usSex forKey:@"latitude"];
    [coder encodeObject:usIsverified forKey:@"longitude"];
    [coder encodeObject:usWebsite forKey:@"forRent"];
    [coder encodeObject:usIndividual forKey:@"forSale"];
    [coder encodeObject:usFbId forKey:@"status"];
    [coder encodeObject:usDateCreated forKey:@"bedRoom"];
    [coder encodeObject:usAdsNumber forKey:@"bathRoom"];

}



@end
