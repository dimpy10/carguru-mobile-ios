//
//  Ads.m
//  Real Ads
//
//  Created by Hicom Solutions on 1/24/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "Ads.h"

@implementation Ads
//@synthesize accountId;
//@synthesize image;
//@synthesize title;
//@synthesize categoryId;
//@synthesize adsDescription;
//@synthesize condition;
//@synthesize price;
//@synthesize country;
//@synthesize address;
//@synthesize year;
//@synthesize forRent;
//@synthesize forSale;
//@synthesize status;
//@synthesize longitude;
//@synthesize latitude;
//@synthesize dateCreated;
//@synthesize start_date;
//@synthesize end_date;
//@synthesize gallery;
//@synthesize hostAgent;
//@synthesize endDate;
//@synthesize paidDate;
//@synthesize value;
//@synthesize duration;
//@synthesize isAvailable;
//- (id) initWithCoder: (NSCoder *)coder
//{
//     self = [super init];
//    if (self != nil)
//    {
//        self.accountId = [coder decodeObjectForKey:@"adsId"];
//        self.image = [coder decodeObjectForKey:@"image"];
//        self.title = [coder decodeObjectForKey:@"title"];
//        self.categoryId = [coder decodeObjectForKey:@"type"];
//        self.country = [coder decodeObjectForKey:@"city"];
//        self.adsDescription = [coder decodeObjectForKey:@"AdsDescription"];
//        self.condition = [coder decodeObjectForKey:@"condition"];
//        self.price = [coder decodeObjectForKey:@"price"];
//        self.address = [coder decodeObjectForKey:@"address"];
//        self.latitude = [coder decodeObjectForKey:@"latitude"];
//        self.longitude = [coder decodeObjectForKey:@"longitude"];
//        self.forRent = [coder decodeObjectForKey:@"forRent"];
//        self.forSale = [coder decodeObjectForKey:@"forSale"];
//        self.status = [coder decodeObjectForKey:@"status"];
//        self.dateCreated = [coder decodeObjectForKey:@"dateCreated"];
//        self.start_date = [coder decodeObjectForKey:@"start_date"];
//        self.end_date = [coder decodeObjectForKey:@"end_date"];
//        self.gallery = [coder decodeObjectForKey:@"gallery"];
//        self.hostAgent = [coder decodeObjectForKey:@"hostAgent"];
//        self.endDate = [coder decodeObjectForKey:@"endDate"];
//        self.paidDate = [coder decodeObjectForKey:@"paidDate"];
//        self.value = [coder decodeObjectForKey:@"value"];
//        self.duration = [coder decodeObjectForKey:@"duration"];
//        self.isAvailable = [coder decodeObjectForKey:@"isAvailable"];
//    }
//    return self;
//}
//
//- (void) encodeWithCoder: (NSCoder *)coder
//{
//    //[coder encodeObject:adsId forKey:@"locationId"];
//    [coder encodeObject:accountId forKey:@"adsId"];
//    [coder encodeObject:image forKey:@"image"];
//    [coder encodeObject:title forKey:@"title"];
//    [coder encodeObject:categoryId forKey:@"type"];
//    [coder encodeObject:country forKey:@"city"];
//    [coder encodeObject:adsDescription forKey:@"AdsDescription"];
//    [coder encodeObject:condition forKey:@"condition"];
//    [coder encodeObject:price forKey:@"price"];
//    [coder encodeObject:address forKey:@"address"];
//    [coder encodeObject:latitude forKey:@"latitude"];
//    [coder encodeObject:longitude forKey:@"longitude"];
//    [coder encodeObject:forRent forKey:@"forRent"];
//    [coder encodeObject:forSale forKey:@"forSale"];
//    [coder encodeObject:status forKey:@"status"];
//    [coder encodeObject:dateCreated forKey:@"dateCreated"];
//    [coder encodeObject:start_date forKey:@"start_date"];
//    [coder encodeObject:end_date forKey:@"end_date"];
//    [coder encodeObject:gallery forKey:@"gallery"];
//    [coder encodeObject:hostAgent forKey:@"hostAgent"];
//    
//    [coder encodeObject:endDate forKey:@"endDate"];
//    [coder encodeObject:paidDate forKey:@"paidDate"];
//    [coder encodeObject:value forKey:@"value"];
//    [coder encodeObject:duration forKey:@"duration"];
//    [coder encodeObject:isAvailable forKey:@"isAvailable"];
//}
@end
