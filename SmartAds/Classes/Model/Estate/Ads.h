//
//  Ads.h
//  Real Ads
//
//  Created by Hicom Solutions on 1/24/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Ads : NSObject

@property (strong,nonatomic) NSString *adsId;
@property (strong,nonatomic) NSString *adsImage;
@property (strong,nonatomic) NSString *adsTitle;
@property (strong,nonatomic) NSString *adsDescription;
@property (strong,nonatomic) NSString *adsPrice;
@property (strong,nonatomic) NSString *adsPriceValue;
@property (strong,nonatomic) NSString *adsCurrencyCode;
@property (strong,nonatomic) NSString *adsPriceUnit;
@property (strong,nonatomic) NSString *adsCity;
@property (strong,nonatomic) NSString *adsForRent;
@property (strong,nonatomic) NSString *adsForSale;
@property (strong,nonatomic) NSString *adsStatus;
@property (strong,nonatomic) NSString *adsAccountId;
@property (strong,nonatomic) NSString *adsAccountName;
@property (strong,nonatomic) NSString *adsAccountDateCreated;
@property (strong,nonatomic) NSString *adsAccountType;
@property (strong,nonatomic) NSString *adsAccountContact;
@property (strong,nonatomic) NSString *adsCategory;
@property (strong,nonatomic) NSString *adsSub;
@property (strong,nonatomic) NSString *adsCityId;
@property (strong,nonatomic) NSString *adsCategoryId;
@property (strong,nonatomic) NSString *adsSubCatId;
@property (strong,nonatomic) NSString *adsDateCreated;
@property (strong,nonatomic) NSString *adsIsAvailable;
@property (strong,nonatomic) NSString *adsViews;
@property (strong,nonatomic) NSMutableArray *adsGallery;
@property (strong,nonatomic) NSString *adsIsFeatured;
@property (strong, nonatomic) NSString *estimate_price;
@property (strong,nonatomic) NSString *adsCountry;
@property (strong,nonatomic) NSString *adsModel;
@property (strong,nonatomic) NSString *adsBrand;
@property (strong,nonatomic) NSString *adsMileage;
@property (strong,nonatomic) NSString *adsYear;

@property (strong,nonatomic) NSString *adsTransmission;



@end
