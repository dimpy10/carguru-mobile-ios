//
//  Messages.h
//  Real Ads
//
//  Created by Mac on 5/14/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Messages : NSObject
@property (strong,nonatomic) NSString *content;
@property (strong,nonatomic) NSString *dateCreated;
@property (strong,nonatomic) NSString *email;
@property (strong,nonatomic) NSString *adsId;
@property (strong,nonatomic) NSString *hostagentId;
@property (strong,nonatomic) NSString *MessageId;
@property (strong,nonatomic) NSString *name;
@property (strong,nonatomic) NSString *phone;
@property (strong,nonatomic) NSString *status;
@property (strong,nonatomic) NSString *title;
@property (strong,nonatomic) NSString *image;
@property (strong,nonatomic) NSString *bookMarkMess;
@end
