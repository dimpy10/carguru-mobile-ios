//
//  CityObj.h
//  Real Estate
//
//  Created by Hicom on 3/18/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CityObj : NSObject
@property (strong, nonatomic) NSString *cityId;
@property (strong, nonatomic) NSString *cityName;
@property (strong, nonatomic) NSString *cityStatus;
@property (strong, nonatomic) NSString *cityCounrncy;
@property (strong, nonatomic) NSString *cityCurrencyCode;
@property (strong, nonatomic) NSString *cityDateCreated;

@end
