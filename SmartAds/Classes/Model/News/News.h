//
//  News.h
//  Real Ads
//
//  Created by De Papier on 4/15/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface News : NSObject
@property (strong,nonatomic) NSString *newsId;
@property (strong,nonatomic) NSString *newsTitle;
@property (strong,nonatomic) NSString *newsImage;
@property (strong,nonatomic) NSString *newsDescription;
@property (strong,nonatomic) NSString *newsUrl;
@property (strong,nonatomic) NSString *newsDateCreated;
@end
