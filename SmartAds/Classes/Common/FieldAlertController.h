//
//  FieldAlertController.h
//  SmartAds
//
//  Created by Rakesh Kumar on 18/04/18.
//  Copyright © 2018 Mr Lemon. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FieldAlertDelegate <NSObject>

-(void)didEnterValue:(NSString*)value forOption:(NSString*)option;

@end


@interface FieldAlertController : UIViewController
@property (strong, nonatomic) NSString *titleString;
@property (strong, nonatomic) NSString *textTitle;
@property (strong, nonatomic) NSString *index;
@property (weak, nonatomic) id<FieldAlertDelegate> delegate;
@property (nonatomic, strong) NSString *searchOption;

- (void)presentInParentViewController:(UIViewController *)parentViewController;
- (void)dismissFromParentViewController;

@end
