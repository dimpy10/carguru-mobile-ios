//
//  FieldAlertController.m
//  SmartAds
//
//  Created by Rakesh Kumar on 18/04/18.
//  Copyright © 2018 Mr Lemon. All rights reserved.
//

#import "FieldAlertController.h"
#import "UIView+Toast.h"


@interface FieldAlertController ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;

@end

@implementation FieldAlertController


- (void)viewDidLoad {
    [super viewDidLoad];
    _titleLabel.text = _titleString;
    if ([_textTitle  isEqualToString:@"Years"] || [_textTitle  isEqualToString:@"Price"] || [_textTitle  isEqualToString:@"Mileage"] || [_textTitle  isEqualToString:@"From Years"] || [_textTitle  isEqualToString:@"To Years"]){
    _textField.placeholder = _textTitle;
    }else{
    _textField.text = _textTitle;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveAction:(id)sender {
    if (_textField.text.length>0){
        [self.delegate didEnterValue:_textField.text forOption:self.searchOption];
        [self dismissFromParentViewController];
    }else{
        [self.view makeToast:@"Please fill The Value"];
   }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)presentInParentViewController:(UIViewController *)parentViewController{
    
    self.view.frame = parentViewController.view.bounds;
    [parentViewController.view addSubview:self.view];
    [parentViewController addChildViewController:self];
    
}

-(void)dismissFromParentViewController{
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    
}

@end
