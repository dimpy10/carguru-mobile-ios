//
//  NewsTableViewCell.m
//  Real Ads
//
//  Created by De Papier on 4/10/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "NewsTableViewCell.h"

@implementation NewsTableViewCell

- (void)awakeFromNib {
    // Initialization code
     _imageNew.contentMode = UIViewContentModeScaleAspectFill;
    _imageNew.clipsToBounds = YES;
    _ViewCell.layer.cornerRadius = 6;
    //_ViewCell.layer.shadowColor = [[UIColor blackColor] CGColor];
    //_ViewCell.layer.shadowOffset = CGSizeMake(0, 1);
    //_ViewCell.layer.shadowOpacity = 0.5;
    self.ViewCell.layer.borderColor = COLOR_DEVIDER.CGColor;;
    self.ViewCell.layer.borderWidth = 0.5;

    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
