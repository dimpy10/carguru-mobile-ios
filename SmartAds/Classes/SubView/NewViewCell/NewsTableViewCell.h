//
//  NewsTableViewCell.h
//  Real Ads
//
//  Created by De Papier on 4/10/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;


@property (strong, nonatomic) IBOutlet UIImageView *imageNew;
@property (strong, nonatomic) IBOutlet UIView *ViewCell;
@property (weak, nonatomic) IBOutlet UILabel *lblPosted;


@end
