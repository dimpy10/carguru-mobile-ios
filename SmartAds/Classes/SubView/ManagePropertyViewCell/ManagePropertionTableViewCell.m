//
//  ManagePropertionTableViewCell.m
//  Real Ads
//
//  Created by hieund@wirezy on 4/22/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "ManagePropertionTableViewCell.h"

@implementation ManagePropertionTableViewCell

- (void)awakeFromNib {
     _image.contentMode = UIViewContentModeScaleAspectFill;
    _image.clipsToBounds = YES;
    // Initialization code
    self.ViewCell.layer.cornerRadius = 6;
    self.ViewCell.clipsToBounds = YES;
    self.ViewCell.layer.borderColor = COLOR_DEVIDER.CGColor;;
    self.ViewCell.layer.borderWidth = 0.5;
    [self layoutView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)layoutView
{

   
}
@end
