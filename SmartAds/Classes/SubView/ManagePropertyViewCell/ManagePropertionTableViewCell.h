//
//  ManagePropertionTableViewCell.h
//  Real Ads
//
//  Created by hieund@wirezy on 4/22/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBAutoScrollLabel.h"
@interface ManagePropertionTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *ViewCell;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *descriptionScrollLabel;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *detailScrollLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressScrollLabel;
@property (weak, nonatomic) IBOutlet UILabel *expiredScrollLabel;
@property (weak, nonatomic) IBOutlet UIImageView *ImgBookMark;

@end
