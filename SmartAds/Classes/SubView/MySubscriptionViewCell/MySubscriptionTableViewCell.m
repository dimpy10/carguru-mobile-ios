//
//  MySubscriptionTableViewCell.m
//  Real Ads
//
//  Created by hieund@wirezy on 4/22/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "MySubscriptionTableViewCell.h"

@implementation MySubscriptionTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
     _imgCell.contentMode = UIViewContentModeScaleAspectFill;
    _imgCell.clipsToBounds = YES;
    self.ViewCell.layer.cornerRadius = 6;
    self.ViewCell.clipsToBounds = YES;
    self.ViewCell.layer.borderColor = COLOR_DEVIDER.CGColor;;
    self.ViewCell.layer.borderWidth = 0.5;
    
    self.duration.text = [@"Duration" localized];
    self.totalSpent.text = [@"Total Spent" localized];
    
    [self layoutView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)layoutView
{

    UIColor *color_Dack = [UIColor darkGrayColor];
    
    self.descriptionScrollLabel.textColor = [UIColor darkTextColor];
 
    self.fromScrollLabel.textColor = color_Dack;
    self.fromScrollLabel.font = [UIFont systemFontOfSize:14];
    self.reachScrollLabel.textColor = color_Dack;
    self.reachScrollLabel.font = [UIFont systemFontOfSize:14];
    self.spentScrollLabel.textColor = color_Dack;
    self.spentScrollLabel.font = [UIFont systemFontOfSize:14];
    
}
@end
