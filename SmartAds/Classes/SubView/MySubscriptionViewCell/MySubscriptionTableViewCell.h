//
//  MySubscriptionTableViewCell.h
//  Real Ads
//
//  Created by hieund@wirezy on 4/22/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBAutoScrollLabel.h"
@interface MySubscriptionTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *ViewCell;
@property (weak, nonatomic) IBOutlet UIImageView *imgCell;
@property (weak, nonatomic) IBOutlet UILabel *descriptionScrollLabel;
@property (weak, nonatomic) IBOutlet UILabel *fromScrollLabel;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *reachScrollLabel;
@property (weak, nonatomic) IBOutlet CBAutoScrollLabel *spentScrollLabel;
    @property (weak, nonatomic) IBOutlet UILabel *duration;
    @property (weak, nonatomic) IBOutlet UILabel *totalSpent;
    
    
    
    
@end
