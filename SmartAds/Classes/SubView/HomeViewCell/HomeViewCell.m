//
//  HomeViewCell.m
//  Real Ads
//
//  Created by Hicom Solutions on 1/24/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "HomeViewCell.h"

@implementation HomeViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _lblPrice.backgroundColor = COLOR_BTN_SMALL;
    _imageAds.contentMode = UIViewContentModeScaleAspectFill;
    _imageAds.clipsToBounds = YES;
    self.imgBackGround.layer.cornerRadius = 6;
    self.imgBackGround.clipsToBounds = YES;
    //self.imgBackGround.layer.shadowColor = [UIColor clearColor].CGColor;
    //self.imgBackGround.layer.borderColor = [UIColor blackColor].CGColor;;
    //self.imgBackGround.layer.borderWidth = 0.5;
//    self.imgBackGround.layer.shadowOffset = CGSizeMake(0 ,1);
//    self.imgBackGround.layer.shadowOpacity = 0.5;
    
    self.lblTitle.font = [UIFont fontWithName:@"OpenSans-Semibold" size:18];
    self.uvConten.layer.cornerRadius = 6;
    self.uvConten.clipsToBounds = YES;
    self.uvConten.layer.borderColor = COLOR_DEVIDER.CGColor;;
    self.uvConten.layer.borderWidth = 0.5;
    
    self.posted.text = [@"Posted" localized];
    self.category.text = [@"Category" localized];
    self.actualPrice.text = [@"Actual Price" localized];
    self.subCategory.text = [@"Sub Category" localized];
    self.estimatePrice.text = [@"Estimate Price" localized];
    self.Location.text = [@"Location" localized];
    self.mileage.text = [@"Mileage" localized];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
