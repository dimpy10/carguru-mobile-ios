//
//  HomeViewCell.h
//  Real Ads
//
//  Created by Hicom Solutions on 1/24/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBAutoScrollLabel.h"
@interface HomeViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgBackGround;
@property (weak, nonatomic) IBOutlet UILabel *lblSubCat;

@property (strong, nonatomic) IBOutlet UIView *uvConten;
@property (strong, nonatomic) IBOutlet CBAutoScrollLabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblCategory;
@property (strong, nonatomic) IBOutlet UILabel *lblRentOrSale;
@property (strong, nonatomic) IBOutlet UILabel *lblCity;
@property (strong, nonatomic) IBOutlet UILabel *lblPosted;
@property (strong, nonatomic) IBOutlet UILabel *lblAvaiable;
@property (strong, nonatomic) IBOutlet UILabel *lblPrice;
@property (strong, nonatomic) IBOutlet UIImageView *imageAds;
//@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
//@property (weak, nonatomic) IBOutlet UILabel *lblStatusValue;
@property (weak, nonatomic) IBOutlet UIImageView *isFeatured;
@property (weak, nonatomic) IBOutlet UILabel *estimatePriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *actualPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *brandLabel;
@property (weak, nonatomic) IBOutlet UILabel *modelLabel;
    
    @property (weak, nonatomic) IBOutlet UILabel *posted;
    @property (weak, nonatomic) IBOutlet UILabel *category;
    @property (weak, nonatomic) IBOutlet UILabel *actualPrice;
    @property (weak, nonatomic) IBOutlet UILabel *subCategory;
    @property (weak, nonatomic) IBOutlet UILabel *estimatePrice;
    @property (weak, nonatomic) IBOutlet UILabel *Location;
    
@property (weak, nonatomic) IBOutlet UILabel *mileage;
@property (weak, nonatomic) IBOutlet UILabel *mileageLabel;

    
    
    

@end
