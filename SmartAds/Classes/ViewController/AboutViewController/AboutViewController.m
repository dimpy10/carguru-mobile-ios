//
//  AboutViewController.m
//  Real Ads
//
//  Created by De Papier on 4/13/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imgNaviBg;
@property (weak, nonatomic) IBOutlet UILabel *lbl1;
@property (weak, nonatomic) IBOutlet UILabel *lbl2;
    @property (weak, nonatomic) IBOutlet UILabel *aboutLabel;
    @property (weak, nonatomic) IBOutlet UILabel *appName;
    @property (weak, nonatomic) IBOutlet UILabel *details;
    @property (weak, nonatomic) IBOutlet UILabel *homePage;
    
@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
        _imgNaviBg.backgroundColor = COLOR_DARK_PR_MARY;
    // Do any additional setup after loading the view from its nib.
    [self setRevealBtn];
    self.view.backgroundColor = [UIColor whiteColor];
    self.btnShare.hidden = YES;
    self.btnRate.hidden = YES;
    
    self.aboutLabel.text = [@"About" localized];
    self.details.text = [NSString stringWithFormat:@"%@\n%@", [@"Provides Android, iOS, Web Source Codes and Development Services." localized],   [@"Highly Committed to Quality Products and Excellent Services." localized] ];
    self.homePage.text = [NSString stringWithFormat:@"%@ %@", [@"Home page" localized], @": http://hicomsolutions.com"];
}

-(void)setRevealBtn{
    SWRevealViewController *revealController = self.revealViewController;
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    [_revealBtn addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
}
- (IBAction)gotoWeb:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://hicomsolutions.com.au/"]];
}

- (IBAction)onShare:(id)sender {
    NSString *textToShare = @"Get our Solutions Completely & Ready-to-go Applications";
    NSURL *myWebsite = [NSURL URLWithString:@"http://hicomsolutions.com.au/"];
    
    NSArray *objectsToShare = @[textToShare, myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}

- (IBAction)onRate:(id)sender {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"neverRate"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=573753324"]]];
//    BOOL neverRate = [[NSUserDefaults standardUserDefaults] boolForKey:@"neverRate"];
//    
//    if (neverRate != YES) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please rate <APP NAME>!"
//                                                        message:@"If you like it we'd like to know."
//                                                       delegate:self
//                                              cancelButtonTitle:nil
//                                              otherButtonTitles:@"Rate It Now", @"Remind Me Later", @"No, Thanks", nil];
//        alert.delegate = self;
//        [alert show];
//    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"neverRate"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=573753324"]]];
    }
    
    else if (buttonIndex == 1) {
        
    }
    
    else if (buttonIndex == 2) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"neverRate"];
    }
}

@end
