//
//  DetailAdsCollectionCellImg.h
//  Real Estate
//
//  Created by Hicom on 3/21/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailAdsCollectionCellImg : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgInCell;

@end
