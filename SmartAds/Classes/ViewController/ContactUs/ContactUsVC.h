//
//  SettingViewController.h
//  Real Ads
//
//  Created by Mac on 5/13/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "JVFloatLabeledTextField.h"
#import "JVFloatLabeledTextView.h"

@interface ContactUsVC : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfName;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfEmail;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfType;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfSubject;

@property (weak, nonatomic) IBOutlet JVFloatLabeledTextView *tvContent;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) NSString *strTitle;
@property (strong, nonatomic) NSString *strContent;

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *tpKeyboard;
@property (weak, nonatomic) IBOutlet UILabel *lblContent;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;

@end
