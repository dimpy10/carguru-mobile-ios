//
//  SettingViewController.m
//  Real Ads
//
//  Created by Mac on 5/13/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "ContactUsVC.h"
#import "UIView+Toast.h"
#import "Validator.h"
#import "JVFloatLabeledTextField.h"

@interface ContactUsVC (){
    NSArray *keyArray,*objectArray;
}
    @property (weak, nonatomic) IBOutlet UILabel *headerLabel;
    @property (weak, nonatomic) IBOutlet UIImageView *imgNaviBg;
@end

@implementation ContactUsVC

- (void)viewDidLoad
{
    [super viewDidLoad];
        _imgNaviBg.backgroundColor = COLOR_DARK_PR_MARY;
    self.headerLabel.text = [@"Contact Us" localized];
   self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view from its nib.
//    [_tpKeyboard contentSizeToFit];
    [self.btnSubmit setTitle:[@"SUBMIT" localized] forState:UIControlStateNormal];
    
    self.tfType.placeholder = [@"Type" localized];
    self.tfName.placeholder = [NSString stringWithFormat:@"%@(*)",[@"Name" localized]];
    self.tfEmail.placeholder = [NSString stringWithFormat:@"%@(*)",[@"Email" localized]];
    self.tfSubject.placeholder = [NSString stringWithFormat:@"%@(*)",[@"Subject" localized]];
    self.tvContent.placeholder = [NSString stringWithFormat:@"%@(*)",[@"Reason" localized]];
    [self setupLayout];
    if ([_strTitle isEqualToString:CONTACT_REPORT_SELLER]|| [_strTitle isEqualToString:CONTACT_REPORT_ADS]) {
        [self setupBackButton];
        self.tfSubject.text = _strContent;
        self.tfSubject.enabled = NO;
        _tfType.text = [@"Report" localized] ;
        self.lblContent.text = [NSString stringWithFormat:@"%@(*)", [@"Reason" localized] ];
    }else {
        _tfType.text = [@"Contact us" localized] ;
        
        [self setRevealBtn];
    }
    _tfType.enabled = NO;
}
//-(void)viewDidLayoutSubviews{
//    _tpKeyboard.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height+100);
//}
-(void)setupBackButton{
    [_btnBack setBackgroundImage:[UIImage imageNamed:@"ic_back.png"] forState:UIControlStateNormal];
    [_btnBack addTarget:self action:@selector(btnBackClick:) forControlEvents:UIControlEventTouchUpInside];
}
-(void)setRevealBtn{
    [self.view endEditing:YES];
    SWRevealViewController *revealController = self.revealViewController;
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    [_btnBack addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
}
-(void)setupLayout{
    _lblTitle.text = _strTitle;

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)btnBackClick:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnSubmitClick:(id)sender {
    [self.view endEditing:YES];
    if (_tfName.text.length == 0) {
        [self.view makeToast:[@"Please input name!" localized] duration:3.0 position:CSToastPositionCenter];
        return;
    }
    if (_tfEmail.text.length == 0) {
        [self.view makeToast:[@"Please input Email!" localized] duration:3.0 position:CSToastPositionCenter];
        return;
    }
    if (_tfSubject.text.length == 0) {
        [self.view makeToast:[@"Please input Subject!" localized] duration:3.0 position:CSToastPositionCenter];
        return;
    }
    if (_tvContent.text.length == 0) {
        [self.view makeToast:[@"Please input Content!" localized] duration:3.0 position:CSToastPositionCenter];
        return;
    }
    
    if([Validator validateEmail:_tfEmail.text]){
        keyArray = [NSArray arrayWithObjects:@"name",@"email",@"type",@"subject",@"content", nil];
        NSString *temp = @"";
        if ([_strTitle isEqualToString:CONTACT_REPORT_ADS]|| [_strTitle isEqualToString:CONTACT_REPORT_SELLER]) {
            temp = @"2";
        }else{
            temp = @"1";
        }

        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [ModelManager contactUsActionWithName:_tfName.text email:_tfEmail.text type:temp subject:_tfSubject.text content:_tvContent.text withSuccess:^(NSString *strSuccess) {
            [self.view makeToast:[@"Successful!" localized] duration:3.0 position:CSToastPositionCenter];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [self clear];
        } failure:^(NSString *err) {
            [self.view makeToast:err duration:3.0 position:CSToastPositionCenter];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }];

    }else{
        [self.view makeToast:[@"Email invalid!" localized] duration:3.0 position:CSToastPositionCenter];
        return;
        
    }
}

-(void) clear{
    if ([_strTitle isEqualToString:[@"Report" localized]]) {
        self.tfName.text = @"";
        self.tfEmail.text = @"";
        self.tvContent.text = @"";
    }else{
        self.tfName.text = @"";
        self.tfEmail.text = @"";
        self.tfSubject.text = @"";
        self.tvContent.text = @"";
    }
}

@end
