//
//  FavoriteViewController.m
//  Real Estate
//
//  Created by De Papier on 4/14/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "MoreFieldDrawMenu.h"

@interface MoreFieldDrawMenu ()

@end

@implementation MoreFieldDrawMenu

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLOR_BACKGROUD_VIEW;
    _lblTitle.text = [@"Favorite" localized];
    [_imgNavi setBackgroundColor:COLOR_DARK_PR_MARY];
    // Do any additional setup after loading the view from its nib.
    if ([_strBack isEqualToString:@"YES"]) {
        [_revealBtn setBackgroundImage:[UIImage imageNamed:@"ic_back.png"] forState:UIControlStateNormal];
    }else{
    [_revealBtn setBackgroundImage:[UIImage imageNamed:@"ic_menu.png"] forState:UIControlStateNormal];
    }
    [self intitData];

}
- (IBAction)onMenuBtn:(id)sender {
    if ([_strBack isEqualToString:@"YES"]) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
    [self.revealViewController revealToggle:nil];
    }
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)intitData{
    _lblTitle.text = _strTitle;
[_webView  loadHTMLString:_strPresent baseURL:nil];
}
@end
