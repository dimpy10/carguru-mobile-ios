//
//  BtnReportTableViewCell.m
//  Real Estate
//
//  Created by Hicom on 3/22/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import "BtnReportTableViewCell.h"

@implementation BtnReportTableViewCell

- (void)awakeFromNib {
    self.btnReport.layer.cornerRadius = 4;
    self.btnReport.clipsToBounds = YES;
    [self.addEstimatePrice setTitle:[@"ADD ESTIMATE PRICE" localized] forState:UIControlStateNormal];
    [self.btnReport setTitle:[@"REPORT" localized] forState:UIControlStateNormal];
    //self.btnReport.layer.shadowColor = [UIColor blackColor].CGColor;
    //self.btnReport.layer.shadowOffset = CGSizeMake(0 ,1);
    //self.btnReport.layer.shadowOpacity = 0.3;
    //_btnReport.backgroundColor = COLOR_PRIMARY_DEFAULT;
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
