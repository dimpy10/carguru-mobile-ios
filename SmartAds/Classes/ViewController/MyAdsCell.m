

#import "MyAdsCell.h"

@implementation MyAdsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.btnEdit.layer.cornerRadius = 4;
    self.btnEdit.clipsToBounds = YES;
    self.btnEdit.layer.shadowColor = [UIColor blackColor].CGColor;
    self.btnEdit.layer.shadowOffset = CGSizeMake(3 ,3);
    self.btnEdit.layer.shadowOpacity = 0.5;
    _btnEdit.backgroundColor = COLOR_PRIMARY_DEFAULT;
    _availbleLable.layer.masksToBounds = YES;
    _feeLabel.layer.masksToBounds = YES;
    _feeLabel.layer.cornerRadius = 10;
    _availbleLable.layer.cornerRadius = 10;
    _thumnails.layer.borderColor = [UIColor blackColor].CGColor;
    _thumnails.layer.borderWidth = 0.5;
    _thumnails.contentMode = UIViewContentModeScaleAspectFill;
    _thumnails.clipsToBounds = YES;
    
    _category.text = [@"Category:" localized];
    _type.text = [@"Type:" localized];
    _location.text = [@"Location:" localized];
    _posted.text = [@"Posted:" localized];
    _status.text = [@"Status:" localized];
}



@end
