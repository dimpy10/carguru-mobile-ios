//
//  SubcriberViewCell.m
//  SmallAds
//
//  Created by Hicom on 5/14/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import "SubcriberViewCell.h"

@implementation SubcriberViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)touchCheck:(id)sender {

    [self.delegate SubcriberViewCellTouch:self.itemEdit];
}

@end
