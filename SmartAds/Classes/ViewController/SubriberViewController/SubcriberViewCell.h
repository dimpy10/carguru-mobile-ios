//
//  SubcriberViewCell.h
//  SmallAds
//
//  Created by Hicom on 5/14/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubCriberItem.h"

@protocol SubcriberViewCellDelegate <NSObject>

-(void)SubcriberViewCellTouch:(SubCriberItem *)item;

@end

@interface SubcriberViewCell : UITableViewCell
- (IBAction)touchCheck:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheck;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblValue;
@property (nonatomic, weak) id<SubcriberViewCellDelegate> delegate;
@property (strong, nonatomic) SubCriberItem *itemEdit;

@end
