//
//  SubcriberItemType.h
//  SmallAds
//
//  Created by Hicom on 5/14/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubcriberItemType : NSObject

@property (nonatomic, strong) NSString *itemCurrencySymbol;
@property (nonatomic, strong) NSString *itemDuration;
@property (assign) int itemID;
@property (assign) int itemPrice;
@property (assign) int itemStatus;


@end
