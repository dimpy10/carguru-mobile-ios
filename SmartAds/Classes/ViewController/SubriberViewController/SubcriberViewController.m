//
//  SubcriberViewController.m
//  Real Ads
//
//  Created by De Papier on 4/23/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "SubcriberViewController.h"
#import "UIView+Toast.h"
#import "SubCriberItem.h"

@interface SubcriberViewController ()<SubcriberViewCellDelegate>
{
    int check;
    int Sum;
    NSArray *arrayItems;
}
@property (weak, nonatomic) IBOutlet UILabel *forLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imgNaviBg;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UILabel *startDate;
@property (weak, nonatomic) IBOutlet UILabel *endDate;

@end

@implementation SubcriberViewController
NSString *currency;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.subLabel.text = [@"Subcribe" localized];
    self.startDate.text = [@"Start Date:" localized];
    self.headerLabel.text = [@"Subcribe to activate ads" localized];
    self.forLabel.text = [@"For" localized];
    self.endDate.text = [@"End Date:" localized];
    [self.btnBanking setTitle:[@"PAYMENT BANKING" localized] forState:UIControlStateNormal];
    [self.btnPayment setTitle:[@"PAYMENT VIA PAYPAL" localized] forState:UIControlStateNormal];
    self.view.backgroundColor = [UIColor whiteColor];
    _imgNaviBg.backgroundColor = COLOR_DARK_PR_MARY;
    _serviceLabel.text = _Ads.adsTitle;
    check = 1;
    Sum = 10;
    // Do any additional setup after loading the view from its nib.
    //Setup keyboard
    keyBoardController=[[UIKeyboardViewController alloc] initWithControllerDelegate:self];
    [keyBoardController addToolbarToKeyboard];
    _startDateTextField.text = [Util stringFromDate:[NSDate date] format:@"yyyy/MM/dd"];
    NSDate *start = [self getDateFromDateString:self.startDateTextField.text];
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setMonth:check];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *newDate = [calendar dateByAddingComponents:dateComponents toDate:start options:0];
    _endDatelabel.text = [self getDateStringFromDate:newDate];
    [self dateAndTimePicker];
    self.acceptCreditCards = NO;
    self.environment = PayPalEnvironmentSandbox;
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    //    [self.tableView registerClass:[SubcriberViewCell class] forCellReuseIdentifier:@"SubcriberViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"SubcriberViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"SubcriberViewCell"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [ModelManager getlistSubscriptionWithSuccess:^(NSArray *arr) {
        arrayItems = arr;
        [self.tableView reloadData];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    } failure:^(NSString *err) {
        
    }];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self layoutView];
}
-(void)layoutView
{
    [[self.ViewShowBanking layer]setCornerRadius:6];
    self.ViewBanking.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];

    [[self.uiView layer]setCornerRadius:6];
    [_serviceLabel setNumberOfLines:0];
    [_serviceLabel  sizeToFit];

}

//
-(void)viewWillAppear:(BOOL)animated{
    self.environment = PayPalEnvironmentSandbox;
    [PayPalMobile initializeWithClientIdsForEnvironments:@{
                                                           PayPalEnvironmentSandbox: kPayPalClientId}];
    [PayPalMobile preconnectWithEnvironment:self.environment];

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrayItems.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SubcriberViewCell *cell = (SubcriberViewCell*)[tableView dequeueReusableCellWithIdentifier:@"SubcriberViewCell"];
    cell.delegate = self;
    
    SubCriberItem *i  = arrayItems[indexPath.row];
    cell.itemEdit = i;
    
    if (i.itemPrice == 0) {
        cell.lblValue.text = [NSString stringWithFormat:@"%@", [@"Free" localized]];
    }else{
        cell.lblValue.text = [NSString stringWithFormat:@"%@ %d", i.itemCurrencySymbol, i.itemPrice];
    }
    if(i.itemIsFeatured == 1){
        cell.lblName.text = [NSString stringWithFormat:@"%@ days (With Featured)", i.itemDuration];
    }else{
        cell.lblName.text = [NSString stringWithFormat:@"%@ days", i.itemDuration];
    }
    cell.imgCheck.hidden = !i.isChecked;
    return cell;
    
}

-(void)SubcriberViewCellTouch:(SubCriberItem *)item{
    item.isChecked = !item.isChecked;
    Sum = item.itemPrice;
    check = [item.itemDuration intValue];
    for (SubCriberItem *i in arrayItems) {
        if (i.itemID != item.itemID){
            i.isChecked = NO;
        }
    }
    
    [self.tableView reloadData];
    [self setEndDate];
    
}

//Date and time
-(void)dateAndTimePicker
{
    //date
    datePicker = [[UIDatePicker alloc]init];
    
    [datePicker setFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 180)];
    [datePicker setDate:[NSDate date]];
    datePicker.datePickerMode = UIDatePickerModeDate;
    //datePicker.backgroundColor = self.view.backgroundColor;
    
    datePicker.backgroundColor = [UIColor clearColor];
    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    UIView *datePickerView = [[UIView alloc]initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 180, [UIScreen mainScreen].bounds.size.width, 180)];
    [datePickerView addSubview:datePicker];
    
    self.startDateTextField.delegate = self;
    [self.startDateTextField setInputView:datePickerView];
    
    
}
-(void)updateTextField:(id)sender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy/MM/dd"];
    self.startDateTextField.text = [dateFormat stringFromDate:datePicker.date];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([self.startDateTextField.text isEqualToString:@""]) {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy/MM/dd"];
        self.startDateTextField.text = [dateFormat stringFromDate:[NSDate date]];
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self setEndDate];
}
- (IBAction)on1M:(id)sender {
    [self hideAll];
    _check1MImg.hidden = NO;
    check = 1;
    Sum = 50;
    [self setEndDate];
}

- (IBAction)on3M:(id)sender {
    [self hideAll];
    _check3MImg.hidden = NO;
    check = 3;
    Sum = 150;
    [self setEndDate];
}

- (IBAction)on6M:(id)sender {
    [self hideAll];
    _check6MImg.hidden = NO;
    check = 6;
    Sum = 300;
    [self setEndDate];
}

- (IBAction)on1Y:(id)sender {
    [self hideAll];
    _check1YImg.hidden = NO;
    check = 12;
    Sum = 600;
    [self setEndDate];
}
-(void)hideAll
{
    _check1MImg.hidden = YES;
    _check6MImg.hidden = YES;
    _check3MImg.hidden = YES;
    _check1YImg.hidden = YES;
}

-(NSString *)getDateStringFromDate :(NSDate *)date {
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy/MM/dd"];
    NSString *dateString = [dateFormatter stringFromDate:date];
    return dateString;
}

-(NSDate *)getDateFromDateString :(NSString *)dateString {
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy/MM/dd"];
    NSDate *date = [dateFormatter dateFromString:dateString];
    return date;
}
-(void)setEndDate
{
    NSDate *start = [self getDateFromDateString:self.startDateTextField.text];
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setDay:check];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *newDate = [calendar dateByAddingComponents:dateComponents toDate:start options:0];
    self.endDatelabel.text = [self getDateStringFromDate:newDate];
}
- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)OnPayment:(id)sender {
    // [self.view makeToast:@"This function is disabled!" duration:1.5 position:CSToastPositionCenter];
    //    [self sendOrderToServer];
    
    
    [self requirePaypalPayment];
}

#pragma mark PAYPAL FUNCTIONS

- (void)requirePaypalPayment {

    
    
    // Remove our last completed payment, just for demo purposes.
    self.completedPayment = nil;
    
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    payment.amount = [[NSDecimalNumber alloc] initWithString:[NSString stringWithFormat:@"%d",Sum]];
    payment.currencyCode = @"USD";
    payment.shortDescription = @"Payment for Order in App";
    NSMutableArray *itemArr = [[NSMutableArray alloc]init];
    for (SubCriberItem *temp in arrayItems) {
        if (temp.isChecked) {
            PayPalItem *item1 = [PayPalItem itemWithName:gUser.usName
                                            withQuantity:1
                                               withPrice:[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%d",temp.itemPrice]]
                                            withCurrency:@"USD"
                                                 withSku:@""];
            [itemArr addObject:item1];
        }
        
    }
    payment.items = itemArr;
    if (!payment.processable) {
    }
    [PayPalMobile preconnectWithEnvironment:self.environment];
    
    PayPalConfiguration *config = [[PayPalConfiguration alloc]init];
    
    config.acceptCreditCards = NO;
    config.languageOrLocale = @"en";
    
    
    PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment configuration:config delegate:self];
    //    paymentViewController.hideCreditCardButton = !self.acceptCreditCards;
    
    // Setting the languageOrLocale property is optional.
    //
    // If you do not set languageOrLocale, then the PayPalPaymentViewController will present
    // its user interface according to the device's current language setting.
    //
    // Setting languageOrLocale to a particular language (e.g., @"es" for Spanish) or
    // locale (e.g., @"es_MX" for Mexican Spanish) forces the PayPalPaymentViewController
    // to use that language/locale.
    //
    // For full details, including a list of available languages and locales, see PayPalPaymentViewController.h.
    //    paymentViewController.languageOrLocale = @"en";
    
    [self presentViewController:paymentViewController animated:YES completion:nil];
}

#pragma mark - Proof of payment validation

- (void)sendCompletedPaymentToServer:(PayPalPayment *)completedPayment {
    // TODO: Send completedPayment.confirmation to server
    NSLog(@"Here is your proof of payment:\n\n%@\n\nSend this to your server for confirmation and fulfillment.", completedPayment.confirmation);
}

#pragma mark - PayPalPaymentDelegate methods

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment {
    NSLog(@"PayPal Payment Success!");
    self.completedPayment = completedPayment;
    [self sendCompletedPaymentToServer:completedPayment]; // Payment was processed successfully; send to server for verification and fulfillment
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self sendOrderToServer];
}
- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    NSLog(@"PayPal Payment Canceled");
    self.completedPayment = nil;
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)sendOrderToServer{
    NSString *subId = @"";
    for (SubCriberItem *temp in arrayItems) {
        if (temp.isChecked) {
            subId = [NSString stringWithFormat:@"%d",temp.itemID];
        }
    }
    NSArray* keyArray = [NSArray arrayWithObjects:@"paidDate",@"endDate",@"duration",@"value",@"adsId",@"payment",@"subId", nil];
    
    NSString *startText = self.startDateTextField.text;
    NSString *endText = self.endDatelabel.text;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy/MM/dd"];
    NSDate *startDate = [dateFormat dateFromString:startText];
    NSDate *endDate = [dateFormat dateFromString:endText];
    NSLog(@"date: %@, %@", startDate, endDate);
    
    
    double resultStart = startDate.timeIntervalSince1970;
    double resultEnd = endDate.timeIntervalSince1970;
    NSLog(@"time: %f, %f", resultStart, resultEnd);
    
    NSArray* objectArray = [NSArray arrayWithObjects:[NSNumber numberWithFloat:resultStart],[NSNumber numberWithFloat:resultEnd],[NSString stringWithFormat:@"%d",check],[NSString stringWithFormat:@"%d",Sum],self.Ads.adsId,@"1",subId, nil];
    NSDictionary *dic = [NSDictionary dictionaryWithObjects:objectArray forKeys:keyArray];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [ModelManager addSubCriptionWithParam:dic withSuccess:^(NSString *strSuccess) {
        NSLog(@"%@", strSuccess);
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [self.view makeToast:strSuccess duration:2.0 position:CSToastPositionCenter];
    } failure:^(NSString *err) {
        NSLog(@"%@", err);
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [self.view makeToast:err duration:2.0 position:CSToastPositionCenter];
        
    }];
    
    
}
- (IBAction)OnBackKing:(id)sender {
    NSArray* keyArray = [NSArray arrayWithObjects:@"paidDate",@"endDate",@"duration",@"value",@"adsId",@"payment", nil];
    NSArray* objectArray = [NSArray arrayWithObjects:self.startDateTextField.text,self.endDatelabel.text,[NSString stringWithFormat:@"%d",check],[NSString stringWithFormat:@"%d",Sum],self.Ads.adsId,@"2", nil];
    NSDictionary *dic = [NSDictionary dictionaryWithObjects:objectArray forKeys:keyArray];
    [MBProgressHUD showHUDAddedTo:self.ViewBanking animated:YES];
    [ModelManager addSubCriptionWithParam:dic withSuccess:^(NSString *strSuccess) {
        [MBProgressHUD hideAllHUDsForView:self.ViewBanking animated:YES];
        [self.view makeToast:strSuccess duration:2.0 position:CSToastPositionCenter];
    } failure:^(NSString *err) {
        [MBProgressHUD hideAllHUDsForView:self.ViewBanking animated:YES];
        [self.view makeToast:err duration:2.0 position:CSToastPositionCenter];
        
    }];
    
    
}
- (IBAction)OnOKBanking:(id)sender {
    self.ViewBanking.hidden = YES;
}
@end
