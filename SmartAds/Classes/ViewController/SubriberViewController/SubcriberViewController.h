//
//  SubcriberViewController.h
//  Real Ads
//
//  Created by De Papier on 4/23/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIKeyboardViewController.h"
#import "Ads.h"
#import "PayPalMobile.h"
#import "SubcriberViewCell.h"

@interface SubcriberViewController : UIViewController<UIKeyboardViewControllerDelegate,UITextFieldDelegate,PayPalPaymentDelegate, UITableViewDelegate, UITableViewDataSource>
{
    UIKeyboardViewController *keyBoardController;
    UIDatePicker *datePicker;
    UIDatePicker *Enddate;
}
@property (strong, nonatomic)  PayPalConfiguration* payPalConfig;
@property(nonatomic, strong, readwrite) PayPalPayment *completedPayment;
#pragma mark Paypal vars
- (IBAction)OnBackKing:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *ViewShowBanking;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (strong, nonatomic) IBOutlet UIView *ViewBanking;
@property (weak, nonatomic) IBOutlet UIButton *btnBanking;
@property(nonatomic, assign, readwrite) BOOL acceptCreditCards;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property(nonatomic, strong, readwrite) NSString *environment;
@property (strong, nonatomic) Ads *Ads;
@property (weak, nonatomic) IBOutlet UILabel *subLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView1;
@property (weak, nonatomic) IBOutlet UIView *uiView;
@property (weak, nonatomic) IBOutlet UITextField *startDateTextField;
- (IBAction)on1M:(id)sender;
- (IBAction)on3M:(id)sender;
- (IBAction)on6M:(id)sender;
- (IBAction)on1Y:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *check1MImg;
- (IBAction)OnOKBanking:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *check3MImg;
@property (weak, nonatomic) IBOutlet UIImageView *check6MImg;
@property (weak, nonatomic) IBOutlet UIImageView *check1YImg;
@property (weak, nonatomic) IBOutlet UILabel *endDatelabel;
- (IBAction)onBack:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnPayment;

- (IBAction)OnPayment:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtEndDate;
@end
