

#import "MyAdsVC.h"
#import "UITableView+DragLoad.h"
#import "MyAdsCell.h"
#import "NothingCell.h"
#import "EditAdsVC.h"
#import "UIScrollView+BottomRefreshControl.h"
#import "UIView+Toast.h"
#import "UIImageView+WebCache.h"
#import "SortViewController.h"
#import "MyAdsCellEdit.h"

#define STATUS_ACTIVE @"1"
#define STATUS_INACTIVE @"0"
#define STATUS_DRAFT @"2"
#define STATUS_EXPIRED @"3"



@interface MyAdsVC () <UITableViewDragLoadDelegate, UITableViewDataSource, UITableViewDelegate, SortViewDelegate, EditAdsDelegate>
@property (weak, nonatomic) IBOutlet UIView *topNaviView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet MarqueeLabel *headerLabel;


@end

@implementation MyAdsVC{
    NSMutableArray *arrAds;
    BOOL isLoading;
    NSArray *arrSortType;
    NSString *sortBy,*sortType;
    int page,start_index;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.headerLabel.text = [@"My Ads" localized];
    self.view.backgroundColor = COLOR_BACKGROUD_VIEW;
    _topNaviView.backgroundColor = COLOR_DARK_PR_MARY;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44.0;
    _tableView.layer.masksToBounds = NO;
    _viewTblView.layer.masksToBounds = YES;
    page =1;
    start_index =1;
    [self initLoadmoreAndPullToRefresh];
    [self initSortFunction];
    [self setRevealBtn];
    [self refreshData];
    _activityIndicator.hidden = YES;
}
-(void)viewWillAppear:(BOOL)animated{
    //    [self.activityIndicator startAnimating];
    //    self.activityIndicator.hidden = NO;
    
    //[self performSelectorInBackground:@selector(refreshData) withObject:nil];
}
-(void)initSortFunction{
    arrSortType = @[[@"Sort by Name asc" localized],
                    [@"Sort by Name desc" localized],
                    [@"Sort by Date asc" localized],
                    [@"Sort by Date desc" localized],
                    [@"Number of Viewed acs" localized],
                    [@"Number of Viewed desc" localized]];
    sortBy = SORT_BY_ALL;
    sortType = SORT_TYPE_NORMAL;
}
-(void)initLoadmoreAndPullToRefresh{
    arrAds = [[NSMutableArray alloc]init];
    isLoading = false;
    
    
    _topRefreshTable = [UIRefreshControl new];
    _topRefreshTable.attributedTitle = [[NSAttributedString alloc] initWithString:[@"Pull down to reload!" localized] attributes:@{NSForegroundColorAttributeName:COLOR_PRIMARY_DEFAULT }];
    _topRefreshTable.tintColor = COLOR_PRIMARY_DEFAULT;
    [_topRefreshTable addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    
    [_tableView addSubview:_topRefreshTable];
    
    UIRefreshControl *refreshControl = [UIRefreshControl new];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:[@"Pull up to load more!" localized] attributes:@{NSForegroundColorAttributeName:COLOR_PRIMARY_DEFAULT}];
    // refreshControl.triggerVerticalOffset = 60;
    [refreshControl addTarget:self action:@selector(loadMore) forControlEvents:UIControlEventValueChanged];
    refreshControl.tintColor = COLOR_PRIMARY_DEFAULT;
    self.tableView.bottomRefreshControl = refreshControl;
    
    
}
-(void)setRevealBtn{
    SWRevealViewController *revealController = self.revealViewController;
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    [_revealBtn addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)refreshData{
    if (isLoading) {
        return;
    }else
        start_index = 1;
    isLoading = true;
    [self performSelectorInBackground:@selector(getData) withObject:nil];
}
-(void)loadMore{
    if (isLoading) {
        return;
    }else{
        isLoading = true;
    }
    if (start_index >= page) {
        [self.view makeToast:[@"No more products" localized] duration:2.0 position:CSToastPositionCenter];
        [self finishLoading];
        return;
    }
    start_index+=1;
    [self performSelectorInBackground:@selector(getData) withObject:nil];
}

-(void)getData{
    [MBProgressHUD showHUDAddedTo:self.viewTblView animated:YES];
    [ModelManager getAdsByUserId:gUser.usId andPage:[NSString stringWithFormat:@"%d",start_index] sortType:sortType sortBy:sortBy withSuccess:^(NSDictionary *dicReturn) {
        if (start_index == 1) {
            [arrAds removeAllObjects];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            page = [dicReturn[@"allpage"] intValue];
            [arrAds addObjectsFromArray:dicReturn[@"arrAds"]];
            NSLog(@"%@",arrAds);
            [_tableView reloadData];
            [self finishLoading];
            
        });
    } failure:^(NSString *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self finishLoading];
            
        });
    }];
    
}
-(void)finishLoading{
    [MBProgressHUD hideAllHUDsForView:self.viewTblView animated:YES];
    
    [_topRefreshTable endRefreshing];
    [self.tableView.bottomRefreshControl endRefreshing];
    isLoading = false;
}

//
//-(void)setRevealBtn{
//    SWRevealViewController *revealController = self.revealViewController;
//    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
//    [_revealBtn addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
//
//}

#pragma mark UITABALEVIEW DATASOURCE

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return arrAds.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MyAdsCellEdit *cell = [tableView dequeueReusableCellWithIdentifier:@"MyAdsCellEdit"];
    
    if (!cell) {
        
        cell = [[[NSBundle mainBundle] loadNibNamed:@"MyAdsCellEdit" owner:nil options:nil] objectAtIndex:0];
    }
    
    
    Ads *est = [arrAds objectAtIndex:indexPath.row];
    cell.lblTitle.text = est.adsTitle;
    [cell.imageAds setImageWithURL:[NSURL URLWithString:est.adsImage] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        
    }];
    
   // NSLog(@"%@" ,est.adsCurrencyCode );
   // NSLog(@"%@" ,est.adsCurrencyCode );
    //cell.imageView3.imageURL = [NSURL URLWithString:est.image];
    if ([est.adsPrice isEqualToString:@"3"]) {
        cell.lblPrice.text = [NSString stringWithFormat:@"%@%@/%@",est.adsCurrencyCode,est.adsPriceValue,est.adsPriceUnit];
    }
    if ([est.adsPrice isEqualToString:@"1"]) {
        cell.lblPrice.text = [NSString stringWithFormat:@"%@", [@"Free" localized]];
    }
    
    if ([est.adsPrice isEqualToString:@"2"]) {
        cell.lblPrice.text = [@"Negotiate" localized];
    }
    cell.lblAvaiable.hidden = NO;
    [cell.lblAvaiable setTransform:CGAffineTransformMakeRotation(-M_PI / 4)];
    if ([est.adsIsAvailable isEqualToString:@"1"]) {
        cell.lblAvaiable.hidden = YES;
    } else if ([est.adsIsAvailable isEqualToString:@"0"])
    {
        if([est.adsForRent isEqualToString:@"1"]){
            cell.lblAvaiable.text =  [@"RENTED" localized] ;
        }else if([est.adsForSale isEqualToString:@"1"]){
            cell.lblAvaiable.text =  [@"SOLD" localized];
        }
    }
    
    [cell.imageAds setContentMode:UIViewContentModeScaleAspectFill];
    cell.imageAds.clipsToBounds = YES;
    if ([est.adsForRent isEqualToString:@"1"]) {
        cell.lblRentOrSale.text = [@"For rent" localized];
    }
    if ([est.adsForSale isEqualToString:@"1"]) {
        cell.lblRentOrSale.text = [@"For sale" localized];
    }
    if (est.adsCategory) {
        cell.lblCategory.text = est.adsCategory;
    }else{
        cell.lblCategory.text = [@"All Categories" localized];
    }
    
    if([est.adsMileage isEqualToString:@""]) {
        cell.mileageLabel.text = @"-";
    } else {
        cell.mileageLabel.text = est.adsMileage;
    }
    
  //  if (![est.estimate_price isEqualToString:@""] && ![est.adsPrice isEqualToString:@"1"] ) {
        cell.estimatePriceLabel.text = [NSString stringWithFormat:@"%.2f %@", est.estimate_price.doubleValue , est.adsCurrencyCode];   // NSString(@"")est.estimate_price;
 //   } else {
 //       cell.estimatePriceLabel.text = @" - ";
 //   }
    
    //if (![est.adsPriceValue isEqualToString:@""] && ![est.adsPrice isEqualToString:@"1"]) {
        cell.actualPriceLabel.text = [NSString stringWithFormat:@"%@ %@",est.adsPriceValue,est.adsCurrencyCode];
   // } else {
  //      cell.actualPriceLabel.text = @" - ";
  //  }
    
    if (est.adsSub) {
        cell.lblSubCat.text = est.adsSub;
    }else{
        cell.lblSubCat.text = [@"All Sub Categories" localized] ;
    }
    
    
    if (est.adsCity) {
        if ([est.adsCountry isEqualToString:@""]) {
            cell.lblCity.text = est.adsCity;
        } else {
            if([est.adsCity isEqualToString:@""]) {
                cell.lblCity.text = est.adsCountry;
            } else {
                cell.lblCity.text = [NSString stringWithFormat:@"%@, %@", est.adsCity, est.adsCountry];
            }
        }
        
    }else{
        cell.lblCity.text = [@"All cities" localized] ;
    }
    
    if ([est.adsStatus isEqualToString:STATUS_DRAFT]) {
        cell.lblStatusValue.text = [@"Draft" localized];
    }
    if ([est.adsStatus isEqualToString:STATUS_ACTIVE]) {
        cell.lblStatusValue.text = [@"Active" localized] ;
    }
    if ([est.adsStatus isEqualToString:STATUS_EXPIRED]) {
        cell.lblStatusValue.text = [@"Expried" localized];
    }
    if ([est.adsStatus isEqualToString:STATUS_INACTIVE]) {
        cell.lblStatusValue.text = [@"Inactive" localized];
    }
    cell.lblPosted.text = [est.adsDateCreated substringWithRange:NSMakeRange(0, 10)];
    if([est.adsIsFeatured isEqualToString:@"1"]){
        cell.isFeatured.hidden = NO;
    }else{
        cell.isFeatured.hidden = YES;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Ads *est = [arrAds objectAtIndex:indexPath.row];
    EditAdsVC *VC = [[EditAdsVC alloc]initWithNibName:@"EditAdsVC" bundle:nil];
    VC.adsObj = est;
    VC.delegate = self;
    [self.navigationController pushViewController:VC animated:YES];
}



- (IBAction)onSort:(id)sender {
    SortViewController *sortVC = [[SortViewController alloc]initWithNibName:@"SortViewController" bundle:nil];
    sortVC.delegate = self;
    sortVC.arrDataSource = arrSortType;
    [sortVC presentInParentViewController:self];
}


-(void)sortViewDidSelectedItemAtIndex:(int)rowSelected{
    switch (rowSelected) {
        case 0:
            sortBy = SORT_BY_NAME;
            sortType = SORT_TYPE_ASC;
            break;
        case 1:
            sortBy = SORT_BY_NAME;
            sortType = SORT_TYPE_DESC;
            break;
        case 2:
            sortBy = SORT_BY_POSTED_DATE;
            sortType = SORT_TYPE_ASC;
            break;
        case 3:
            sortBy = SORT_BY_POSTED_DATE;
            sortType = SORT_TYPE_DESC;
            break;
        case 4:
            sortBy = SORT_BY_VIEW;
            sortType = SORT_TYPE_ASC;
            break;
        case 5:
            sortBy = SORT_BY_VIEW;
            sortType = SORT_TYPE_DESC;
            break;
            
        default:
            break;
    }
    start_index =1;
    isLoading = YES;
    [self getData];
    
    
}
-(void)didDeleteAds:(id)adsObj{
    [arrAds removeObject:adsObj];
    [_tableView reloadData];
}


@end
