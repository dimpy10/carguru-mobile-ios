//
//  AllSubCategoriesViewController.m
//  Real Estate
//
//  Created by Hicom on 2/20/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import "AllSubCategoriesViewController.h"
#import "CategoryPr.h"
#import "CategoriesCell.h"
#import "SearchAdsResultVC.h"

@interface AllSubCategoriesViewController () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
    @property (weak, nonatomic) IBOutlet UILabel *headerLabel;
    

@end

@implementation AllSubCategoriesViewController{
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    _lblSubCate.text = _strTitle;
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.tableFooterView = view;
    self.headerLabel.text = [@"Sub Categories" localized];
}

- (IBAction)toggleMenu:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.listSubCategories.count == 0) {
        return 1;
    }else{
        return self.listSubCategories.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"CategoriesCell";
    CategoriesCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CategoriesCell" owner:nil options:nil] objectAtIndex:0];
    }
    cell.imgRight.hidden = YES;
    
    if (self.listSubCategories.count == 0) {
        cell.lblTitle.text = [@"No SubCategories" localized];
        return cell;

    }else{
         CategoryPr *item = self.listSubCategories[indexPath.row];
        cell.lblTitle.text = [NSString stringWithFormat:@"%@", item.categoryName] ;
        
        return cell;

    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.listSubCategories.count == 0) {
        return;
    }
    CategoryPr *cate = self.listSubCategories[indexPath.row];
    NSDictionary *param;
    if (indexPath.row == 0) {
        param = @{
                  @"page":@"1",
                  @"sortBy":@"",
                  @"sortType":@"",
                  @"keyword":@"",
                  @"type":@"",
                  @"categoryId":[Validator getSafeString:cate.categoryParentId],
                  @"subCate":@"",
                  @"city": @"",
                  @"individual": @"",
                  @"date": @"",
                  @"time_option":@""};
    }else{
    param = @{
                            @"page":@"1",
                            @"sortBy":@"",
                            @"sortType":@"",
                            @"keyword":@"",
                            @"type":@"",
                            @"categoryId":[Validator getSafeString:cate.categoryParentId],
                            @"subCate":[Validator getSafeString:cate.categoryId],
                            @"city": @"",
                            @"individual": @"",
                            @"date": @"",
                            @"time_option":@""};
    }
    SearchAdsResultVC *searchResultVC = [[SearchAdsResultVC alloc]init];
    searchResultVC.paramDic = param.mutableCopy;
    searchResultVC.titleText = cate.categoryName;
    [self.navigationController pushViewController:searchResultVC animated:YES];
}


@end
