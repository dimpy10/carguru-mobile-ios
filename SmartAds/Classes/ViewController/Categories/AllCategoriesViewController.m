//
//  AllCategoriesViewController.m
//  Real Estate
//
//  Created by Hicom on 2/20/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import "AllCategoriesViewController.h"
#import "AllSubCategoriesViewController.h"
#import "UIView+Toast.h"
#import "CategoryPr.h"
#import "CategoriesCell.h"
#import "SearchAdsResultVC.h"


@interface AllCategoriesViewController () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
    @property (weak, nonatomic) IBOutlet UILabel *headerLabel;
    
@end

@implementation AllCategoriesViewController{
    NSArray *listCategories;
    NSMutableArray *arrParent,*arrsubCate;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.tableFooterView = view;
    listCategories = [[NSArray alloc]init];
    arrsubCate = [[NSMutableArray alloc]init];
    arrParent = [[NSMutableArray alloc]init];
    CategoryPr *cateOne = [[CategoryPr alloc]init];
    cateOne.categoryId = @"0";
    cateOne.categoryName = [@"All" localized] ;
    cateOne.categoryParentId =@"0";
    [arrParent addObject:cateOne];
    [_indicator startAnimating];
    [self initData];
    self.headerLabel.text = [@"Categories" localized];
    
    
}
-(void)initData{
[ModelManager getListCategoryWithSuccess:^(NSArray *arrCategory) {
    [_indicator stopAnimating];
    _indicator.hidden = YES;
    listCategories = arrCategory;
    for (CategoryPr *cateObj in listCategories) {
        if ([[Validator getSafeString:cateObj.categoryParentId] isEqualToString:@"0"]) {
            [arrParent addObject:cateObj];
        }
    }
    [_tableView reloadData];
    
} failure:^(NSString *err) {
    if (self) {
        [_indicator stopAnimating];
        _indicator.hidden = YES;
        [self.view makeToast:err duration:2.0 position:CSToastPositionCenter];
    }
}];

}

- (IBAction)toggleMenu:(id)sender {
    [self.revealViewController revealToggle:nil];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrParent.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"CategoriesCell";
    CategoriesCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
       if (cell == nil) {
           cell = [[[NSBundle mainBundle] loadNibNamed:@"CategoriesCell" owner:nil options:nil] objectAtIndex:0];
       }
    
  
        CategoryPr *item = arrParent[indexPath.row];
        cell.lblTitle.text = [NSString stringWithFormat:@"%@", item.categoryName] ;
        if (arrParent.count ==1) {
        cell.lblTitle.text = [@"No available category" localized];
    }
    [arrsubCate removeAllObjects];
    for (CategoryPr *cate in listCategories) {
        if ([[Validator getSafeString:cate.categoryParentId] isEqualToString:item.categoryId]) {
            [arrsubCate addObject:cate];
        }
    }
    if (arrsubCate.count >0) {
        if (indexPath.row==0) {
            cell.imgRight.hidden =YES;
        }
        else
        {
            cell.imgRight.hidden =NO;
        }

    }else{
        cell.imgRight.hidden= YES;
    }
        return cell;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row ==0) {
  
        NSDictionary *param = @{
                                @"page":@"1",
                                @"sortBy":@"",
                                @"sortType":@"",
                                @"keyword":@"",
                                @"type":@"",
                                @"categoryId":@"",
                                @"subCate":@"",
                                @"city": @"",
                                @"individual": @"",
                                @"date": @"",
                                @"time_option":@""};
        SearchAdsResultVC *searchResultVC = [[SearchAdsResultVC alloc]init];
        searchResultVC.paramDic = param.mutableCopy;
        searchResultVC.titleText = [@"All Ads" localized];
        [self.navigationController pushViewController:searchResultVC animated:YES];
        
        return;
    }
    CategoryPr *cateObj = [arrParent objectAtIndex:indexPath.row];
    NSString *idParentSub = [Validator getSafeString:cateObj.categoryId];
    [arrsubCate removeAllObjects];
    CategoryPr *cateSub = [[CategoryPr alloc]init];
    cateSub.categoryParentId = idParentSub;
    cateSub.categoryName = [@"All" localized] ;
    [arrsubCate addObject:cateSub];
    for (CategoryPr *cate in listCategories) {
        if ([[Validator getSafeString:cate.categoryParentId] isEqualToString:idParentSub]) {
            [arrsubCate addObject:cate];
        }
    }
   AllSubCategoriesViewController *controller = [[AllSubCategoriesViewController alloc]initWithNibName:@"AllSubCategoriesViewController" bundle:nil];
    controller.listSubCategories = arrsubCate;
    CategoryPr *item = arrParent[indexPath.row];
    controller.strTitle = [NSString stringWithFormat:@"%@", item.categoryName] ;
    controller.lblSubCate.text = cateObj.categoryName;
    [self.navigationController pushViewController:controller animated:YES];
}


@end
