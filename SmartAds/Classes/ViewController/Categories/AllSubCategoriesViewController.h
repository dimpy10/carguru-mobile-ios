//
//  AllSubCategoriesViewController.h
//  Real Estate
//
//  Created by Hicom on 2/20/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllSubCategoriesViewController : UIViewController

@property (nonatomic, strong) NSArray *listSubCategories;
@property (weak, nonatomic) IBOutlet UILabel *lblSubCate;
@property (weak, nonatomic) NSString *strTitle;


@end

