//
//  CategoriesCell.h
//  Real Estate
//
//  Created by Hicom on 3/31/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoriesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgRight;

@end
