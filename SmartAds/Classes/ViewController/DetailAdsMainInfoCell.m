//
//  DetailAdsMainInfoCell.m
//  Real Estate
//
//  Created by Hicom on 3/5/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import "DetailAdsMainInfoCell.h"

@implementation DetailAdsMainInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.posted.text = [@"Posted" localized];
    self.category.text = [@"Category" localized];
    self.actualPrice.text = [@"Actual Price" localized];
    self.subCategory.text = [@"Sub Category" localized];
    self.estimatePrice.text = [@"Estimate Price" localized];
    self.Location.text = [@"Location" localized];
    self.mileage.text = [@"Mileage" localized];
}







@end
