//
//  optionsController.h
//  SmartAds
//
//  Created by Rakesh Kumar on 17/03/18.
//  Copyright © 2018 Mr Lemon. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol optionsDelegate <NSObject>
-(void)optionsViewDidSelectedItemAtIndex:(int)rowSelected;
@end

@interface optionsController : UIViewController
@property (strong, nonatomic) NSArray *arrDataSource;

@property (nonatomic, weak) id<optionsDelegate> delegate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTbViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTbViewTrailling;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constrainTbViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTbViewWidth;


- (void)presentInParentViewController:(UIViewController *)parentViewController;
- (void)dismissFromParentViewController;

@end
