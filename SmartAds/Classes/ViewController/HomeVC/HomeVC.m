//
//  HomeVC.m
//  Real Estate
//]//

#import "HomeVC.h"
#import "HomeViewCell.h"
#import "MBProgressHUD.h"

#import "Macros.h"
#import "DetailAdsViewController.h"
//#import "LoadMoreTableViewCell.h"
#import "UIView+Toast.h"
#import "UIScrollView+BottomRefreshControl.h"
#import "UIImageView+WebCache.h"
#import "CountryViewController.h"
#import "CityObj.h"

#define SELECT_CITY             @"SELECT_CITY"

@interface HomeVC ()<CountryViewControllerDelegate>
{
    NSMutableArray *arrAds;
    NSString* isLoading;
    NSArray *arrSortType;
    NSString *sortBy,*sortType;
}
    @property (weak, nonatomic) IBOutlet UILabel *homeLabel;
    @property (weak, nonatomic) IBOutlet UIImageView *imgNaviBg;
@end

@implementation HomeVC{
    UIRefreshControl *refreshControl;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    [self initSortFunction];
    [self initLoadmoreAndPullToRefresh];
    [self setRevealBtn];
    
    [self.activityIndicator startAnimating];
    self.activityIndicator.hidden = NO;
   // arrCountries = [[NSMutableArray alloc] init];
    NSString *Country = [Util stringForKey:KEY_COUNTRY];
    arrCountries = [[NSMutableArray alloc] init];
    if([Country isEqualToString:@""]){
        self.homeLabel.text = [@"Countries" localized];        
    }else{
        self.homeLabel.text = [Country localized];
        [self getData];
    }
    [self setCountries];
    
}
-(void)setCountries {
    [ModelManager getCountriesWithSuccess:^(NSArray *countries) {
        if (arrCountries.count <2) {
            arrCountries = [arrCountries.copy arrayByAddingObjectsFromArray:countries].mutableCopy;
        }
        [self decorateUI];
    } failure:^(NSString *error) {
        
    }];
}

-(void)decorateUI {
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"HasLaunchedOnce"])
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HasLaunchedOnce"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self openCountrySelectView];
    }
   
}

- (IBAction)cancelSearchAction:(id)sender {
    start_index = 1;
    if(self.searchField.text.length > 0) {
        self.searchField.text = @"";
        [self.searchField resignFirstResponder];
        [self getData];
    }
    self.searchView.hidden = YES;
}

- (void)searchAction {
    start_index = 1;
    if(self.searchField.text.length > 0) {
        [self.searchField resignFirstResponder];
        [self getData];
    } else {
        self.searchField.text = @"";
        self.searchView.hidden = YES;
    }
}

-(void)initSortFunction{
    arrSortType = @[[@"Sort by Name asc" localized] ,
                    [@"Sort by Name desc" localized],
                    [@"Sort by Date asc" localized],
                    [@"Sort by Date desc" localized],
                    [@"Number of Viewed asc" localized],
                    [@"Number of Viewed desc" localized]];
    sortBy = SORT_BY_ALL;
    sortType = SORT_TYPE_NORMAL;
}

-(void)initData{
    
    NSDictionary *param = @{
                            @"page":@"1",
                            @"sortBy":@"",
                            @"sortType":@"",
                            @"keyword":@"",
                            @"type":@"",
                            @"categoryId":@"",
                            @"subCate":@"",
                            @"city":@"",
                            @"individual":@"",
                            @"date":@"",
                            @"time_option":@""};
    _paramDic = param.mutableCopy;
    
   
    
    self.view.backgroundColor = COLOR_BACKGROUD_VIEW;
    _imgNaviBg.backgroundColor = COLOR_DARK_PR_MARY;
    self.searchView.hidden = YES;
    self.searchView.backgroundColor = COLOR_DARK_PR_MARY;
    self.searchField.placeholder = [@"Search" localized];
    
    UIButton *addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //[addButton setTitle:@"H" forState:UIControlStateNormal];
    //[addButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    addButton.frame = CGRectMake(0, 0, 50, 50);
    
    [addButton setImage:[[UIImage imageNamed:@"ic-search.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    addButton.tintColor = [UIColor blackColor];
    [addButton addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
   _searchField.rightViewMode = UITextFieldViewModeAlways;
    _searchField.rightView = addButton;
    
    [self.cancelButton setTitle: [@"Cancel" localized] forState:UIControlStateNormal];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44.0;
    page =1;
    start_index =1;
    arrAds = [[NSMutableArray alloc]init];
    _tableView.layer.masksToBounds = NO;
    [_tableView registerNib:[UINib nibWithNibName:@"HomeViewCell" bundle:nil] forCellReuseIdentifier:@"HomeViewCell"];
    _viewTableView.layer.masksToBounds = YES;
}

-(void)initLoadmoreAndPullToRefresh{
    isLoading = false;
    _topRefreshTable = [UIRefreshControl new];
    _topRefreshTable.attributedTitle = [[NSAttributedString alloc] initWithString: [@"Pull down to reload!" localized] attributes:@{NSForegroundColorAttributeName:COLOR_PRIMARY_DEFAULT }];
    _topRefreshTable.tintColor = COLOR_PRIMARY_DEFAULT;
    [_topRefreshTable addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    
    [_tableView addSubview:_topRefreshTable];
    
    refreshControl = [UIRefreshControl new];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:[@"Pull up to load more!" localized] attributes:@{NSForegroundColorAttributeName:COLOR_PRIMARY_DEFAULT}];
   // refreshControl.triggerVerticalOffset = 60;
    [refreshControl addTarget:self action:@selector(loadMore) forControlEvents:UIControlEventValueChanged];
    refreshControl.tintColor = COLOR_PRIMARY_DEFAULT;
    self.tableView.bottomRefreshControl = refreshControl;
}

-(void)refreshData{
    if ([isLoading isEqualToString:@"1"]) {
        return;
    }else
        start_index = 1;
    isLoading = @"1";
//    [self performSelectorInBackground:@selector(getData) withObject:nil];
    [self getData];
}
-(void)loadMore{
    if ([isLoading isEqualToString:@"1"]) {
        return;
    }else{
        isLoading = @"1";
        if (start_index == page) {
            [self.view makeToast:[@"No more products" localized] duration:2.0 position:CSToastPositionCenter];
            [self finishLoading];
            return;
        }
        start_index+=1;
        [self performSelectorInBackground:@selector(getData) withObject:nil];
        
    }

}

-(void)getData{
    // NSString *Country = [Util stringForKey:KEY_COUNTRY];
    _paramDic[@"page"] = [NSString stringWithFormat:@"%d",start_index];
    _paramDic[@"sortBy"] = sortBy;
    _paramDic[@"sortType"] = sortType;
    _paramDic[@"keyword"] = self.searchField.text;
    _paramDic[@"country"] = @"";
    _paramDic[@"currency_code"] = [Util stringForKey:KEY_COUNRENCY_CODE];
    _paramDic[@"currency_symbol"] = [Util stringForKey:KEY_COUNRENCY];

    NSLog(@"updated params = %@", _paramDic);
    
    [MBProgressHUD showHUDAddedTo:self.viewTableView animated:YES];
   // [ModelManager searchAdsWithParam:_paramDic withSuccess:^(NSDictionary *dicReturn) {
    [ModelManager getAdsInPage:[NSString stringWithFormat:@"%d",start_index] sortType:sortType sortBy:sortBy searchBy:self.searchField.text params:_paramDic withSuccess:^(NSDictionary *dicReturn) {
        if (start_index == 1) {
            [arrAds removeAllObjects];
        }
        if (self) {
            dispatch_async(dispatch_get_main_queue(), ^{
                page = [dicReturn[@"allpage"] intValue];
            [arrAds addObjectsFromArray:dicReturn[@"arrAds"]];
            //arrAds = [[arrAds.copy arrayByAddingObjectsFromArray:dicReturn[@"arrAds"]] mutableCopy];
            [_tableView reloadData];
            [self finishLoading];
            
           });
        }
    } failure:^(NSString *error) {
        
        if (self) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self finishLoading];
                [self.view makeToast:error duration:3.0 position:CSToastPositionCenter];
            });
        }
    }];
}
-(void)finishLoading{
    [MBProgressHUD hideAllHUDsForView:self.viewTableView animated:YES];
    [_activityIndicator stopAnimating];
    _activityIndicator.hidden = YES;
    [_topRefreshTable endRefreshing];
    [self.tableView.bottomRefreshControl endRefreshing];
    isLoading = @"0";
}


-(void)setRevealBtn{
    SWRevealViewController *revealController = self.revealViewController;
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    [_revealBtn addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark UITABALEVIEW DATASOURCE

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return arrAds.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //  if (!((indexPath.row == estateArr.count)&&(estateArr.count == start_index))) {
    HomeViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HomeViewCell"];
    
    Ads *est = [arrAds objectAtIndex:indexPath.row];
    cell.lblTitle.text = est.adsTitle;
    [cell.imageAds setImageWithURL:[NSURL URLWithString:est.adsImage] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        
    }];
   // cell.lblStatusValue.hidden = YES;
    //cell.lblStatus.hidden = YES;
    //cell.imageView3.imageURL = [NSURL URLWithString:est.image];
//    if ([est.adsPrice isEqualToString:@"3"]) {
//        cell.lblPrice.text = [NSString stringWithFormat:@"$%@/%@",[Util convertPriceString:est.adsPriceValue],est.adsPriceUnit];
//    }
//    if ([est.adsPrice isEqualToString:@"1"]) {
//        cell.lblPrice.text = [NSString stringWithFormat: @"%@", [@"Free" localized]];
//    }
//    
//    if ([est.adsPrice isEqualToString:@"2"]) {
//        cell.lblPrice.text = [@"Negotiate" localized] ;
//    }
    
    if (![est.adsPriceValue isEqualToString:@""]) {
        cell.actualPriceLabel.text = [NSString stringWithFormat:@"%@ %@",est.adsCurrencyCode,est.adsPriceValue];
    } else {
        cell.actualPriceLabel.text = @" - ";
    }
    
    if([est.adsMileage isEqualToString:@""]) {
        cell.mileageLabel.text = @"-";
    } else {
        cell.mileageLabel.text = est.adsMileage;
    }
    
    
    /*NSDictionary *attributes = @{NSFontAttributeName:[UIFont fontWithName:cell.lblPrice.font.fontName size:cell.lblPrice.font.pointSize]};
    CGSize stringBoundingBox = [cell.lblPrice.text sizeWithAttributes:attributes];
    cell.lblPrice.frame = CGRectMake(15, cell.lblPrice.frame.origin.y, (stringBoundingBox.width+20)<90?90:(stringBoundingBox.width+20), 30);*/
    
    cell.lblAvaiable.hidden = NO;
    [cell.lblAvaiable setTransform:CGAffineTransformMakeRotation(-M_PI / 4)];
    if ([est.adsIsAvailable isEqualToString:@"1"]) {
        cell.lblAvaiable.hidden = YES;
    } else if ([est.adsIsAvailable isEqualToString:@"0"])
    {
        if([est.adsForRent isEqualToString:@"1"]){
            cell.lblAvaiable.text =  [@"RENTED" localized] ;
        }else if([est.adsForSale isEqualToString:@"1"]){
            cell.lblAvaiable.text =  [@"SOLD" localized] ;
        }
    }

    [cell.imageAds setContentMode:UIViewContentModeScaleAspectFill];
    cell.imageAds.clipsToBounds = YES;
    if ([est.adsForRent isEqualToString:@"1"]) {
        cell.lblRentOrSale.text = [@"For rent" localized] ;
    }
    if ([est.adsForSale isEqualToString:@"1"]) {
        cell.lblRentOrSale.text = [@"For sale" localized] ;
    }
    if (est.adsCategory) {
        cell.lblCategory.text = est.adsCategory;
    }else{
    cell.lblCategory.text = [@"All Categories" localized] ;
    }

    if (est.adsSub) {
        cell.lblSubCat.text = est.adsSub;
    }else{
    cell.lblSubCat.text = [@"All Sub Categories" localized] ;
    }
    
    if (est.adsCity) {
        if ([est.adsCountry isEqualToString:@""]) {
            cell.lblCity.text = est.adsCity;
        } else {
            if([est.adsCity isEqualToString:@""]) {
                cell.lblCity.text = est.adsCountry;
            } else {
                cell.lblCity.text = [NSString stringWithFormat:@"%@, %@", est.adsCity, est.adsCountry];
            }
        }
    
    }else{
        cell.lblCity.text = [@"All cities" localized] ;
    }

    cell.lblPosted.text = [est.adsDateCreated substringWithRange:NSMakeRange(0, 10)];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if([est.adsIsFeatured isEqualToString:@"1"]){
        cell.isFeatured.hidden = NO;
    }else{
        cell.isFeatured.hidden = YES;
    }
  
   // if (![est.estimate_price isEqualToString:@""] && ![est.adsPrice isEqualToString:@"1"] ) {
        cell.estimatePriceLabel.text = [NSString stringWithFormat:@"%@ %.2f",est.adsCurrencyCode,est.estimate_price.doubleValue];   // NSString(@"")est.estimate_price;
   // } else {
  //      cell.estimatePriceLabel.text = @" - ";
  //  }
    
 //   if (![est.adsPriceValue isEqualToString:@""] && ![est.adsPrice isEqualToString:@"1"]) {
    
        cell.actualPriceLabel.text = [NSString stringWithFormat:@"%@ %@",est.adsCurrencyCode,est.adsPriceValue];
 //   } else {
 //       cell.actualPriceLabel.text = @" - ";
 //   }
    
    
    if (![est.adsBrand isEqualToString:@""]) {
        cell.brandLabel.text = est.adsBrand;
    }else{
        cell.brandLabel.text = @"-";
    }
    
    if (![est.adsModel isEqualToString:@""]) {
        cell.modelLabel.text = est.adsModel;
    }else{
        cell.modelLabel.text = @"-";
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    Ads *est = [arrAds objectAtIndex:indexPath.row];
    DetailAdsViewController *VC = [[DetailAdsViewController alloc]initWithNibName:@"DetailAdsViewController" bundle:nil];
    VC.objAds = est;
    [self.navigationController pushViewController:VC animated:YES];
}

- (IBAction)onSearch:(id)sender {
    self.searchView.hidden = NO;
}

- (IBAction)onCity:(id)sender {
     [self setCountries];
    [self openCountrySelectView];
}
-(void)openCountrySelectView{
    CountryViewController *controller = [[CountryViewController alloc]initWithNibName:@"CountryViewController" bundle:nil];
    controller.delegate = self;
    NSMutableArray *arrName = [[NSMutableArray alloc]init];
    for (CityObj *city in arrCountries) {
        [arrName addObject:[Validator getSafeString:city.cityName]];
    }
    controller.arrayItems = arrName;
    [controller presentInParentViewController:self];
}
- (IBAction)onSort:(id)sender {
    optionsController  *optionsVC = [[optionsController alloc]initWithNibName:@"optionsController" bundle:nil];
    optionsVC.delegate = self;
    optionsVC.arrDataSource = @[[@"Filter" localized] ,
                                [@"Sort" localized] ];
    [optionsVC presentInParentViewController:self];
}

-(void)gotoFilter{
    SeachViewController *detail = [[SeachViewController alloc]initWithNibName:@"SeachViewController" bundle:nil];
    detail.delegate = self;
    detail.fromHome = true;
    [self.navigationController pushViewController:detail animated:YES];
}

-(void)filterApplyAction:(NSMutableDictionary *)params {
    NSLog(@"params = %@", params);
    self.paramDic = params;
    [self getData];
    
}
- (void)setSorting{
    SortViewController *sortVC = [[SortViewController alloc]initWithNibName:@"SortViewController" bundle:nil];
    sortVC.delegate = self;
    sortVC.arrDataSource = arrSortType;
    
    [sortVC presentInParentViewController:self];
}

-(void)sortViewDidSelectedItemAtIndex:(int)rowSelected{
    switch (rowSelected) {
        case 0:
            sortBy = SORT_BY_NAME;
            sortType = SORT_TYPE_ASC;
            break;
        case 1:
            sortBy = SORT_BY_NAME;
            sortType = SORT_TYPE_DESC;
            break;
        case 2:
            sortBy = SORT_BY_POSTED_DATE;
            sortType = SORT_TYPE_ASC;
            break;
        case 3:
            sortBy = SORT_BY_POSTED_DATE;
            sortType = SORT_TYPE_DESC;
            break;
        case 4:
            sortBy = SORT_BY_VIEW;
            sortType = SORT_TYPE_ASC;
            break;
        case 5:
            sortBy = SORT_BY_VIEW;
            sortType = SORT_TYPE_DESC;
            break;
            
        default:
            break;
    }
    start_index = 1;
    isLoading =@"1";
    [self getData];
    [_activityIndicator startAnimating];
    _activityIndicator.hidden =NO;

}

-(void)optionsViewDidSelectedItemAtIndex:(int)rowSelected {
    switch (rowSelected) {
        case 0:
            [self gotoFilter];
            break;
        case 1:
            [self setSorting];
            break;
            
        default:
            break;
    }
}

#pragma mark ==> delegate dropdown list
-(void)CountryViewControllerDidSelectedItem:(int)rowSelected{
     CityObj *priceObj = [[CityObj alloc]init];
     priceObj = [arrCountries objectAtIndex:rowSelected];
     self.homeLabel.text = [priceObj.cityName localized];
     [Util setObject:priceObj.cityName forKey:KEY_COUNTRY];
     [Util setObject:priceObj.cityId forKey:@"COUNTRYID"];
     [Util setObject:priceObj.cityCounrncy forKey:KEY_COUNRENCY];
     [Util setObject:priceObj.cityCurrencyCode forKey:KEY_COUNRENCY_CODE];
     NSLog(@"%@",priceObj.cityCounrncy);
     NSLog(@"%@",priceObj.cityId);
     [self getData];
}

@end
