//
//  HomeVC.h
//  Real Ads
//
//  Created by Hicom Solutions on 1/24/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "SortViewController.h"
#import "RevealViewController.h"
#import "optionsController.h"
#import "SeachViewController.h"

@interface HomeVC : UIViewController<UITableViewDataSource,UITableViewDelegate, SortViewDelegate, UITextFieldDelegate, optionsDelegate, FilterDelegate>
{
    int start_index;
    int page;
}
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UIView *viewTableView;
@property (strong, nonatomic) IBOutlet UIButton *revealBtn;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) UIRefreshControl *topRefreshTable;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UITextField *searchField;
@property (strong, nonatomic) NSMutableDictionary *paramDic;
- (void)setSorting;
@end
