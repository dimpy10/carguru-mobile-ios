

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "SortViewController.h"
#import "RevealViewController.h"
@interface MyBookMarksVC : UIViewController<UITableViewDataSource,UITableViewDelegate, SortViewDelegate>
{
    int start_index;
    int page;
}
@property (strong, nonatomic) IBOutlet UIButton *revealBtn;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) UIRefreshControl *topRefreshTable;
@property (weak, nonatomic) IBOutlet UIView *viewTblView;

@end
