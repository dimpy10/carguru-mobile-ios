
#import "MyBookMarksVC.h"
#import "HomeViewCell.h"

#import "Macros.h"
#import "DetailAdsViewController.h"
//#import "LoadMoreTableViewCell.h"
#import "UIView+Toast.h"
#import "UIScrollView+BottomRefreshControl.h"
#import "UIImageView+WebCache.h"

@interface MyBookMarksVC ()
{
    NSMutableArray *arrAds;
    BOOL isLoading;
    NSArray *arrSortType;
    NSString *sortBy,*sortType;
    
    
}
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imgNaviBg;
@end

@implementation MyBookMarksVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.headerLabel.text = [@"My Bookmarks" localized];
    [self initData];
    [self initLoadmoreAndPullToRefresh];
    [self setRevealBtn];
    //    [self.activityIndicator startAnimating];
    //    self.activityIndicator.hidden = NO;
}

-(void)viewWillAppear:(BOOL)animated{
    //    _activityIndicator.hidden = NO;
    //    [_activityIndicator startAnimating];
    [self performSelectorInBackground:@selector(refreshData) withObject:nil];
}

-(void)initData{
    arrSortType = @[[@"Sort by Name asc" localized],
                    [@"Sort by Name desc" localized],
                    [@"Sort by Date asc" localized],
                    [@"Sort by Date desc" localized],
                    [@"Number of Viewed acs" localized],
                    [@"Number of Viewed desc" localized]];
    self.view.backgroundColor = COLOR_BACKGROUD_VIEW;
    _imgNaviBg.backgroundColor = COLOR_DARK_PR_MARY;
    sortBy = SORT_BY_ALL;
    sortType = SORT_TYPE_NORMAL;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44.0;
    _tableView.layer.masksToBounds = NO;
    _viewTblView.layer.masksToBounds = YES;
    page =1;
    start_index =1;
    arrAds = [[NSMutableArray alloc]init];
}

-(void)initLoadmoreAndPullToRefresh{
    isLoading = false;
    _topRefreshTable = [UIRefreshControl new];
    _topRefreshTable.attributedTitle = [[NSAttributedString alloc] initWithString:[@"Pull down to reload!" localized] attributes:@{NSForegroundColorAttributeName:COLOR_PRIMARY_DEFAULT }];
    _topRefreshTable.tintColor = COLOR_PRIMARY_DEFAULT;
    [_topRefreshTable addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    [_tableView addSubview:_topRefreshTable];
    UIRefreshControl *refreshControl = [UIRefreshControl new];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:[@"Pull up to load more!" localized] attributes:@{NSForegroundColorAttributeName:COLOR_PRIMARY_DEFAULT}];
    //refreshControl.triggerVerticalOffset = 60;
    [refreshControl addTarget:self action:@selector(loadMore) forControlEvents:UIControlEventValueChanged];
    refreshControl.tintColor = COLOR_PRIMARY_DEFAULT;
    self.tableView.bottomRefreshControl = refreshControl;
}

-(void)refreshData{
    if (isLoading) {
        return;
    }else
        start_index = 1;
    isLoading = true;
    [self performSelectorInBackground:@selector(getData) withObject:nil];
}
-(void)loadMore{
    if (isLoading) {
        return;
    }else{
        isLoading = true;
    }
    if (start_index==page) {
        [self.view makeToast:[@"No more products" localized] duration:2.0 position:CSToastPositionCenter];
        [self finishLoading];
        return;
    }
    start_index+=1;
    [self performSelectorInBackground:@selector(getData) withObject:nil];
}

-(void)getData{
    
    if (gUser.usId) {
        [MBProgressHUD showHUDAddedTo:self.viewTblView animated:YES];
        [ModelManager getBookmarksAdsByUserId:gUser.usId andPage:[NSString stringWithFormat:@"%d",start_index] sortType:sortType sortBy:sortBy withSuccess:^(NSDictionary *dicReturn) {
            if (start_index == 1) {
                [arrAds removeAllObjects];
            }
            page = [dicReturn[@"allpage"] intValue];
            [arrAds addObjectsFromArray:dicReturn[@"arrAds"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self finishLoading];
                [_tableView reloadData];
            });
            
        } failure:^(NSString *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self finishLoading];
                
            });
            [self.view makeToast:error duration:3.0 position:CSToastPositionCenter];
            
        }];}else{
            [self finishLoading];
            [self.view makeToast:[@"Please login to use this function" localized] duration:2.0 position:CSToastPositionCenter];
            
        }
    
    //
    //    } failure:^(NSString *error) {
    //
    //    }];
}
-(void)finishLoading{
    [MBProgressHUD hideAllHUDsForView:self.viewTblView animated:YES];
    [_topRefreshTable endRefreshing];
    [self.tableView.bottomRefreshControl endRefreshing];
    isLoading = false;
}


-(void)setRevealBtn{
    SWRevealViewController *revealController = self.revealViewController;
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    [_revealBtn addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
}

#pragma mark UITABALEVIEW DATASOURCE

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return arrAds.count;
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //  if (!((indexPath.row == estateArr.count)&&(estateArr.count == start_index))) {
    HomeViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HomeViewCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"HomeViewCell" owner:nil options:nil] objectAtIndex:0];
    }
    
    
    Ads *est = [arrAds objectAtIndex:indexPath.row];
    cell.lblTitle.text = est.adsTitle;
    [cell.imageAds setImageWithURL:[NSURL URLWithString:est.adsImage] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        
    }];
    //cell.lblStatusValue.hidden = YES;
    //cell.lblStatus.hidden = YES;
    //cell.imageView3.imageURL = [NSURL URLWithString:est.image];
    if ([est.adsPrice isEqualToString:@"3"]) {
        cell.lblPrice.text = [NSString stringWithFormat:@"$%@/%@",est.adsPriceValue,est.adsPriceUnit];
    }
    if ([est.adsPrice isEqualToString:@"1"]) {
        cell.lblPrice.text = [NSString stringWithFormat:@"%@", [@"Free" localized]];
    }
    
    if ([est.adsPrice isEqualToString:@"2"]) {
        cell.lblPrice.text = [@"Negotiate" localized];
    }
    cell.lblAvaiable.hidden = NO;
    [cell.lblAvaiable setTransform:CGAffineTransformMakeRotation(-M_PI / 4)];
    if ([est.adsIsAvailable isEqualToString:@"1"]) {
        cell.lblAvaiable.hidden = YES;
    } else if ([est.adsIsAvailable isEqualToString:@"0"])
    {
        if([est.adsForRent isEqualToString:@"1"]){
            cell.lblAvaiable.text =  [@"RENTED" localized];
        }else if([est.adsForSale isEqualToString:@"1"]){
            cell.lblAvaiable.text =  [@"SOLD" localized];
        }
    }
    
    [cell.imageAds setContentMode:UIViewContentModeScaleAspectFill];
    cell.imageAds.clipsToBounds = YES;
    if ([est.adsForRent isEqualToString:@"1"]) {
        cell.lblRentOrSale.text = [@"For rent" localized];
    }
    if ([est.adsForSale isEqualToString:@"1"]) {
        cell.lblRentOrSale.text = [@"For sale" localized];
    }
    if (est.adsCategory) {
        cell.lblCategory.text = est.adsCategory;
    }else{
        cell.lblCategory.text = [@"All Categories" localized];
    }
    
    if (est.adsSub) {
        cell.lblSubCat.text = est.adsSub;
    }else{
        cell.lblSubCat.text = [@"All Sub Categories" localized];
    }
    
    if (est.adsCity) {
        cell.lblCity.text = est.adsCity;
    }else{
        cell.lblCity.text = [@"All cities" localized];
    }
    
    cell.lblPosted.text = [est.adsDateCreated substringWithRange:NSMakeRange(0, 10)];
    if([est.adsIsFeatured isEqualToString:@"1"]){
        cell.isFeatured.hidden = NO;
    }else{
        cell.isFeatured.hidden = YES;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Ads *est = [arrAds objectAtIndex:indexPath.row];
    DetailAdsViewController *VC = [[DetailAdsViewController alloc]initWithNibName:@"DetailAdsViewController" bundle:nil];
    VC.objAds = est;
    [self.navigationController pushViewController:VC animated:YES];
}

- (IBAction)onSort:(id)sender {
    
    SortViewController *sortVC = [[SortViewController alloc]initWithNibName:@"SortViewController" bundle:nil];
    sortVC.delegate = self;
    sortVC.arrDataSource = arrSortType;
    [sortVC presentInParentViewController:self];
    
    
}

-(void)sortViewDidSelectedItemAtIndex:(int)rowSelected{
    switch (rowSelected) {
        case 0:
            sortBy = SORT_BY_NAME;
            sortType = SORT_TYPE_ASC;
            break;
        case 1:
            sortBy = SORT_BY_NAME;
            sortType = SORT_TYPE_DESC;
            break;
        case 2:
            sortBy = SORT_BY_POSTED_DATE;
            sortType = SORT_TYPE_ASC;
            break;
        case 3:
            sortBy = SORT_BY_POSTED_DATE;
            sortType = SORT_TYPE_DESC;
            break;
        case 4:
            sortBy = SORT_BY_VIEW;
            sortType = SORT_TYPE_ASC;
            break;
        case 5:
            sortBy = SORT_BY_VIEW;
            sortType = SORT_TYPE_DESC;
            break;
            
        default:
            break;
    }
    start_index =1;
    isLoading = YES;
    [self getData];
    
}

@end
