

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "JVFloatLabeledTextField.h"
#import "JVFloatLabeledTextView.h"


@interface MyAccountVC : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    BOOL female;
    BOOL male;
    BOOL isPrivate; 
}
@property (strong, nonatomic) NSString *sex;
@property (strong, nonatomic) UIImagePickerController *imagePicker;
@property (retain, nonatomic) UIPopoverController* libraryPopover;
@property (strong, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scollview;

@property (strong, nonatomic) IBOutlet UITextField *txtName;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtPhone;

@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *txtAddress;
@property (strong, nonatomic) IBOutlet UITextField *txtSkype;
@property (strong, nonatomic) IBOutlet UIButton *btnRegister;
- (IBAction)OnRegister:(id)sender;
- (IBAction)OnBack:(id)sender;
- (IBAction)OnImage:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnCamera;


@property (strong, nonatomic) IBOutlet UIImageView *imgAvartar;
@property (strong, nonatomic) IBOutlet UIButton *btnImage;
@property (strong, nonatomic) IBOutlet UIView *ViewUpImage;
@property (strong, nonatomic) IBOutlet UIButton *btnGallerty;
- (IBAction)OnGallery:(id)sender;
- (IBAction)OnCamera:(id)sender;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentGender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentCompany;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *segmentGenderHeight;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfWebsite;


@end
