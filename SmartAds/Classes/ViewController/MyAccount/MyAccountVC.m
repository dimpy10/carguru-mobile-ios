
//
//  Created by Mac on 5/13/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "MyAccountVC.h"
#import "ModelManager.h"
#import "UIView+Toast.h"
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"


@interface MyAccountVC ()
@property (weak, nonatomic) IBOutlet UIImageView *imgNavi;
@property (weak, nonatomic) IBOutlet UIButton *btnReveal;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UILabel *uploadPictureLabel;
@property (weak, nonatomic) IBOutlet UILabel *imageMessageLabel;

@end

@implementation MyAccountVC
@synthesize imagePicker;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.headerLabel.text = [@"My Account" localized];
    self.uploadPictureLabel.text = [@"Upload Pictures Option" localized];
    self.imageMessageLabel.text = [@"How do you want to set your picture?" localized];
    [self.btnGallerty setTitle:[@"Gallery" localized] forState:UIControlStateNormal];
    [self. btnCamera setTitle:[@"Camera" localized] forState:UIControlStateNormal];
    
    [self.segmentCompany setTitle:[@"INDIVIDUAL" localized] forSegmentAtIndex:0];
    [self.segmentCompany setTitle:[@"COMPANY" localized] forSegmentAtIndex:1];
    
    [self.segmentGender setTitle:[@"MALE" localized] forSegmentAtIndex:0];
    [self.segmentGender setTitle:[@"FEMALE" localized] forSegmentAtIndex:1];
    
    [self.btnRegister setTitle:[@"UPDATE" localized] forState:UIControlStateNormal];
    
    [self setRevealBtn];
    [self setupLayout];

}
-(void)setupLayout{
    [_txtEmail setAllowsEditingTextAttributes:NO];
   
    _txtName.placeholder = [@"Full name" localized];
    _txtEmail.placeholder = [@"Email" localized];
    _txtPhone.placeholder = [@"Phone number" localized];
    _txtAddress.placeholder = [@"Address" localized];
    _txtSkype.placeholder = [@"Skype" localized];
    _tfWebsite.placeholder = [@"Website" localized];
    self.view.backgroundColor = [UIColor whiteColor];
    _imgNavi.backgroundColor = COLOR_DARK_PR_MARY;
    _btnRegister.backgroundColor = COLOR_BTN_SMALL;
    _segmentCompany.tintColor = COLOR_SEGMENTTINT;
    _segmentGender.tintColor = COLOR_SEGMENTTINT;
    _btnGallerty.backgroundColor  = COLOR_BTN_SMALL;
    _btnCamera.backgroundColor  = COLOR_BTN_SMALL;
    _segmentGender.selectedSegmentIndex =0;
    _tfWebsite.hidden = YES;
    _segmentGender.hidden = NO;
    _segmentCompany.selectedSegmentIndex = 0;
    [[self.btnRegister layer]setCornerRadius:4];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(doSingleTap:)];
    tap.numberOfTapsRequired = 1;
    [self.ViewUpImage addGestureRecognizer:tap];
    // self.ViewImage.hidden = YES;
    self.ViewUpImage.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
    // male = YES;
    self.sex= @"1";
    [self.imgAvartar setContentMode:UIViewContentModeScaleAspectFill];
    self.imgAvartar.clipsToBounds = YES;
    self.txtName.text = gUser.usName;
    self.txtEmail.text = gUser.usEmail;
    self.txtSkype.text = gUser.usSkype;
    self.txtAddress.text = gUser.usAddress;
    self.txtPhone.text = gUser.usPhone;
    _tfWebsite.text = gUser.usWebsite;
    if ([gUser.usIndividual isEqualToString:@"0"]) {
        _segmentCompany.selectedSegmentIndex = 0;
        _tfWebsite.hidden = YES;
        _segmentGender.hidden = NO;
    }else{
        _segmentCompany.selectedSegmentIndex =1;
        _tfWebsite.hidden = NO;
        _segmentGender.hidden = YES;
    }
    if ([gUser.usSex isEqualToString:@"0"]) {
        _segmentGender.selectedSegmentIndex =1;
    }else{
        _segmentGender.selectedSegmentIndex =0;
    }
    [self.imgAvartar setImageWithURL:[NSURL URLWithString:gUser.usImage] placeholderImage:IMAGE_HODER];
}

-(void)setRevealBtn{
    SWRevealViewController *revealController = self.revealViewController;
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    [_btnReveal addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
}
- (void) doSingleTap :(UIGestureRecognizer *)gesture
{
    [self.ViewUpImage setHidden:YES];
}

- (IBAction)OnRegister:(id)sender { 
    [self.view endEditing:YES];
    if(!self.txtEmail.text.length ||!self.txtName.text.length ||!self.txtAddress.text.length ||!self.txtPhone.text.length || !self.txtSkype.text.length ){
        [self.view makeToast:[@"Some field is missing. Please fill all required field!" localized] duration:2.0 position:CSToastPositionCenter];
    }else{
      
            NSData* imageData = UIImageJPEGRepresentation(self.imgAvartar.image, 0.5);
            if(_segmentGender.selectedSegmentIndex ==0){
                _sex = @"1";
            }else{
                _sex =@"0";
            }
        NSString *individual = [[NSString alloc]init];
        if (_segmentGender.selectedSegmentIndex ==0) {
            individual =@"0";
        }else{
            individual =@"1";
            if (!self.tfWebsite.text.length) {
                [self.view makeToast:[@"Website is missing. Please fill all required field!" localized] duration:2.0 position:CSToastPositionCenter];
                return;
            }
        }
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [ModelManager editAccountInfoWithName:_txtName.text phone:_txtPhone.text address:_txtAddress.text sex:_sex skype:_txtSkype.text web:_tfWebsite.text individual:individual dataImgAvatar:imageData withSuccess:^(NSString *successStr) {
            
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

            [self.view makeToast:successStr duration:2.0 position:CSToastPositionCenter];
        } failure:^(NSString *err) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [self.view makeToast:err duration:2.0 position:CSToastPositionCenter];
        }];

      
    }
}
#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0){
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)OnImage:(id)sender {
    [self.ViewUpImage setHidden:NO];
}
- (IBAction)OnGallery:(id)sender {
    [_ViewUpImage setHidden:YES];
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    [self presentViewController:imagePicker animated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    [imagePicker dismissViewControllerAnimated:YES completion:nil];
    
    self.imgAvartar.image = image;
}

//  On cancel, only dismiss the picker controller
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [imagePicker dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)OnCamera:(id)sender {
    [_ViewUpImage setHidden:YES];
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.showsCameraControls = YES;
    UIImage *image = [UIImage imageNamed:@"bg_camera.png"] ;
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 30, SCREEN_WIDTH, SCREEN_WIDTH)] ;
    imgView.image = image;
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    imagePicker.cameraOverlayView = imgView;
    
    [self presentViewController:imagePicker animated:YES completion:NULL];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (IBAction)segmentCompanyChanged:(id)sender {
    if (_segmentCompany.selectedSegmentIndex ==0) {
        
        _tfWebsite.hidden = YES;
        _segmentGender.hidden = NO;
    }else{
        _tfWebsite.hidden = NO;
        _segmentGender.hidden = YES;
    }
}

@end
