//
//  MyAgentCell.m
//  Real Estate
//
//  Created by Hicom on 3/5/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import "MyAgentCell.h"

@implementation MyAgentCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
     _thumnails.contentMode = UIViewContentModeScaleAspectFill;
    _thumnails.clipsToBounds = YES;
     _viewCell.layer.cornerRadius = 6;

    self.viewCell.layer.borderColor = COLOR_DEVIDER.CGColor;;
    self.viewCell.clipsToBounds = YES;
    self.viewCell.layer.borderWidth = 0.5;
    self.memberSince.text = [@"Member since" localized];
    
    [self.btnContact setTitle:[@"CONTACT" localized] forState:UIControlStateNormal];
    [self.btnAds setTitle:[@"ADS" localized] forState:UIControlStateNormal];
}

- (IBAction)actionContact:(id)sender {
}

- (IBAction)actionAds:(id)sender {
}

-(void)configureCellForSeller:(User *)seller{
    _agentNameLabel.text = seller.usName;
    if (seller.usDateCreated.length >4) {
        _agentDateLabel.text = [seller.usDateCreated substringWithRange:NSMakeRange(0, 4)];
    }
    if ([seller.usIndividual isEqualToString:@"1"]) {
        _agentInfoLabel.text = [@"Individual" localized];
    }else{
        _agentInfoLabel.text = [@"Company" localized];
    }
    
}

@end
