//
//  AllAgentViewController.m
//  Real Estate
//
//  Created by Hicom on 3/5/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import "AllAgentViewController.h"
#import "AllAdsViewController.h"
#import "User.h"
#import "MyAgentCell.h"
#import "NothingCell.h"
#import "UITableView+DragLoad.h"
#import "UIScrollView+BottomRefreshControl.h"
#import "UIView+Toast.h"
#import "UIImageView+WebCache.h"
#import "ContactViewController.h"

@interface AllAgentViewController ()< UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *topNaviView;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSString *NumberPage;
    @property (weak, nonatomic) IBOutlet UILabel *headerLabel;
    
@end

@implementation AllAgentViewController{
    NSString *searchText;
    NSMutableArray *arrNews;
    int allPage,currentPage;
    BOOL isLoading;
    NSString *sortType,*sortBy;
    NSArray* arrSortType;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.headerLabel.text = [@"Sellers" localized];
    self.searchTextField.placeholder = [@"Search" localized];
    self.view.backgroundColor = COLOR_BACKGROUD_VIEW;
    _viewSearch.clipsToBounds = YES;
    _viewSearch.layer.cornerRadius = 6;
    _viewSearch.layer.borderColor = COLOR_DEVIDER.CGColor;;
    _viewSearch.layer.borderWidth = 0.5;
    [self setupPullToRefresh];
    arrSortType = @[[@"Sort by Name asc" localized],
                    [@"Sort by Name desc" localized],
                    [@"Register Date asc" localized],
                    [@"Register Date desc" localized],
                    [@"Verified First" localized],
                    [@"UnVerified First" localized],
                    [@"Number of Ads acs" localized],
                    [@"Number of Ads desc" localized],
                    [@"Individual First" localized],
                    [@"Company First" localized] ];
    sortBy = SORT_BY_ALL;
    sortType = SORT_TYPE_NORMAL;

    _searchTextField.delegate = self;
    self.topNaviView.backgroundColor = COLOR_DARK_PR_MARY;
    UINib *nib = [UINib nibWithNibName:@"MyAgentCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"MyAgentCell"];
    nib = [UINib nibWithNibName:@"NothingCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"NothingCell"];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
  //  [_indicator startAnimating];
    _indicator.hidden = YES;
    [self performSelectorInBackground:@selector(onSearch:) withObject:nil];
}
-(void)setupPullToRefresh{
    currentPage =1;
    allPage =1;
    arrNews = [[NSMutableArray alloc]init];
    sortBy = 0;
    sortType= @"0";
    isLoading = false;
    _topNaviView.backgroundColor = COLOR_DARK_PR_MARY;
    
    _topRefreshTable = [UIRefreshControl new];
    _topRefreshTable.attributedTitle = [[NSAttributedString alloc] initWithString:[@"Pull down to reload!" localized] attributes:@{NSForegroundColorAttributeName:COLOR_PRIMARY_DEFAULT}];
    _topRefreshTable.tintColor = COLOR_PRIMARY_DEFAULT;
    [_topRefreshTable addTarget:self action:@selector(onRefreshData) forControlEvents:UIControlEventValueChanged];
    [_tableView addSubview:_topRefreshTable];
    
    UIRefreshControl *refreshControl = [UIRefreshControl new];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:[@"Pull up to load more!" localized] attributes:@{NSForegroundColorAttributeName:COLOR_PRIMARY_DEFAULT}];
   // refreshControl.triggerVerticalOffset = 60;
    [refreshControl addTarget:self action:@selector(onLoadmore) forControlEvents:UIControlEventValueChanged];
    refreshControl.tintColor = COLOR_PRIMARY_DEFAULT;
    self.tableView.bottomRefreshControl = refreshControl;
    
}

-(void)onRefreshData{
    if (isLoading) {
        return;
    }else
        currentPage = 1;
    isLoading = YES;
    [self performSelectorInBackground:@selector(getData) withObject:nil];
    
}
-(void)onLoadmore{
    if (isLoading) {
        return;
    }else{
        isLoading = true;
    }
    if (currentPage==allPage) {
        [self.view makeToast:[@"No more products" localized] duration:2.0 position:CSToastPositionCenter];
        [self finishLoading];
        return;
    }
    currentPage+=1;
    [self performSelectorInBackground:@selector(getData) withObject:nil];
    
}

-(void)finishLoading{
    [MBProgressHUD hideAllHUDsForView:self.viewTblView animated:YES];
    [_topRefreshTable endRefreshing];
    [self.tableView.bottomRefreshControl endRefreshing];
    isLoading = false;
}

#pragma mark - Handle action

- (IBAction)actionSort:(id)sender {
    SortViewController *sortVC = [[SortViewController alloc]initWithNibName:@"SortViewController" bundle:nil];
    sortVC.delegate = self;
    sortVC.arrDataSource = arrSortType;
    [sortVC presentInParentViewController:self];
    
}

- (IBAction)actionLeftMenu:(id)sender {
    [self.revealViewController revealToggle:nil];

}
- (IBAction)onSearch:(id)sender {
    [self.searchTextField resignFirstResponder];
    currentPage = 1;
    [self getData];

}

-(void)getData{
    [MBProgressHUD showHUDAddedTo:self.viewTblView animated:YES];
    [ModelManager getListSellerWithPage:[NSString stringWithFormat:@"%d",currentPage] sortType:sortType sortBy:sortBy searchKey:_searchTextField.text withSuccess:^(id dicReturn) {
        if (currentPage == 1) {
            [arrNews removeAllObjects];
        }
        if (self) {
            dispatch_async(dispatch_get_main_queue(), ^{
                arrNews = [arrNews arrayByAddingObjectsFromArray:dicReturn[@"arrAcc"]].mutableCopy;
                allPage = [dicReturn[@"allpage"] intValue];
                [self finishLoading];
                [_tableView reloadData];
            });
            
        }
        
    } failure:^(NSString *error) {
        if (self) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self finishLoading];
                [self.view makeToast:error duration:2.0 position:CSToastPositionCenter];
            });
            
        }
        
    }];
}

#pragma mark - TableView DataSource

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (arrNews.count == 0) {
        return 44;
    }else{
        return 130;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (arrNews.count == 0) {
        return 1;
    }else{
        return arrNews.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (arrNews.count == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NothingCell"];
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }else{
        MyAgentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyAgentCell"];
        User *userObj = arrNews[indexPath.row];
        [cell configureCellForSeller:userObj];
        [cell.thumnails setImageWithURL:[NSURL URLWithString:userObj.usImage] placeholderImage:IMAGE_HODER];
        cell.btnContact.tag = indexPath.row;
        [cell.btnContact addTarget:self action:@selector(onContact:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnAds.tag = indexPath.row;
        [cell.btnAds addTarget:self action:@selector(onAds:) forControlEvents:UIControlEventTouchUpInside];
        cell.backgroundColor = [UIColor clearColor];
        if ([userObj.usIsverified isEqualToString:@"1"]) {
            cell.imgVerified.hidden = NO;
        }else{
            cell.imgVerified.hidden = YES;
        }
        
        return cell;
    }
    
}

-(IBAction)onContact:(id)sender{
    User *objSeller = arrNews[[sender tag]];
    ContactViewController *contactVC = [[ContactViewController alloc]initWithNibName:@"ContactViewController" bundle:nil];
        contactVC.seller = objSeller;
    [self.navigationController pushViewController:contactVC animated:YES];

}
-(IBAction)onAds:(id)sender{
    User *objSeller = arrNews[[sender tag]];
    AllAdsViewController *allAdsVC = [[AllAdsViewController alloc] initWithNibName:@"AllAdsViewController" bundle:nil];
    allAdsVC.sellerId = objSeller.usId;
    allAdsVC.lblTitle.text = [NSString stringWithFormat:@"%@'s Ads",objSeller.usName];
    [self.navigationController pushViewController:allAdsVC animated:YES];
    

}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    [self onSearch:nil];
    return YES;
}

-(void)sortViewDidSelectedItemAtIndex:(int)rowSelected{
    switch (rowSelected) {
        case 0:
            sortBy = SORT_BY_NAME;
            sortType = SORT_TYPE_ASC;
            break;
        case 1:
            sortBy = SORT_BY_NAME;
            sortType = SORT_TYPE_DESC;
            break;
        case 2:
            sortBy = SORT_BY_REGISTERED_DATE;
            sortType = SORT_TYPE_ASC;
            break;
        case 3:
            sortBy = SORT_BY_REGISTERED_DATE;
            sortType = SORT_TYPE_DESC;
            break;
        case 4:
            sortBy = SORT_BY_VERIFIED;
            sortType = SORT_TYPE_ASC;
            break;
        case 5:
            sortBy = SORT_BY_VERIFIED;
            sortType = SORT_TYPE_DESC;
            break;
        case 6:
            sortBy = SORT_BY_NUMBER_OF_ADS;
            sortType = SORT_TYPE_ASC;
            break;
        case 7:
            sortBy = SORT_BY_NUMBER_OF_ADS;
            sortType = SORT_TYPE_DESC;
            break;
        case 8:
            sortBy = SORT_BY_INDIVIDUAL;
            sortType = SORT_TYPE_NORMAL;
            break;
        case 9:
            sortBy = SORT_BY_COMPANY;
            sortType = SORT_TYPE_NORMAL;
            break;
            
        default:
            break;
    }
    currentPage =1;
    isLoading = YES;
    [self getData];
    [_indicator startAnimating];
    _indicator.hidden =NO;

}


@end
