//
//  AllAgentViewController.h
//  Real Estate
//
//  Created by Hicom on 3/5/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SortViewController.h"
@interface AllAgentViewController : UIViewController <SortViewDelegate>
@property (nonatomic, strong) UIRefreshControl *topRefreshTable;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIView *viewTblView;
@property (strong, nonatomic) IBOutlet UIView *viewSearch;
@end
