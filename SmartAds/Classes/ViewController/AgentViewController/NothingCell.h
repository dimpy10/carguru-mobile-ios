//
//  NothingCell.h
//  Real Estate
//
//  Created by Hicom on 3/5/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NothingCell : UITableViewCell
    @property (weak, nonatomic) IBOutlet UILabel *nothingFound;
    
@end
