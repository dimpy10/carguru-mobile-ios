//
//  NothingCell.m
//  Real Estate
//
//  Created by Hicom on 3/5/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import "NothingCell.h"

@implementation NothingCell

- (void)awakeFromNib {

    self.nothingFound.text = [@"Nothing Found" localized];
}



@end
