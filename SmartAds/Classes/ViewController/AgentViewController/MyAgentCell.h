//
//  MyAgentCell.h
//  Real Estate
//
//  Created by Hicom on 3/5/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface MyAgentCell : UITableViewCell

    @property (weak, nonatomic) IBOutlet UILabel *memberSince;
    @property (weak, nonatomic) IBOutlet UIImageView *thumnails;
@property (weak, nonatomic) IBOutlet UILabel *agentNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *agentDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *agentInfoLabel;

-(void)configureCellForSeller:(User*)seller;
@property (weak, nonatomic) IBOutlet UIButton *btnContact;
@property (weak, nonatomic) IBOutlet UIButton *btnAds;

@property (weak, nonatomic) IBOutlet UIView *viewCell;
@property (weak, nonatomic) IBOutlet UIImageView *imgVerified;


@end
