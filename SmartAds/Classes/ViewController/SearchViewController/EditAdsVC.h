

#import <UIKit/UIKit.h>
#import "RBImagePickerController.h"
#import "TPKeyboardAvoidingScrollView.h"

@protocol EditAdsDelegate <NSObject>

-(void)didDeleteAds:(id)adsObj;

@end

@interface EditAdsVC : UIViewController<RBImagePickerDataSource, RBImagePickerDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate, UINavigationBarDelegate>
@property (strong, nonatomic) RBImagePickerController *imagepicker;
@property (strong, nonatomic) UIImagePickerController *imagePic;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *viewKeyboard;
@property (strong, nonatomic) Ads *adsObj;
@property (weak, nonatomic) IBOutlet UIButton *btnUpdate;
@property (weak, nonatomic) IBOutlet UIButton *btnSubCribe;
@property (weak, nonatomic) id<EditAdsDelegate> delegate;

@end
