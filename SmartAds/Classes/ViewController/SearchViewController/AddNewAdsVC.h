//
//  SeachViewController.h
//  Real Estate
//
//  Created by Hicom on 2/20/16.
//  Copyright © 2016 2016 TuanVN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RBImagePickerController.h"

@interface AddNewAdsVC : UIViewController<RBImagePickerDataSource, RBImagePickerDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate, UINavigationBarDelegate>
@property (strong, nonatomic) RBImagePickerController *imagepicker;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UIButton *getEstimateButton;
@property (weak, nonatomic) IBOutlet UILabel *estimateLabel;

@property (strong, nonatomic) UIImagePickerController *imagePic;
@property (weak, nonatomic) IBOutlet UIButton *btnIsAvailable;
@end
