//
//  SeachViewController.h
//  Real Estate
//
//  Created by Hicom on 2/20/16.
//  Copyright © 2016 2016 TuanVN. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol FilterDelegate <NSObject>
-(void)filterApplyAction:(NSMutableDictionary*)params;
@end

@interface SeachViewController : UIViewController
@property (nonatomic, weak) id<FilterDelegate> delegate;
@property (nonatomic) BOOL fromHome;
@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

- (void)setBack;
@end
