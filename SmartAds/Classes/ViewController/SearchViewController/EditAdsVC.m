//
//  SeachViewController.m
//  Real Estate
//
//  Created by Hicom on 2/20/16.
//  Copyright © 2016 TuanVN. All rights reserved.
//

#import "EditAdsVC.h"
#import "JVFloatLabeledTextView.h"
#import "FieldAlertController.h"
#import "DetailAdsCollectionCellImg.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "SearchOptionsViewController.h"
#import "SearchDateViewController.h"
#import "SearchAdsResultVC.h"
#import "CategoryPr.h"
#import "ImageObj.h"
#import "CityObj.h"
#import "UIView+Toast.h"
#import "UIImageView+WebCache.h"
#import "SubcriberViewController.h"
#import "MyAdsVC.h"
#import "HomeVC.h"
#define SELECT_PARENT_CATEGORY @"PARENT_CATEGORY"
#define SELECT_SUB_CATEGORY     @"SUB_CATEGORY"
#define SELECT_CITY             @"SELECT_CITY"
#define PRICE_FREE              @"1"
#define PRICE_NEGO              @"2"
#define PRICE_CUSTOM            @"3"
#define FOR_RENT                1
#define FOR_BUY                 0
#define SELECT_COUNTRY            @"SELECT_COUNTRY"
#define SELECT_YEARS            @"SELECT_YEARS"
#define SELECT_PRICE            @"SELECT_PRICE"
#define SELECT_MILEAGE            @"SELECT_MILEAGE"
#define SELECT_TRANS            @"SELECT_TRANS"
#define SELECT_TRIM            @"SELECT_TRIM"
#define SELECT_COLOR            @"SELECT_COLOR"
#define SELECT_FEATURE            @"SELECT_FEATURE"

@interface EditAdsVC () <UITextFieldDelegate, SearchOptionsViewControllerDelegate, SearchDateViewControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource,FieldAlertDelegate,SWRevealViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btnisAvailable;

@property (weak, nonatomic) IBOutlet UIImageView *imgImage;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControler;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *subCategoriesLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;

@property (weak, nonatomic) IBOutlet UIView *vTopNavi;
@property (weak, nonatomic) IBOutlet UITextField *tfTitle;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionGalleryView;
@property (weak, nonatomic) IBOutlet UITextField *tfPrice;
@property (weak, nonatomic) IBOutlet UITextField *tfUnit;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextView *tvContent;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;


@property (weak, nonatomic) IBOutlet UIButton *btnFreeType;
@property (weak, nonatomic) IBOutlet UIButton *btnNegotile;
@property (weak, nonatomic) IBOutlet UIButton *btnCustom;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UILabel *available;



//@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
//@property (weak, nonatomic) IBOutlet UILabel *subCategoriesLabel;
@property (weak, nonatomic) IBOutlet UILabel *countryLabel;
@property (weak, nonatomic) IBOutlet UILabel *yearsLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *mileageLabel;
@property (weak, nonatomic) IBOutlet UILabel *transLabel;
@property (weak, nonatomic) IBOutlet UILabel *trimLabel;
@property (weak, nonatomic) IBOutlet UILabel *colorLabel;
@property (weak, nonatomic) IBOutlet UILabel *featureLabel;
@property (weak, nonatomic) IBOutlet UILabel *brandLabel;
@property (weak, nonatomic) IBOutlet UILabel *ModelLabel;

@end

@implementation EditAdsVC{
    NSString *selectedType;
    NSString *oldGallery, *priceType;
    NSString* catParentId, *catSubId, *cityId,*countryId,*companyType, *beforeOrAfter, *yearsId,*priceId,*mileageId,*transId,*trimId,*colorId,*featureId, *brandId, *modelId;

    NSDate *selectedDate;
    NSString *textSearch;
    NSArray  *arrAllCategories;
    NSString *a,*b,*c;
    BOOL isCamera;
    ImageObj *objImageAvar;
    BOOL isSelectedAvatar;
    NSMutableArray *arrCities, *arrCountries, *arrParentCategories, *arrSubCategories, *arrUrlGallery, *arrDataGallery, *arrYears, *arrPrice, *arrMileage, *arrTrans, *arrTrim, *arrColor, *arrFeatures,*arrObjImageGallery;

    NSString *Price , *Mileage , *Countries , *From , *To , *Cities , *Transmission , *colors , *brands , *trim;

}
- (IBAction)onIsAvailable:(id)sender {
    _btnisAvailable.selected =!_btnisAvailable.selected;
}
//pick image
- (IBAction)onAddImage:(id)sender {
    [self showAlertChoseCameraOrGallery];
}
- (IBAction)onAddGallery:(id)sender {
    
    //[self.collectionview reloadData];
    [self presentViewController:self.imagepicker animated:YES completion:nil];
}
-(void)showAlertChoseCameraOrGallery{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[@"Upload picture option" localized] message:[@"How do you want to upload picture?" localized] preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:[@"Camera" localized] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        _imagePic = [[UIImagePickerController alloc] init];
        _imagePic.delegate = self;
        _imagePic.allowsEditing = YES;
        _imagePic.sourceType = UIImagePickerControllerSourceTypeCamera;
        _imagePic.showsCameraControls = YES;
        UIImage *image = [UIImage imageNamed:@"bg_camera.png"] ;
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 30, SCREEN_WIDTH, SCREEN_WIDTH*9/16)] ;
        imgView.image = image;
        imgView.contentMode = UIViewContentModeScaleAspectFit;
        _imagePic.cameraOverlayView = imgView;
        
        [self presentViewController:_imagePic animated:YES completion:NULL];
        
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:[@"Galery" localized] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        _imagePic = [[UIImagePickerController alloc] init];
        _imagePic.delegate = self;
        _imagePic.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        [self presentViewController:_imagePic animated:YES completion:nil];
        
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:[@"Cancel" localized] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self closeAlertview];
    }]];
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self presentViewController:alertController animated:YES completion:nil];
    });
}

- (IBAction)onFreeType:(id)sender {
    _btnFreeType.selected = YES;
    _btnCustom.selected = NO;
    _btnNegotile.selected = NO;
    _tfUnit.enabled = NO;
    _tfPrice.enabled = NO;
    priceType = PRICE_FREE;
    
}
- (IBAction)onNegotileType:(id)sender {
    _btnFreeType.selected = NO;
    _btnCustom.selected = NO;
    _btnNegotile.selected = YES;
    _tfUnit.enabled = NO;
    _tfPrice.enabled = NO;
    priceType = PRICE_NEGO;
}
- (IBAction)onCustomType:(id)sender {
    _btnFreeType.selected = NO;
    _btnCustom.selected = YES;
    _btnNegotile.selected = NO;
    _tfUnit.enabled = YES;
    _tfPrice.enabled = YES;
    priceType = PRICE_CUSTOM;
}

- (IBAction)onSubcribe:(id)sender {
    if ([self.adsObj.adsStatus isEqualToString:@"0"]) {
        [self.view makeToast:[@"You cannot subscription for draft property!" localized] duration:2.0 position:CSToastPositionCenter];
    }else{
        SubcriberViewController *vc = [[SubcriberViewController alloc]initWithNibName:@"SubcriberViewController" bundle:nil];
        vc.Ads = self.adsObj;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (IBAction)onSave:(id)sender {
    if (_tfTitle.text.length ==0) {
        [self.view makeToast:[@"Please fill title of ads!" localized] duration:2.0 position:CSToastPositionCenter];
        return;
    }
    if (_tvContent.text.length ==0) {
        [self.view makeToast:[@"Please fill description!" localized] duration:2.0 position:CSToastPositionCenter];
        return;
    }
//   
//    if (!isSelectedAvatar) {
//        [self.view makeToast:[@"Please select image" localized] duration:2.0 position:CSToastPositionCenter];
//        return;
//    }
    if ([catParentId isEqualToString:@""]) {
        [self.view makeToast:@"Please select Brand" duration:2.0 position:CSToastPositionCenter];
        return;
    }
    //    if ([catSubId isEqualToString:@""]) {
    //        [self.view makeToast:@"Please select sub category" duration:2.0 position:CSToastPositionCenter];
    //        return;
    //    }
    //    if ([cityId isEqualToString:@""]) {
    //        [self.view makeToast:[@"Please select city" localized] duration:2.0 position:CSToastPositionCenter];
    //        return;
    //    }
    
    //    if ([yearsId isEqualToString:@""]) {
    //        [self.view makeToast:@"Please select year" duration:2.0 position:CSToastPositionCenter];
    //        return;
    //    }
    //    if ([transId isEqualToString:@""]) {
    //        [self.view makeToast:@"Please select transmission" duration:2.0 position:CSToastPositionCenter];
    //        return;
    //    }
    //
    NSLog(@"%@", _priceLabel.text);
    if ([_priceLabel.text isEqualToString:@"Price"]){
        [self.view makeToast:@"Please select price" duration:2.0 position:CSToastPositionCenter];
        return;
    }else{
        if (_priceLabel.text.length == 0) {
            [self.view makeToast:@"Please select price" duration:2.0 position:CSToastPositionCenter];
            return;
        }
    }
    //
    //    if ([colorId isEqualToString:@""]) {
    //        [self.view makeToast:@"Please select color" duration:2.0 position:CSToastPositionCenter];
    //        return;
    //    }
    //
    //    if ([featureId isEqualToString:@""]) {
    //        [self.view makeToast:@"Please select feature" duration:2.0 position:CSToastPositionCenter];
    //        return;
    //    }
    //
    //    if ([trimId isEqualToString:@""]) {
    //        [self.view makeToast:@"Please select trim" duration:2.0 position:CSToastPositionCenter];
    //        return;
    //    }
    //
    //    if ([mileageId isEqualToString:@""]) {
    //        [self.view makeToast:@"Please select mileage" duration:2.0 position:CSToastPositionCenter];
    //        return;
    //    }
    
    
    //    if ([priceType isEqualToString:PRICE_CUSTOM]) {
    //        if (_tfPrice.text.length ==0  || _tfUnit.text.length==0) {
    //            [self.view makeToast:@"Please fill value and unit" duration:2.0 position:CSToastPositionCenter];
    //            return;
    //        }

    
    
    NSData *imgAvatarData = UIImageJPEGRepresentation(self.imgImage.image, 0.5);
    NSString *forRent = [[NSString alloc]init];
    NSString *forSale = [[NSString alloc]init];
    if (_segmentControler.selectedSegmentIndex == FOR_RENT) {
        forRent =@"1";
        forSale =@"0";
    }else{
        forSale =@"1";
        forRent =@"0";
    }
    NSMutableArray *arrUrlGallery= [[NSMutableArray alloc]init];
    oldGallery=@"";
    for (ImageObj *objGallery in arrObjImageGallery) {
        if ([objGallery.imgId isEqualToString:@"0"]) {
            [arrUrlGallery addObject:objGallery.imgName];
        }else{
            if (oldGallery.length <2) {
                oldGallery = objGallery.imgId;
            }else{
                oldGallery = [NSString stringWithFormat:@"%@,%@", oldGallery, objGallery.imgId];
            }
        }
    }
    
    NSString *isAvailable = @"";
    if (_btnisAvailable.isSelected) {
        isAvailable =@"1";
    }else{
        isAvailable =@"0";
    }
    
    if ([_priceLabel.text isEqualToString:@"Price"]) {
        Price = @"";
    }else{
        Price = _priceLabel.text ;
    }
    
    if ([_mileageLabel.text isEqualToString:@"Mileage"] ) {
        Mileage = @"";
    }else{
        Mileage = _mileageLabel.text;
    }
    
    if ( [_countryLabel.text isEqualToString:@"Countries"]) {
        Countries = @"";
    }else{
        Countries = _countryLabel.text;
    }
    
    if ( [_yearsLabel.text isEqualToString:@"From Years"]) {
        From = @"";
    }else{
        From = _yearsLabel.text;
    }
    
    
    if ( [_cityLabel.text isEqualToString:@"Cities"]) {
        Cities = @"";
    }else{
        Cities = _cityLabel.text;
    }
    
    if ( [_transLabel.text isEqualToString:@"Transmission"]) {
        Transmission = @"";
    }else{
        Transmission = _transLabel.text;
    }
    
    
    if ( [_colorLabel.text isEqualToString:@"colors"]) {
        colors = @"";
    }else{
        colors = _colorLabel.text;
    }
    
    if ( [_brandLabel.text isEqualToString:@"Brand"]) {
        brands = @"";
    }else{
        brands = _brandLabel.text;
    }
    
    
    if ( [_ModelLabel.text isEqualToString:@"Model"]) {
        trim = @"";
    }else{
        trim = _ModelLabel.text;
    }

    
    [MBProgressHUD showHUDAddedTo:self.viewKeyboard animated:YES];

    [ModelManager editAdsWithTitle:_tfTitle.text
                       oldGallery:oldGallery
                       description:_tvContent.text
                             price:priceType
                       price_value:[Validator getSafeString:Price]
                        price_unit:@""
                           forRent:forRent
                           forSale:forSale
                         accountId:gUser.usId
                        categoryId:catParentId
                           subCate:catSubId
                              city:[Validator getSafeString:Cities]
                     dataImgAvatar:imgAvatarData
                       dataGallery:arrUrlGallery
                       isAvailable:isAvailable
                           country:[Validator getSafeString:Countries]
                              year:[Validator getSafeString:From]
                           priceId:[Validator getSafeString:Price]
                           mileage:[Validator getSafeString:Mileage]
                             color:[Validator getSafeString:colorId]
                              trim:[Validator getSafeString:trimId]
                           feature:[Validator getSafeString:featureId]
                      transmission:[Validator getSafeString:Transmission]
                             brand:[Validator getSafeString:brands]
                             model:[Validator getSafeString:trim]
                             adsId:(NSString*)_adsObj.adsId
                       withSuccess:^(NSString *successStr) {
                           
                           [MBProgressHUD hideHUDForView:self.viewKeyboard animated:YES];
                           [self.view makeToast:[@"Add Ads success!" localized] duration:2.0 position:CSToastPositionCenter];
                           MyAdsVC *frontController = [[MyAdsVC alloc] initWithNibName:@"MyAdsVC" bundle:nil];
                           RevealViewController *rearViewController = [[RevealViewController alloc] initWithNibName:@"RevealViewController" bundle:nil];
                           SWRevealViewController *mainRevealController = [[SWRevealViewController alloc] initWithRearViewController:rearViewController frontViewController:frontController];
                           
                           mainRevealController.rearViewRevealWidth = self.view.frame.size.width*3/4;
                           mainRevealController.rearViewRevealOverdraw = 0;
                           mainRevealController.bounceBackOnOverdraw = NO;
                           mainRevealController.stableDragOnOverdraw = YES;
                           [mainRevealController setFrontViewPosition:FrontViewPositionLeft];
                           mainRevealController.delegate = self;
                           [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                           [self.navigationController pushViewController:mainRevealController animated:YES];
                           
                           
                       } failure:^(NSString *err) {
                           [MBProgressHUD hideHUDForView:self.viewKeyboard animated:YES];
                           [self.view makeToast:err duration:2.0 position:CSToastPositionCenter];
                       }];
    
    
   
}

- (IBAction)onDelete:(id)sender {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[@"Delete ad" localized] message:[@"Do you want to delete this ad?" localized] preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:[@"Delete" localized] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [MBProgressHUD showHUDAddedTo:self.viewKeyboard animated:YES];
        [ModelManager deleteAdsWitjAdsId:_adsObj.adsId withSuccess:^(NSString *strSuccess) {
            if (self) {
                [MBProgressHUD hideHUDForView:self.viewKeyboard animated:YES];
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self.view makeToast:[@"Delete ads successful" localized]];
                    [self.delegate didDeleteAds:_adsObj];
                    [self toggleMenu:nil];
                });
            }
            
        } failure:^(NSString *err) {
            if (self) {
                [MBProgressHUD hideHUDForView:self.viewKeyboard animated:YES];
                [self.view makeToast:err duration:2.0 position:CSToastPositionCenter];
            }
        }];
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:[@"Cancel" localized] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
    }]];
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self presentViewController:alertController animated:YES completion:nil];
    });
}


- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *Country = [Util stringForKey:KEY_COUNTRY];
    _countryLabel.text = Country;
    self.headerLabel.text = [@"Edit Ads" localized];
    self.tfTitle.placeholder = [@"Title Ads" localized];
    self.available.text = [@"Available" localized];
    [self.btnUpdate setTitle:[@"UPDATE" localized] forState:UIControlStateNormal];
    [self.btnSubCribe setTitle:[@"SUBSCRIBE" localized] forState:UIControlStateNormal];
    [self.btnDelete setTitle:[@"DELETE" localized] forState:UIControlStateNormal];
    //register Nib gallery colllectionview cell
    self.view.backgroundColor = [UIColor whiteColor];
    [_collectionGalleryView registerNib:[UINib nibWithNibName:@"DetailAdsCollectionCellImg" bundle:nil] forCellWithReuseIdentifier:@"DetailAdsCollectionCellImgcell"];
   // [self initLayout];
    // textSearch = @"";
    [self performSelectorInBackground:@selector(initCityAndCategoryData) withObject:nil];
    [self setupView];
    
    
    _imgImage.contentMode = UIViewContentModeScaleAspectFill;
    _imgImage.clipsToBounds = YES;
    self.segmentControler.selectedSegmentIndex = FOR_BUY;
    arrAllCategories = [[NSArray alloc]init];
    arrCities = [[NSMutableArray alloc]init];
    arrCountries = [[NSMutableArray alloc]init];
    arrParentCategories = [[NSMutableArray alloc]init];
    arrSubCategories = [[NSMutableArray alloc]init];
    arrUrlGallery = [[NSMutableArray alloc]init];
    arrDataGallery = [[NSMutableArray alloc] init];
    arrYears = [[NSMutableArray alloc]init];
    arrPrice = [[NSMutableArray alloc]init];
    arrMileage = [[NSMutableArray alloc]init];
    arrTrans = [[NSMutableArray alloc]init];
    arrTrim = [[NSMutableArray alloc]init];
    arrColor = [[NSMutableArray alloc]init];
    arrFeatures = [[NSMutableArray alloc]init];
    // arrBrand = [[NSMutableArray alloc] init];
    
    [self getCities];

    CityObj *cityObj = [[CityObj alloc]init];
    cityObj.cityId =@"";
    cityObj.cityName =[@"All Cities" localized];
    [arrCities addObject:cityObj];
    cityId =@"";
    
    CityObj *countryObj = [[CityObj alloc]init];
    countryObj.cityId =@"";
    countryObj.cityName = [@"All Countries" localized];
    [arrCountries addObject:countryObj];
    [self setCountries];
    
    CityObj *yearObj = [[CityObj alloc]init];
    yearObj.cityId =@"";
    yearObj.cityName = [@"All Years" localized];
    [arrYears addObject:yearObj];
    yearsId = @"";
    [self setYears];
    
    CityObj *priceObj = [[CityObj alloc]init];
    priceObj.cityId =@"";
    priceObj.cityName = [@"All Price" localized];
    [arrPrice addObject:priceObj];
    priceId = @"";
    
    CityObj *milObj = [[CityObj alloc]init];
    milObj.cityId =@"";
    milObj.cityName = [@"All Mileage" localized];
    [arrMileage addObject:milObj];
    mileageId = @"";
    
    CityObj *transObj = [[CityObj alloc]init];
    transObj.cityId =@"";
    transObj.cityName = [@"All Transmission" localized];
    [arrTrans addObject:transObj];
    transId = @"";
    
    CityObj *trimObj = [[CityObj alloc]init];
    trimObj.cityId =@"";
    trimObj.cityName = [@"All Trim" localized];
    [arrTrim addObject:trimObj];
    trimId = @"";
    
    CityObj *colorObj = [[CityObj alloc]init];
    colorObj.cityId =@"";
    colorObj.cityName = [@"All Colors" localized];
    [arrColor addObject:colorObj];
    colorId = @"";
    
    CityObj *featureObj = [[CityObj alloc]init];
    featureObj.cityId =@"";
    featureObj.cityName = [@"All Features" localized];
    [arrFeatures addObject:featureObj];
    featureId = @"";
    
    
    CategoryPr *categoryAllObj = [[CategoryPr alloc]init];
    categoryAllObj.categoryId=@"";
    categoryAllObj.categoryName = [@"All Brand" localized];
    [arrParentCategories addObject:categoryAllObj];
    catParentId =@"";
    
    CategoryPr *categoryAllObj1 = [[CategoryPr alloc]init];
    categoryAllObj1.categoryId=@"";
    categoryAllObj1.categoryName = [@"All Model" localized];
    [arrSubCategories addObject:categoryAllObj1];
    catSubId = @"";
    
    self.imagepicker = [[RBImagePickerController alloc] init];
    self.imagepicker.delegate = self;
    self.imagepicker.dataSource = self;
    self.imagepicker.selectionType = RBMultipleImageSelectionType;
    self.imagepicker.title = @"Custom Title";
    self.imagepicker.navigationController.navigationItem.leftBarButtonItem.title = @"no";
    [self performSelectorInBackground:@selector(initData) withObject:nil];
    [self setupView];
    
    
    self.available.text = [@"Available" localized];
    self.tfTitle.placeholder = [@"Title Ads" localized];
    self.tvContent.placeholder = [@"Description" localized];
    
}
-(void)initData{
    //    [ModelManager getlistCityWithSuccess:^(NSArray *arrCity) {
    //        if (arrCities.count <2) {
    //            arrCities =[arrCities.copy arrayByAddingObjectsFromArray:arrCity].mutableCopy;
    //        }
    //    } failure:^(NSString *error) {
    //
    //    }];
    //
    //    [ModelManager getListCategoryWithSuccess:^(NSArray *arrCategory) {
    //        if (arrAllCategories.count<2) {
    //            arrAllCategories = [arrAllCategories arrayByAddingObjectsFromArray:arrCategory];
    //            for (CategoryPr *cateObj in arrAllCategories) {
    //                if ([[Validator getSafeString:cateObj.categoryParentId] isEqualToString:@"0"]) {
    //                    [arrParentCategories addObject:cateObj];
    //                }
    //            }
    //        }
    //    } failure:^(NSString *err) {
    //    }];
    
    [ModelManager getFilterWithSuccess:^(NSMutableDictionary *dic) {
        
        NSArray *arrCategory = dic[@"categories"];
        if (arrAllCategories.count<2) {
            arrAllCategories = [arrAllCategories arrayByAddingObjectsFromArray:arrCategory];
            for (CategoryPr *cateObj in arrAllCategories) {
                if ([[Validator getSafeString:cateObj.categoryParentId] isEqualToString:@"0"]) {
                    [arrParentCategories addObject:cateObj];
                }
            }
        }
        
        //        NSArray *arrCont = dic[@"countries"];
        //        if (arrCountries.count <2) {
        //            arrCountries =[arrCountries.copy arrayByAddingObjectsFromArray:arrCont].mutableCopy;
        //        }
        
        //        NSArray *arrCity = dic[@"cities"];
        //        if (arrCities.count <2) {
        //            arrCities =[arrCities.copy arrayByAddingObjectsFromArray:arrCity].mutableCopy;
        //        }
        
        //        NSArray *arrYearsTemp = dic[@"years"];
        //        if (arrYears.count <2) {
        //            arrYears =[arrYears.copy arrayByAddingObjectsFromArray:arrYearsTemp].mutableCopy;
        //        }
        
        NSArray *arrPriceTemp = dic[@"price"];
        if (arrPrice.count <2) {
            arrPrice =[arrPrice.copy arrayByAddingObjectsFromArray:arrPriceTemp].mutableCopy;
        }
        
        NSArray *tempMileage = dic[@"mileage"];
        if (arrMileage.count <2) {
            arrMileage =[arrMileage.copy arrayByAddingObjectsFromArray:tempMileage].mutableCopy;
        }
        
        NSArray *tempTrans = dic[@"transmission"];
        if (arrTrans.count <2) {
            arrTrans =[arrTrans.copy arrayByAddingObjectsFromArray:tempTrans].mutableCopy;
        }
        
        NSArray *tempTrim = dic[@"trim"];
        if (arrTrim.count <2) {
            arrTrim =[arrTrim.copy arrayByAddingObjectsFromArray:tempTrim].mutableCopy;
        }
        
        NSArray *tempColor = dic[@"colors"];
        if (arrColor.count <2) {
            arrColor =[arrColor.copy arrayByAddingObjectsFromArray:tempColor].mutableCopy;
        }
        
        NSArray *tempFeature = dic[@"feature"];
        if (arrFeatures.count <2) {
            arrFeatures =[arrFeatures.copy arrayByAddingObjectsFromArray:tempFeature].mutableCopy;
        }
    } failure:^(NSString *err) {
        
    }];
    
}

-(void)initLayout{
    _imgImage.contentMode = UIViewContentModeScaleAspectFill;
    _imgImage.clipsToBounds = YES;
    self.segmentControler.selectedSegmentIndex = 0;
    arrObjImageGallery = [[NSMutableArray alloc]init];
    [arrObjImageGallery addObjectsFromArray:_adsObj.adsGallery.copy];
    
    CityObj *cityObj = [[CityObj alloc]init];
    cityObj.cityId =@"";
    cityObj.cityName = [@"All Cities" localized];
    [arrCities addObject:cityObj];
    if ([_adsObj.adsIsAvailable isEqualToString:@"1"]) {
        _btnisAvailable.selected = YES;
    }else{
        _btnisAvailable.selected = NO;
    }

    selectedType = [@"Buy" localized];
    
    
}

-(void)initCityAndCategoryData{
    
    [ModelManager getlistCityWithSuccess:^(NSArray *arrCity) {
        if (arrCities.count <2) {
            arrCities =[arrCities.copy arrayByAddingObjectsFromArray:arrCity].mutableCopy;
        }
    } failure:^(NSString *error) {
        
    }];
    
    [ModelManager getListCategoryWithSuccess:^(NSArray *arrCategory) {
        if (arrAllCategories.count<2) {
            arrAllCategories = [arrAllCategories arrayByAddingObjectsFromArray:arrCategory];
            for (CategoryPr *cateObj in arrAllCategories) {
                if ([[Validator getSafeString:cateObj.categoryParentId] isEqualToString:@"0"]) {
                    [arrParentCategories addObject:cateObj];
                }
            }
        }
        
    } failure:^(NSString *err) {
    }];
}

-(void)setupView{
    self.vTopNavi.backgroundColor = COLOR_DARK_PR_MARY;
    self.segmentControler.tintColor = COLOR_SEGMENTTINT;
    // self..backgroundColor = COLOR_PRIMARY_DEFAULT;
    [_imgImage setImageWithURL:[NSURL URLWithString:_adsObj.adsImage] placeholderImage:IMAGE_HODER];
    arrObjImageGallery = [[NSMutableArray alloc]init];
    [arrObjImageGallery addObjectsFromArray:_adsObj.adsGallery.copy];
    [_collectionGalleryView reloadData];
    if ([_adsObj.adsForRent isEqualToString:@"1"]) {
        _segmentControler.selectedSegmentIndex = FOR_RENT;
    }else{
        _segmentControler.selectedSegmentIndex = FOR_BUY;
    }
    
    [_tfTitle setText:_adsObj.adsTitle];
    _tfUnit.text = _adsObj.adsPriceUnit;
    _tfPrice.text = _adsObj.adsPriceValue;
    priceType = _adsObj.adsPrice;
    if ([_adsObj.adsPrice isEqualToString:PRICE_FREE]) {
        _btnFreeType.selected = YES;
        _btnCustom.selected = NO;
        _btnNegotile.selected = NO;
        _tfUnit.enabled = NO;
        _tfPrice.enabled = NO;
    }else if([_adsObj.adsPrice isEqualToString:PRICE_NEGO]){
        _btnFreeType.selected = NO;
        _btnCustom.selected = NO;
        _btnNegotile.selected = YES;
        _tfUnit.enabled = NO;
        _tfPrice.enabled = NO;
    }else{
        _btnFreeType.selected = NO;
        _btnCustom.selected = YES;
        _btnNegotile.selected = NO;
        _tfPrice.enabled = YES;
        _tfUnit.enabled = YES;
    }
    
    _tvContent.text = _adsObj.adsDescription;
    self.categoryLabel.text = _adsObj.adsCategory;
    catParentId = _adsObj.adsCategoryId;
    if ([catParentId isEqualToString:@""]) {
        _categoryLabel.text = [@"All Category" localized];
    }
    self.subCategoriesLabel.text = _adsObj.adsSub;
    catSubId = _adsObj.adsSubCatId;
    if ([catSubId isEqualToString:@""]) {
        _subCategoriesLabel.text = [@"All SubCategory" localized];
    }
    self.cityLabel.text = [ Validator getSafeString:_adsObj.adsCity];
    cityId = [ Validator getSafeString:_adsObj.adsCityId];
    if ([cityId isEqualToString:@""]) {
        _cityLabel.text =[@"All city" localized];
    }
    
    self.imagepicker = [[RBImagePickerController alloc] init];
    self.imagepicker.delegate = self;
    self.imagepicker.dataSource = self;
    self.imagepicker.selectionType = RBMultipleImageSelectionType;
    self.imagepicker.title = @"Custom Title";
    self.imagepicker.navigationController.navigationItem.leftBarButtonItem.title = @"no";
    
    _btnUpdate.layer.cornerRadius = 4;
    _btnUpdate.clipsToBounds = YES;
//    _btnUpdate.layer.shadowColor = [UIColor blackColor].CGColor;
//    _btnUpdate.layer.shadowOffset = CGSizeMake(0, 1);
//    _btnUpdate.layer.shadowOpacity = 0.5;
    _btnUpdate.backgroundColor = COLOR_BTN_SMALL;
    
    _btnSubCribe.layer.cornerRadius = 4;
    _btnSubCribe.clipsToBounds = YES;
    _btnSubCribe.backgroundColor = COLOR_EFEFEF;
    [_btnSubCribe setTitleColor:COLOR_0ALPHA80 forState:UIControlStateNormal];
    //_btnSubCribe.layer.shadowColor = [UIColor blackColor].CGColor;
    //_btnSubCribe.layer.shadowOffset = CGSizeMake(0, 1);
    //_btnSubCribe.layer.shadowOpacity = 0.5;
    //_btnSubCribe.backgroundColor = COLOR_BTN_SMALL;
    
    _btnDelete.layer.cornerRadius = 4;
    _btnDelete.clipsToBounds = YES;
    _btnDelete.backgroundColor = COLOR_EFEFEF;
    [_btnDelete setTitleColor:COLOR_0ALPHA80 forState:UIControlStateNormal];
    //    _btnDelete.layer.shadowColor = [UIColor blackColor].CGColor;
    //    _btnDelete.layer.shadowOffset = CGSizeMake(0, 1);
    //    _btnDelete.layer.shadowOpacity = 0.5;
    //    _btnDelete.backgroundColor = COLOR_PRIMARY_DEFAULT;
    
    
    self.vTopNavi.backgroundColor = COLOR_DARK_PR_MARY;
    self.segmentControler.tintColor = COLOR_SEGMENTTINT;
    _btnFreeType.selected = YES;
    _btnCustom.selected = NO;
    _btnNegotile.selected = NO;
    _tfUnit.enabled = NO;
    _tfPrice.enabled = NO;
    priceType = PRICE_FREE;
    
    //self.categoryLabel.text = @"Categories";
    // self.subCategoriesLabel.text = @"Sub Categories";
    self.cityLabel.text = _adsObj.adsCity ;
    self.yearsLabel.text = _adsObj.adsYear;
    self.priceLabel.text = _adsObj.adsPriceValue;
    self.mileageLabel.text = _adsObj.adsMileage;
     self.transLabel.text = _adsObj.adsTransmission;
   // self.trimLabel.text = [@"Trim" localized];
   // self.colorLabel.text = _adsObj.ads;
    self.featureLabel.text = _adsObj.adsIsFeatured;
    self.brandLabel.text = _adsObj.adsCategory;
    self.ModelLabel.text = _adsObj.adsSub;
   // self.transLabel.text = [@"Transmission" localized];
    self.trimLabel.text = [@"Trim" localized];
    self.colorLabel.text = [@"Color" localized];
    self.featureLabel.text = [@"Features" localized];
 
}


- (IBAction)actionCategory:(id)sender {
    if (arrParentCategories.count<2) {
        [self.view makeToast:[@"Please try later" localized]  duration:1.5 position:CSToastPositionCenter];
        [self performSelectorInBackground:@selector(initData) withObject:nil];
    }else{
        NSMutableArray *arrCatName = [[NSMutableArray alloc]init];
        
        for (CategoryPr *cateObj in arrParentCategories) {
            [arrCatName addObject:[Validator getSafeString:cateObj.categoryName]];
        }
        SearchOptionsViewController *controller = [[SearchOptionsViewController alloc]initWithNibName:@"SearchOptionsViewController" bundle:nil];
        controller.delegate = self;
        controller.searchOption = SELECT_PARENT_CATEGORY;
        if (arrParentCategories.count>0) {
            controller.arrayItems = arrCatName;
        }
        [controller presentInParentViewController:self];
    }
    
}
-(void)setYears {
    for(int i=1900;i<=2020; i++) {
        CityObj *yearObj = [[CityObj alloc]init];
        yearObj.cityId =@"";
        yearObj.cityName =[NSString stringWithFormat:@"%i", i];
        [arrYears addObject:yearObj];
    }
}

-(void)setCountries {
    //    NSArray *countries = @[@"Argentina", @"Australia", @"Brazil", @"Canada", @"China", @"France", @"Germany", @"Hungary", @"India", @"Japan", @"Lithuania", @"Malaysia", @"Nepal", @"Russia", @"Thailand", @"United Arab Emirates", @"United Kingdom", @"United States of America", @"Vietnam"];
    //
    //    for(int i=0;i<countries.count; i++) {
    //        CityObj *yearObj = [[CityObj alloc]init];
    //        yearObj.cityId =@"";
    //        yearObj.cityName = [countries objectAtIndex:i];
    //        [arrCountries addObject:yearObj];
    //    }
    
    [ModelManager getCountriesWithSuccess:^(NSArray *countries) {
        if (arrCountries.count <2) {
            arrCountries = [arrCountries.copy arrayByAddingObjectsFromArray:countries].mutableCopy;
        }
    } failure:^(NSString *error) {
        
    }];
    
    
}

- (IBAction)brandAction:(id)sender {
    //    FieldAlertController *alert = [[FieldAlertController alloc] initWithNibName:@"FieldAlertController" bundle:nil];
    //    alert.delegate = self;
    //    alert.titleString = @"Brand";
    //    alert.searchOption = SELECT_BRAND;
    //    [alert presentInParentViewController:self];
    //
    
    //    if (arrParentCategories.count <2) {
    //        [self.view makeToast:[@"Please try again late!" localized] duration:1.5 position:CSToastPositionCenter];
    //        [self performSelectorInBackground:@selector(initData) withObject:nil];
    //
    //    }else{
    //        SearchOptionsViewController *controller = [[SearchOptionsViewController alloc]initWithNibName:@"SearchOptionsViewController" bundle:nil];
    //        controller.delegate = self;
    //        controller.searchOption = SELECT_BRAND;
    //        NSMutableArray *arrName = [[NSMutableArray alloc]init];
    //        for (NSString *name in arrBrand) {
    //            [arrName addObject:name];
    //        }
    //        controller.arrayItems = arrName;
    //        [controller presentInParentViewController:self];
    //    }
    //
    if (arrParentCategories.count<2) {
        [self.view makeToast:[@"Please try later" localized] duration:1.5 position:CSToastPositionCenter];
        [self performSelectorInBackground:@selector(initData) withObject:nil];
    }else{
        NSMutableArray *arrCatName = [[NSMutableArray alloc]init];
        for (CategoryPr *cateObj in arrParentCategories) {
            [arrCatName addObject:[Validator getSafeString:cateObj.categoryName]];
        }
        SearchOptionsViewController *controller = [[SearchOptionsViewController alloc]initWithNibName:@"SearchOptionsViewController" bundle:nil];
        controller.delegate = self;
        controller.searchOption = SELECT_PARENT_CATEGORY;
        if (arrParentCategories.count>0) {
            controller.arrayItems = arrCatName;
        }
        [controller presentInParentViewController:self];
    }
    
}

- (IBAction)transmissionAction:(id)sender {
    if (arrTrans.count <2) {
        [self.view makeToast: [@"Please try again late!" localized] duration:1.5 position:CSToastPositionCenter];
        [self performSelectorInBackground:@selector(initData) withObject:nil];
        
    }else{
        SearchOptionsViewController *controller = [[SearchOptionsViewController alloc]initWithNibName:@"SearchOptionsViewController" bundle:nil];
        controller.delegate = self;
        controller.searchOption = SELECT_TRANS;
        NSMutableArray *arrName = [[NSMutableArray alloc]init];
        for (CityObj *city in arrTrans) {
            [arrName addObject:[Validator getSafeString:city.cityName]];
        }
        controller.arrayItems = arrName;
        [controller presentInParentViewController:self];
    }
    
}

- (IBAction)trimAction:(id)sender {
    if (arrTrim.count <2) {
        [self.view makeToast:[@"Please try again late!" localized] duration:1.5 position:CSToastPositionCenter];
        [self performSelectorInBackground:@selector(initData) withObject:nil];
        
    }else{
        SearchOptionsViewController *controller = [[SearchOptionsViewController alloc]initWithNibName:@"SearchOptionsViewController" bundle:nil];
        controller.delegate = self;
        controller.searchOption = SELECT_TRIM;
        NSMutableArray *arrName = [[NSMutableArray alloc]init];
        for (CityObj *city in arrTrim) {
            [arrName addObject:[Validator getSafeString:city.cityName]];
        }
        controller.arrayItems = arrName;
        [controller presentInParentViewController:self];
    }
    
}

- (IBAction)colorAction:(id)sender {
    if (arrColor.count <2) {
        [self.view makeToast:[@"Please try again late!" localized] duration:1.5 position:CSToastPositionCenter];
        [self performSelectorInBackground:@selector(initData) withObject:nil];
        
    }else{
        SearchOptionsViewController *controller = [[SearchOptionsViewController alloc]initWithNibName:@"SearchOptionsViewController" bundle:nil];
        controller.delegate = self;
        controller.searchOption = SELECT_COLOR;
        NSMutableArray *arrName = [[NSMutableArray alloc]init];
        for (CityObj *city in arrColor) {
            [arrName addObject:[Validator getSafeString:city.cityName]];
        }
        controller.arrayItems = arrName;
        [controller presentInParentViewController:self];
    }
    
}

- (IBAction)featureAction:(id)sender {
    if (arrFeatures.count <2) {
        [self.view makeToast:[@"Please try again late!" localized] duration:1.5 position:CSToastPositionCenter];
        [self performSelectorInBackground:@selector(initData) withObject:nil];
        
    }else{
        SearchOptionsViewController *controller = [[SearchOptionsViewController alloc]initWithNibName:@"SearchOptionsViewController" bundle:nil];
        controller.delegate = self;
        controller.searchOption = SELECT_FEATURE;
        NSMutableArray *arrName = [[NSMutableArray alloc]init];
        for (CityObj *city in arrFeatures) {
            [arrName addObject:[Validator getSafeString:city.cityName]];
        }
        controller.arrayItems = arrName;
        [controller presentInParentViewController:self];
    }
    
}

- (IBAction)mileageAction:(id)sender {
    FieldAlertController *alert = [[FieldAlertController alloc] initWithNibName:@"FieldAlertController" bundle:nil];
    alert.delegate = self;
    alert.titleString = [@"Mileage" localized];
    alert.textTitle = _mileageLabel.text;
    alert.searchOption = SELECT_MILEAGE;
    alert.index = @"1";
    [alert presentInParentViewController:self];
    //    if (arrMileage.count <2) {
    //        [self.view makeToast:@"Please try again late!" duration:1.5 position:CSToastPositionCenter];
    //        [self performSelectorInBackground:@selector(initData) withObject:nil];
    //
    //    }else{
    //        SearchOptionsViewController *controller = [[SearchOptionsViewController alloc]initWithNibName:@"SearchOptionsViewController" bundle:nil];
    //        controller.delegate = self;
    //        controller.searchOption = SELECT_MILEAGE;
    //        NSMutableArray *arrName = [[NSMutableArray alloc]init];
    //        for (CityObj *city in arrMileage) {
    //            [arrName addObject:[Validator getSafeString:city.cityName]];
    //        }
    //        controller.arrayItems = arrName;
    //        [controller presentInParentViewController:self];
    //    }
    
}
- (IBAction)priceAction:(id)sender {
    FieldAlertController *alert = [[FieldAlertController alloc] initWithNibName:@"FieldAlertController" bundle:nil];
    alert.delegate = self;
    alert.titleString = [@"Price" localized];
    alert.textTitle = _priceLabel.text ;
    alert.searchOption = SELECT_PRICE;
    alert.index = @"1";
    [alert presentInParentViewController:self];
    //    if (arrPrice.count <2) {
    //        [self.view makeToast:@"Please try again late!" duration:1.5 position:CSToastPositionCenter];
    //        [self performSelectorInBackground:@selector(initData) withObject:nil];
    //
    //    }else{
    //        SearchOptionsViewController *controller = [[SearchOptionsViewController alloc]initWithNibName:@"SearchOptionsViewController" bundle:nil];
    //        controller.delegate = self;
    //        controller.searchOption = SELECT_PRICE;
    //        NSMutableArray *arrName = [[NSMutableArray alloc]init];
    //        for (CityObj *city in arrPrice) {
    //            [arrName addObject:[Validator getSafeString:city.cityName]];
    //        }
    //        controller.arrayItems = arrName;
    //        [controller presentInParentViewController:self];
    //    }
    
}
- (IBAction)modelAction:(id)sender {
    //    FieldAlertController *alert = [[FieldAlertController alloc] initWithNibName:@"FieldAlertController" bundle:nil];
    //    alert.delegate = self;
    //    alert.titleString = [@"Model" localized];
    //    alert.searchOption = SELECT_MODEL;
    //    [alert presentInParentViewController:self];
    
    if (!catParentId) {
        [self.view makeToast:[@"Please select Model first!" localized]  duration:2.0 position:CSToastPositionCenter];
        return;
    }
    [arrSubCategories removeAllObjects];
    CategoryPr *categoryAllObj = [[CategoryPr alloc]init];
    categoryAllObj.categoryId=@"";
    categoryAllObj.categoryName = [@"All Models" localized];
    [arrSubCategories addObject:categoryAllObj];
    NSMutableArray *arrName = [[NSMutableArray alloc]init];
    [arrName addObject:categoryAllObj.categoryName];
    
    SearchOptionsViewController *controller = [[SearchOptionsViewController alloc]initWithNibName:@"SearchOptionsViewController" bundle:nil];
    controller.delegate = self;
    controller.searchOption = SELECT_SUB_CATEGORY;
    
    if ([catParentId isEqualToString:@""]) {
        for (CategoryPr *cateObj in arrAllCategories) {
            if (![cateObj.categoryParentId isEqualToString:@"0"]) {
                [arrSubCategories addObject:cateObj];
                [arrName addObject:cateObj.categoryName];
            }
        }
    }else{
        for (CategoryPr *cateObj in arrAllCategories) {
            if ([cateObj.categoryParentId isEqualToString:catParentId]) {
                [arrSubCategories addObject:cateObj];
                [arrName addObject:cateObj.categoryName];
            }
        }
        
    }
    controller.arrayItems = arrName;
    [controller presentInParentViewController:self];
}
- (IBAction)yearsAction:(id)sender {
    //    if (arrYears.count <2) {
    //        [self.view makeToast:[@"Please try again late!" localized] duration:1.5 position:CSToastPositionCenter];
    //        [self performSelectorInBackground:@selector(initData) withObject:nil];
    //
    //    }else{
    //        SearchOptionsViewController *controller = [[SearchOptionsViewController alloc]initWithNibName:@"SearchOptionsViewController" bundle:nil];
    //        controller.delegate = self;
    //        controller.searchOption = SELECT_YEARS;
    //        NSMutableArray *arrName = [[NSMutableArray alloc]init];
    //        for (CityObj *city in arrYears) {
    //            [arrName addObject:[Validator getSafeString:city.cityName]];
    //        }
    //        controller.arrayItems = arrName;
    //        [controller presentInParentViewController:self];
    //    }
    
    FieldAlertController *alert = [[FieldAlertController alloc] initWithNibName:@"FieldAlertController" bundle:nil];
    alert.delegate = self;
    alert.titleString =  [@"Years" localized];
    alert.textTitle = _yearsLabel.text;
    alert.searchOption = SELECT_YEARS;
    alert.index = @"1";
    [alert presentInParentViewController:self];
}
- (IBAction)actionCountries:(id)sender {
    if (arrCountries.count <2) {
        [self.view makeToast:[@"Please try again late!" localized] duration:1.5 position:CSToastPositionCenter];
        [self performSelectorInBackground:@selector(setCountries) withObject:nil];
        
    }else{
        SearchOptionsViewController *controller = [[SearchOptionsViewController alloc]initWithNibName:@"SearchOptionsViewController" bundle:nil];
        controller.delegate = self;
        controller.searchOption = SELECT_COUNTRY;
        NSMutableArray *arrName = [[NSMutableArray alloc]init];
        for (CityObj *city in arrCountries) {
            [arrName addObject:[Validator getSafeString:city.cityName]];
        }
        controller.arrayItems = arrName;
        [controller presentInParentViewController:self];
    }
}
- (IBAction)subCategory:(id)sender {
    if (!catParentId) {
        [self.view makeToast:[@"Please select Category first!" localized] duration:2.0 position:CSToastPositionCenter];
        return;
    }
    [arrSubCategories removeAllObjects];
    CategoryPr *categoryAllObj = [[CategoryPr alloc]init];
    categoryAllObj.categoryId=@"";
    categoryAllObj.categoryName = [@"All Sub Categories" localized];
    [arrSubCategories addObject:categoryAllObj];
    NSMutableArray *arrName = [[NSMutableArray alloc]init];
    [arrName addObject:categoryAllObj.categoryName];
    
    SearchOptionsViewController *controller = [[SearchOptionsViewController alloc]initWithNibName:@"SearchOptionsViewController" bundle:nil];
    controller.delegate = self;
    controller.searchOption = SELECT_SUB_CATEGORY;
    
    if ([catParentId isEqualToString:@""]) {
        for (CategoryPr *cateObj in arrAllCategories) {
            if (![cateObj.categoryParentId isEqualToString:@"0"]) {
                [arrSubCategories addObject:cateObj];
                [arrName addObject:cateObj.categoryName];
            }
        }
    }else{
        for (CategoryPr *cateObj in arrAllCategories) {
            if ([cateObj.categoryParentId isEqualToString:catParentId]) {
                [arrSubCategories addObject:cateObj];
                [arrName addObject:cateObj.categoryName];
            }
        }
        
    }
    controller.arrayItems = arrName;
    [controller presentInParentViewController:self];
}

- (IBAction)actionCity:(id)sender {
    if (arrCities.count <2) {
        [self.view makeToast:[@"Please try again late!" localized] duration:1.5 position:CSToastPositionCenter];
        [self performSelectorInBackground:@selector(initData) withObject:nil];
        
    }else{
        SearchOptionsViewController *controller = [[SearchOptionsViewController alloc]initWithNibName:@"SearchOptionsViewController" bundle:nil];
        controller.delegate = self;
        controller.searchOption = SELECT_CITY;
        NSMutableArray *arrName = [[NSMutableArray alloc]init];
        for (CityObj *city in arrCities) {
            [arrName addObject:[Validator getSafeString:city.cityName]];
        }
        controller.arrayItems = arrName;
        [controller presentInParentViewController:self];
    }
}


- (IBAction)toggleMenu:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
#pragma mark ==> delegate dropdown list
-(void)searchOptionsViewControllerDidSelectedItem:(int)rowSelected forOption:(NSString *)option{
    if ([option isEqualToString:SELECT_PARENT_CATEGORY]) {
        CategoryPr *cateSelected = [arrParentCategories objectAtIndex:rowSelected];
        _brandLabel.text = cateSelected.categoryName;
        catParentId = cateSelected.categoryId;
        _ModelLabel.text = [@"Model" localized];
        catSubId = @"";
        return;
    }
    if ([option isEqualToString:SELECT_SUB_CATEGORY]) {
        CategoryPr *cateSelected = [arrSubCategories objectAtIndex:rowSelected];
        _ModelLabel.text = cateSelected.categoryName;
        catSubId = cateSelected.categoryId;
        return;
    }
    if ([option isEqualToString:SELECT_CITY]) {
        CityObj *cityObj = [arrCities objectAtIndex:rowSelected];
        _cityLabel.text = cityObj.cityName;
        cityId = cityObj.cityId;
        
        return;
    }
    
    if ([option isEqualToString:SELECT_COUNTRY]) {
        CityObj *cityObj = [arrCountries objectAtIndex:rowSelected];
        _countryLabel.text = cityObj.cityName;
        countryId = cityObj.cityId;
       // [self getCities];
        return;
    }
    
    if ([option isEqualToString:SELECT_YEARS]) {
        CityObj *cityObj = [arrYears objectAtIndex:rowSelected];
        _yearsLabel.text = cityObj.cityName;
        yearsId = cityObj.cityId;
        return;
    }
    
    if ([option isEqualToString:SELECT_PRICE]) {
        CityObj *cityObj = [arrPrice objectAtIndex:rowSelected];
        _priceLabel.text = cityObj.cityName;
        priceId = cityObj.cityId;
        return;
    }
    
    if ([option isEqualToString:SELECT_MILEAGE]) {
        CityObj *cityObj = [arrMileage objectAtIndex:rowSelected];
        _mileageLabel.text = cityObj.cityName;
        mileageId = cityObj.cityId;
        return;
    }
    
    //    if ([option isEqualToString:SELECT_MODEL]) {
    //
    //        return;
    //    }
    //
    //    if ([option isEqualToString:SELECT_BRAND]) {
    //        NSString *brand = [arrBrand objectAtIndex:rowSelected];
    //        _brandLabel.text = brand;
    //        return;
    //    }
    
    
    if ([option isEqualToString:SELECT_TRANS]) {
        CityObj *cityObj = [arrTrans objectAtIndex:rowSelected];
        _transLabel.text = cityObj.cityName;
        transId = cityObj.cityId;
        return;
    }
    
    if ([option isEqualToString:SELECT_TRIM]) {
        CityObj *cityObj = [arrTrim objectAtIndex:rowSelected];
        _trimLabel.text = cityObj.cityName;
        trimId = cityObj.cityId;
        return;
    }
    
    if ([option isEqualToString:SELECT_COLOR]) {
        CityObj *cityObj = [arrColor objectAtIndex:rowSelected];
        _colorLabel.text = cityObj.cityName;
        colorId = cityObj.cityId;
        return;
    }
    
    if ([option isEqualToString:SELECT_FEATURE]) {
        CityObj *cityObj = [arrFeatures objectAtIndex:rowSelected];
        _featureLabel.text = cityObj.cityName;
        featureId = cityObj.cityId;
        return;
    }
    
    
}

-(void)didEnterValue:(NSString *)value forOption:(NSString *)option {
    if ([option isEqualToString:SELECT_PRICE]) {
        self.priceLabel.text = value;
        if(value.length == 0) {
            self.priceLabel.text = [@"Price" localized] ;
        }
    } else  if ([option isEqualToString:SELECT_MILEAGE]) {
        self.mileageLabel.text = value;
        if(value.length == 0) {
            self.mileageLabel.text = [@"Mileage" localized];
        }
    } else  if ([option isEqualToString:SELECT_YEARS]) {
        self.yearsLabel.text = value;
        if(value.length == 0) {
            self.yearsLabel.text = [@"Year" localized];
        }
    }
    
    //    else  if ([option isEqualToString:SELECT_PARENT_CATEGORY]) {
    //        self.brandLabel.text = value;
    //        if(value.length == 0) {
    //            self.brandLabel.text = [@"Brand" localized];
    //        }
    //    } else  if ([option isEqualToString:sele]) {
    //        self.ModelLabel.text = value;
    //        if(value.length == 0) {
    //            self.ModelLabel.text = [@"Model" localized];
    //        }
    //    }
}


-(void)getCities {
    [ModelManager getCitiesWithSuccess:[Util stringForKey:@"COUNTRYID"] success:^(NSArray *cities) {
        if (arrCities.count <2) {
            arrCities = [arrCities.copy arrayByAddingObjectsFromArray:cities].mutableCopy;
        }
    } failure:^(NSString *error) {
        
    }];
}
-(void)searchDateViewControllerDidSelectedDate:(NSDate *)item{
    selectedDate = item;
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    textSearch = textField.text;
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString *oldText = textField.text;
    NSString *newText = [oldText stringByReplacingCharactersInRange:range withString:string];
    
    textSearch = newText;
    
    return YES;
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
#pragma mark ==> Collectionview gallery delegate and datasoure
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return arrObjImageGallery.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(_collectionGalleryView.frame.size.height*16/9 , _collectionGalleryView.frame.size.height);
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    DetailAdsCollectionCellImg *cell =[collectionView dequeueReusableCellWithReuseIdentifier:@"DetailAdsCollectionCellImgcell" forIndexPath:indexPath];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"DetailAdsCollectionCellImg" owner:self options:nil]objectAtIndex:0];
    }
    ImageObj *objImg = arrObjImageGallery[indexPath.row];
    
    if ([objImg.imgId isEqualToString:@"0"]) {
        
        
        NSURL *a1 =[NSURL URLWithString:objImg.imgName];
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        [library assetForURL:a1 resultBlock:^(ALAsset *asset)
         {
             UIImage  *copyOfOriginalImage = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage] scale:0.5 orientation:UIImageOrientationUp];
             
             cell.imgInCell.image = copyOfOriginalImage;
         }
                failureBlock:^(NSError *error)
         {
             // error handling
         }];
        
    }else{
        [cell.imgInCell setImageWithURL:[NSURL URLWithString:objImg.imgName] placeholderImage:IMAGE_HODER];
    }
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[@"Do you want to delete this picture" localized] message:[@"Select OK to remove this picture" localized] preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:[@"OK" localized] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [arrObjImageGallery removeObjectAtIndex:indexPath.row];
            [_collectionGalleryView reloadData];
        });
        
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:[@"Cancel" localized] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self closeAlertview];
        
    }]];
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self presentViewController:alertController animated:YES completion:nil];
    });
    
    
    
}
-(void)closeAlertview
{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark ==>uiimage picker delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary<NSString *,id> *)editingInfo{
    [_imagePic dismissViewControllerAnimated:YES completion:nil];
    
    self.imgImage.image = image;
     isSelectedAvatar = YES;
}

#pragma mark =>RBpickerview delegate
-(void)imagePickerController:(RBImagePickerController *)imagePicker didFinishPickingImagesWithURL:(NSArray *)imageURLS{
    for (NSURL *url in imageURLS) {
        ImageObj *objImg = [[ImageObj alloc]init];
        objImg.imgId =@"0";
        objImg.imgName = [NSString stringWithFormat:@"%@",url];
        NSString *isContain =@"0";
        for (ImageObj *objOld in arrObjImageGallery) {
            if ([objOld.imgName isEqualToString:objImg.imgName]) {
                isContain =@"1";
                break;
            }
            
        }
        if ([isContain isEqualToString:@"0"]) {
            [arrObjImageGallery addObject:objImg];
        }
        
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [_collectionGalleryView reloadData];
    });
    
    
    
}
-(void)imagePickerControllerDidCancel:(RBImagePickerController *)imagePicker{
    
    [imagePicker dismissViewControllerAnimated:YES completion:nil];
    [_imagePic dismissViewControllerAnimated:YES completion:nil];
}

-(NSInteger)imagePickerControllerMaxSelectionCount:(RBImagePickerController *)imagePicker
{
    
    return 8;
    
}

-(NSInteger)imagePickerControllerMinSelectionCount:(RBImagePickerController *)imagePicker
{
    return 0;
}


@end
