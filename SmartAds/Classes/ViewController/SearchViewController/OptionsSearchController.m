//
//  OptionsSearchController.m
//  SmartAds
//
//  Created by Rakesh Kumar on 12/06/18.
//  Copyright © 2018 Mr Lemon. All rights reserved.
//

#import "OptionsSearchController.h"

@interface OptionsSearchController ()<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UITableView *optionsTable;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

@end

@implementation OptionsSearchController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.arrayTableData = [[NSMutableArray alloc] initWithArray:_arrayItems];
    self.arraySearchData = [[NSMutableArray alloc] initWithArray:_arrayItems];
    
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    self.optionsTable.tableFooterView = view;
    self.optionsTable.delegate = self;
    self.optionsTable.dataSource = self;
    self.searchBar.delegate = self;
    self.optionsTable.bounces = NO;
    self.headerLabel.text = [@"Year" localized];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrayTableData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSString *item = self.arrayTableData[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@", item] ;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //    NSString *item = self.arrayItems[indexPath.row];
    [self closeDetail];
    [self.delegate optionsSearchViewControllerDidSelectedItem:(int)indexPath.row forOption:self.searchOption];
}

-(void)presentInParentViewController:(UIViewController *)parentViewController{
    
    self.view.frame = parentViewController.view.bounds;
    [parentViewController.view addSubview:self.view];
    [parentViewController addChildViewController:self];
    
}

-(void)dismissFromParentViewController{
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    [self didMoveToParentViewController:self.parentViewController];
}

-(void)closeDetail{
    [self dismissFromParentViewController];
}

- (IBAction)backAction:(id)sender {
    [self dismissFromParentViewController];
}


#pragma mark - SearchBar Delegate Methods

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    @try
    {
        [self.arrayTableData removeAllObjects];
        //stringSearch = @"YES";
        NSString *name = @"";
        if ([searchText length] > 0)
        {
            for (int i = 0; i < [self.arraySearchData count] ; i++)
            {
                name = [self.arraySearchData objectAtIndex:i];
                if (name.length >= searchText.length)
                {
                    NSRange titleResultsRange = [name rangeOfString:searchText options:NSCaseInsensitiveSearch];
                    if (titleResultsRange.length > 0)
                    {
                        [self.arrayTableData addObject:[self.arraySearchData objectAtIndex:i]];
                    }
                }
            }
        }
        else
        {
            [self.arrayTableData addObjectsFromArray:self.arraySearchData];
        }
        [_optionsTable reloadData];
    }
    @catch (NSException *exception) {
    }
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)SearchBar
{
    SearchBar.showsCancelButton=YES;
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)theSearchBar
{
    [theSearchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)SearchBar
{
    @try
    {
        SearchBar.showsCancelButton=NO;
        [SearchBar resignFirstResponder];
        [_optionsTable reloadData];
    }
    @catch (NSException *exception) {
    }
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)SearchBar
{
    [SearchBar resignFirstResponder];
}


@end
