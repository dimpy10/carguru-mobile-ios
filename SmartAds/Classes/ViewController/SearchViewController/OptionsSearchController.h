//
//  OptionsSearchController.h
//  SmartAds
//
//  Created by Rakesh Kumar on 12/06/18.
//  Copyright © 2018 Mr Lemon. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol optionsSerchViewControllerDelegate <NSObject>

-(void)optionsSearchViewControllerDidSelectedItem:(int)rowSelected forOption:(NSString*)option;

@end

@interface OptionsSearchController : UIViewController
@property (nonatomic, weak) id<optionsSerchViewControllerDelegate> delegate;
@property (nonatomic, strong) NSArray *arrayItems;
@property (nonatomic, strong) NSMutableArray *arrayTableData;
@property (nonatomic, strong) NSMutableArray *arraySearchData;
@property (nonatomic, strong) NSString *searchOption;


- (void)presentInParentViewController:(UIViewController *)parentViewController;
- (void)dismissFromParentViewController;


@end
