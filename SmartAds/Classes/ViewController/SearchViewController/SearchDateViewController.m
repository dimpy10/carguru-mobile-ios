//
//  SearchDateViewController.m
//  Real Estate
//
//  Created by Hicom on 2/20/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import "SearchDateViewController.h"

@interface SearchDateViewController ()
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *setButton;

@end

@implementation SearchDateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (_dateSelected) {
        self.datePicker.date = self.dateSelected;
    }else{
       _dateSelected= self.datePicker.date = [NSDate date];
    }
    
    [self.cancelButton setTitle:[@"Cancel" localized] forState:UIControlStateNormal];
    [self.setButton setTitle:[@"Set" localized] forState:UIControlStateNormal];
    

}

- (IBAction)dateChanged:(UIDatePicker* )picker {
    self.dateSelected = picker.date;
}

- (IBAction)actionCancel:(id)sender {
   // [self.delegate searchDateViewControllerDidSelectedDate:self.dateSelected];
    [self closeDetail];
}

- (IBAction)actionSet:(id)sender {
    [self.delegate searchDateViewControllerDidSelectedDate:self.dateSelected];
    [self closeDetail];

}



-(void)presentInParentViewController:(UIViewController *)parentViewController{
    
    self.view.frame = parentViewController.view.bounds;
    [parentViewController.view addSubview:self.view];
    [parentViewController addChildViewController:self];
    
}

-(void)dismissFromParentViewController{
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    [self didMoveToParentViewController:self.parentViewController];
}

-(void)closeDetail{
    [self dismissFromParentViewController];
}






@end
