//
//  SearchOptionsViewController.h
//  Real Estate
//
//  Created by Hicom on 2/20/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Macros.h"

@protocol SearchOptionsViewControllerDelegate <NSObject>

-(void)searchOptionsViewControllerDidSelectedItem:(int)rowSelected forOption:(NSString*)option;

@end

@interface SearchOptionsViewController : UIViewController

@property (nonatomic, weak) id<SearchOptionsViewControllerDelegate> delegate;
@property (nonatomic, strong) NSArray *arrayItems;
@property (nonatomic, strong) NSString *searchOption;

- (void)presentInParentViewController:(UIViewController *)parentViewController;
- (void)dismissFromParentViewController;


@end
