//
//  SeachViewController.m
//  Real Estate
//
//  Created by Hicom on 2/20/16.
//  Copyright © 2016 TuanVN. All rights reserved.
//

#import "SeachViewController.h"
#import "FieldAlertController.h"
#import "SearchOptionsViewController.h"
#import "OptionsSearchController.h"
#import "SearchDateViewController.h"
#import "SearchAdsResultVC.h"
#import "CategoryPr.h"
#import "CityObj.h"
#import "UIView+Toast.h"
#define SELECT_PARENT_CATEGORY @"PARENT_CATEGORY"
#define SELECT_SUB_CATEGORY     @"SUB_CATEGORY"
#define SELECT_CITY             @"SELECT_CITY"
#define SELECT_COUNTRY            @"SELECT_COUNTRY"
#define SELECT_COMPANY          @"SELECT_COMPANY"
#define SELECT_DATE_TYPE        @"SELECT_DATE_TYPE"
#define SELECT_FROM_YEARS          @"SELECT_FROM_YEARS"
#define SELECT_TO_YEARS           @"SELECT_TO_YEARS"
#define SELECT_PRICE            @"SELECT_PRICE"
#define SELECT_MILEAGE            @"SELECT_MILEAGE"
#define SELECT_TRANS            @"SELECT_TRANS"
#define SELECT_TRIM            @"SELECT_TRIM"
#define SELECT_COLOR            @"SELECT_COLOR"
#define SELECT_FEATURE            @"SELECT_FEATURE"
#define SELECT_MODEL            @"SELECT_MODEL"

@interface SeachViewController () <UITextFieldDelegate, SearchOptionsViewControllerDelegate, SearchDateViewControllerDelegate, FieldAlertDelegate, optionsSerchViewControllerDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControler;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *subCategoriesLabel;
@property (weak, nonatomic) IBOutlet UILabel *countryLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
@property (weak, nonatomic) IBOutlet UILabel *yearsLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *mileageLabel;
@property (weak, nonatomic) IBOutlet UILabel *transmissionLabel;
@property (weak, nonatomic) IBOutlet UILabel *trimLabel;
@property (weak, nonatomic) IBOutlet UILabel *colorLabel;
@property (weak, nonatomic) IBOutlet UILabel *featureLabel;

@property (weak, nonatomic) IBOutlet UILabel *toYear;


@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UITextField *inputTextField;
@property (weak, nonatomic) IBOutlet UIView *vTopNavi;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UILabel *dateExtensionLabel;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@end

@implementation SeachViewController{
    NSString *selectedType;
    NSString* catParentId, *catSubId, *cityId, *countryId, *companyType, *beforeOrAfter, *yearsId, *priceId, *mileageId, *transId, *trimId, *colorId, *featureId;
    NSDate *selectedDate;
    NSString *textSearch;
    NSArray  *arrAllCategories;
    NSMutableArray *arrCities, *arrCountries, *arrParentCategories, *arrSubCategories, *arrYears, *arrPrice, *arrMileage, *arrTrans, *arrTrim, *arrColor, *arrFeatures;
    NSString *a,*b,*c;
    
    NSString *Price , *Mileage , *Countries , *From , *To , *Cities , *Transmission , *colors;

}

- (void)viewWillAppear:(BOOL)animated {
    NSString *Country = [Util stringForKey:KEY_COUNTRY];
    _countryLabel.text = Country;
   
    if(self.fromHome == YES) {
        [self setBack];
        self.searchView.hidden = YES;
        self.segmentControler.hidden = YES;
        [self.searchButton setTitle:[@"APPLY FILTER" localized] forState:UIControlStateNormal];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.segmentControler setTitle:[@"BUY" localized] forSegmentAtIndex:0];
    [self.segmentControler setTitle:[@"RENT" localized] forSegmentAtIndex:0];
    self.segmentControler.selectedSegmentIndex = 0;
    arrAllCategories = [[NSArray alloc]init];
    arrCities = [[NSMutableArray alloc]init];
    arrCountries = [[NSMutableArray alloc] init];
    arrParentCategories = [[NSMutableArray alloc]init];
    arrSubCategories = [[NSMutableArray alloc]init];
    arrYears = [[NSMutableArray alloc]init];
    arrPrice = [[NSMutableArray alloc]init];
    arrMileage = [[NSMutableArray alloc]init];
    arrTrans = [[NSMutableArray alloc]init];
    arrTrim = [[NSMutableArray alloc]init];
    arrColor = [[NSMutableArray alloc]init];
    arrFeatures = [[NSMutableArray alloc]init];
//    CityObj *cityObj = [[CityObj alloc]init];
//    cityObj.cityId =@"";
//    cityObj.cityName =[@"All Cities" localized];
//    [arrCities addObject:cityObj];
    [self getCities];
    CityObj *countryObj = [[CityObj alloc]init];
    countryObj.cityId =@"";
    countryObj.cityName = [@"All Countries" localized];
    [arrCountries addObject:countryObj];
    [self setCountries];
    
    CityObj *yearObj = [[CityObj alloc]init];
    yearObj.cityId =@"";
    yearObj.cityName = [@"All Years" localized];
    [arrYears addObject:yearObj];
    [self setYears];
    
    CityObj *priceObj = [[CityObj alloc]init];
    priceObj.cityId =@"";
    priceObj.cityName = [@"All Price" localized];
    [arrPrice addObject:priceObj];
    
    CityObj *milObj = [[CityObj alloc]init];
    milObj.cityId =@"";
    milObj.cityName = [@"All Mileage" localized];
    [arrMileage addObject:milObj];
    
    CityObj *transObj = [[CityObj alloc]init];
    transObj.cityId =@"";
    transObj.cityName = [@"All Transmission" localized];
    [arrTrans addObject:transObj];
    
    CityObj *trimObj = [[CityObj alloc]init];
    trimObj.cityId =@"";
    trimObj.cityName = [@"All Trim" localized];
    [arrTrim addObject:trimObj];
    
    CityObj *colorObj = [[CityObj alloc]init];
    colorObj.cityId =@"";
    colorObj.cityName = [@"All Colors" localized];
    [arrColor addObject:colorObj];
    
    CityObj *featureObj = [[CityObj alloc]init];
    featureObj.cityId =@"";
    featureObj.cityName = [@"All Features" localized];
    [arrFeatures addObject:featureObj];
    
    
    
    CategoryPr *categoryAllObj = [[CategoryPr alloc]init];
    categoryAllObj.categoryId=@"";
    categoryAllObj.categoryName = [@"All Brand" localized];
    [arrParentCategories addObject:categoryAllObj];
    //[arrParentCategories addObject:[@"All Brand" localized]];
    
    
    CategoryPr *categoryAllObj1 = [[CategoryPr alloc]init];
    categoryAllObj1.categoryId=@"";
    categoryAllObj1.categoryName = [@"All Model" localized];
    [arrSubCategories addObject:categoryAllObj1];
    
    
    selectedType = @"2";
    _dateExtensionLabel.text = [@"All" localized];
    
    
   // textSearch = @"";
    [self performSelectorInBackground:@selector(initData) withObject:nil];
    [self setupView];
}

-(void)setYears {
    for(int i=1900;i<=2020; i++) {
        CityObj *yearObj = [[CityObj alloc]init];
        yearObj.cityId =@"";
        yearObj.cityName =[NSString stringWithFormat:@"%i", i];
        [arrYears addObject:yearObj];
    }
    
    CityObj *yearObj = [[CityObj alloc]init];
    yearObj.cityId =@"";
    yearObj.cityName = [@"Older than 1900" localized] ;
    [arrYears addObject:yearObj];
    
}

-(void)setCountries {
//    NSArray *countries = @[@"Argentina", @"Australia", @"Brazil", @"Canada", @"China", @"France", @"Germany", @"Hungary", @"India", @"Japan", @"Lithuania", @"Malaysia", @"Nepal", @"Russia", @"Thailand", @"United Arab Emirates", @"United Kingdom", @"United States of America", @"Vietnam"];
//
//    for(int i=0;i<countries.count; i++) {
//        CityObj *yearObj = [[CityObj alloc]init];
//        yearObj.cityId =@"";
//        yearObj.cityName = [countries objectAtIndex:i];
//        [arrCountries addObject:yearObj];
//    }
    
    [ModelManager getCountriesWithSuccess:^(NSArray *countries) {
        if (arrCountries.count <2) {
            arrCountries = [arrCountries.copy arrayByAddingObjectsFromArray:countries].mutableCopy;
        }
    } failure:^(NSString *error) {
        
    }];

}

-(void)getCities {
    NSLog(@"%@",[Util stringForKey:@"COUNTRYID"]);
    [ModelManager getCitiesWithSuccess:[Util stringForKey:@"COUNTRYID"] success:^(NSArray *cities) {
        if (arrCities.count <2) {
            arrCities = [arrCities.copy arrayByAddingObjectsFromArray:cities].mutableCopy;
        }
    } failure:^(NSString *error) {
        
    }];
}

//-(void)setBrands{
//    [arrParentCategories addObject:@"BMW"];
//    [arrParentCategories addObject:@"Mercedes"];
//    [arrParentCategories addObject:@"Audi"];
//    [arrParentCategories addObject:@"Jeep"];
//    [arrParentCategories addObject:@"Ferrari"];
//}

-(void)setBack {
     [self.backButton setImage:nil forState:UIControlStateNormal];
    [self.backButton setImage:[UIImage imageNamed:@"ic_back.png"] forState:UIControlStateNormal];
}

-(void)initData{
    
    [ModelManager getFilterWithSuccess:^(NSMutableDictionary *dic) {
        
        NSArray *arrCategory = dic[@"categories"];
        if (arrAllCategories.count<2) {
            arrAllCategories = [arrAllCategories arrayByAddingObjectsFromArray:arrCategory];
            for (CategoryPr *cateObj in arrAllCategories) {
                if ([[Validator getSafeString:cateObj.categoryParentId] isEqualToString:@"0"]) {
                    [arrParentCategories addObject:cateObj];
                }
            }
        }
        
       
//        NSArray *arrCity = dic[@"cities"];
//        if (arrCities.count <2) {
//            arrCities =[arrCities.copy arrayByAddingObjectsFromArray:arrCity].mutableCopy;
//        }
        
//        NSArray *arrCont = dic[@"countries"];
//        if (arrCountries.count <2) {
//            arrCountries =[arrCountries.copy arrayByAddingObjectsFromArray:arrCont].mutableCopy;
//        }
        
//        NSArray *arrYearsTemp = dic[@"years"];
//        if (arrYears.count <2) {
//            arrYears =[arrYears.copy arrayByAddingObjectsFromArray:arrYearsTemp].mutableCopy;
//        }
        
        NSArray *arrPriceTemp = dic[@"price"];
        if (arrPrice.count <2) {
            arrPrice =[arrPrice.copy arrayByAddingObjectsFromArray:arrPriceTemp].mutableCopy;
        }
        
        NSArray *tempMileage = dic[@"mileage"];
        if (arrMileage.count <2) {
            arrMileage =[arrMileage.copy arrayByAddingObjectsFromArray:tempMileage].mutableCopy;
        }
        
        NSArray *tempTrans = dic[@"transmission"];
        if (arrTrans.count <2) {
            arrTrans =[arrTrans.copy arrayByAddingObjectsFromArray:tempTrans].mutableCopy;
        }
        
        NSArray *tempTrim = dic[@"trim"];
        if (arrTrim.count <2) {
            arrTrim =[arrTrim.copy arrayByAddingObjectsFromArray:tempTrim].mutableCopy;
        }
        
        NSArray *tempColor = dic[@"colors"];
        if (arrColor.count <2) {
            arrColor =[arrColor.copy arrayByAddingObjectsFromArray:tempColor].mutableCopy;
        }
        
        NSArray *tempFeature = dic[@"feature"];
        if (arrFeatures.count <2) {
            arrFeatures =[arrFeatures.copy arrayByAddingObjectsFromArray:tempFeature].mutableCopy;
        }
    } failure:^(NSString *err) {
        
    }];
    
    
    
    
    
    
//    [ModelManager getlistCityWithSuccess:^(NSArray *arrCity) {
//        if (arrCities.count <2) {
//            arrCities =[arrCities.copy arrayByAddingObjectsFromArray:arrCity].mutableCopy;
//        }
//    } failure:^(NSString *error) {
//
//    }];
//
//    [ModelManager getListCategoryWithSuccess:^(NSArray *arrCategory) {
//        if (arrAllCategories.count<2) {
//            arrAllCategories = [arrAllCategories arrayByAddingObjectsFromArray:arrCategory];
//            for (CategoryPr *cateObj in arrAllCategories) {
//                if ([[Validator getSafeString:cateObj.categoryParentId] isEqualToString:@"0"]) {
//                    [arrParentCategories addObject:cateObj];
//                }
//            }
//        }
//        
//    } failure:^(NSString *err) {
//    }];
}

-(void)setupView{
    self.inputTextField.placeholder = [@"Search" localized];
    self.vTopNavi.backgroundColor = COLOR_DARK_PR_MARY;
    self.segmentControler.tintColor = COLOR_SEGMENTTINT;

    self.categoryLabel.text = [@"Brand" localized];
    self.subCategoriesLabel.text = [@"Model" localized];
    self.cityLabel.text = [@"Cities" localized];
    self.countryLabel.text = [@"Countries" localized];
    self.companyLabel.text = [@"Type" localized];
    self.yearsLabel.text = [@"From Years" localized];
     self.toYear.text = [@"To Years" localized];
    self.priceLabel.text = [@"Price" localized];
    self.mileageLabel.text = [@"Mileage" localized];
    self.transmissionLabel.text = [@"Transmission" localized];
    self.trimLabel.text = [@"Trim" localized];
    self.colorLabel.text = [@"Color" localized];
    self.featureLabel.text = [@"Features" localized];
    _dateLabel.text =[@"All" localized];
    self.headerLabel.text = [@"Search" localized];
    [self.searchButton setTitle:[@"SEARCH" localized] forState:UIControlStateNormal];
    if(self.fromHome == YES) {
        [self.searchButton setTitle:[@"APPLY FILTER" localized] forState:UIControlStateNormal];
        self.searchButton.titleLabel.text = [@"APPLY FILTER" localized] ;
        self.headerLabel.text = [@"FILTER" localized];
        self.topConstraint.constant = -100;
    }
    
    NSLog(@"%@",self.cityLabel.text);
    // self.dateExtensionLabel.text = selectedDateEntension;
   // self.dateLabel.text = [self formatDate:selectedDate];
}

- (IBAction)actionCategory:(id)sender {
    if (arrParentCategories.count<2) {
        [self.view makeToast:[@"Please try later" localized] duration:1.5 position:CSToastPositionCenter];
        [self performSelectorInBackground:@selector(initData) withObject:nil];
    }else{
        NSMutableArray *arrCatName = [[NSMutableArray alloc]init];
        
        for (CategoryPr *cateObj in arrParentCategories) {
            [arrCatName addObject:[Validator getSafeString:cateObj.categoryName]];
        }
        //controller.arrayItems = arrName;

//        for (NSString *cateObj in arrParentCategories) {
//            [arrCatName addObject:cateObj];
//        }
        
       
        
        SearchOptionsViewController *controller = [[SearchOptionsViewController alloc]initWithNibName:@"SearchOptionsViewController" bundle:nil];
        controller.delegate = self;
        controller.searchOption = SELECT_PARENT_CATEGORY;
        if (arrParentCategories.count>0) {
            controller.arrayItems = arrCatName;
        }
        [controller presentInParentViewController:self];
    }
    
}

- (IBAction)subCategory:(id)sender {
    if (!catParentId) {
        [self.view makeToast:[@"Please select Model first!" localized]  duration:2.0 position:CSToastPositionCenter];
        return;
    }
    [arrSubCategories removeAllObjects];
    CategoryPr *categoryAllObj = [[CategoryPr alloc]init];
    categoryAllObj.categoryId=@"";
    categoryAllObj.categoryName = @"All Modals";
    [arrSubCategories addObject:categoryAllObj];
    NSMutableArray *arrName = [[NSMutableArray alloc]init];
    [arrName addObject:categoryAllObj.categoryName];

    SearchOptionsViewController *controller = [[SearchOptionsViewController alloc]initWithNibName:@"SearchOptionsViewController" bundle:nil];
    controller.delegate = self;
    controller.searchOption = SELECT_SUB_CATEGORY;

    if ([catParentId isEqualToString:@""]) {
        for (CategoryPr *cateObj in arrAllCategories) {
            if (![cateObj.categoryParentId isEqualToString:@"0"]) {
                [arrSubCategories addObject:cateObj];
                [arrName addObject:cateObj.categoryName];
            }
        }
    }else{
        for (CategoryPr *cateObj in arrAllCategories) {
            if ([cateObj.categoryParentId isEqualToString:catParentId]) {
                [arrSubCategories addObject:cateObj];
                [arrName addObject:cateObj.categoryName];
            }
        }

    }
    controller.arrayItems = arrName;
    [controller presentInParentViewController:self];
    
//    FieldAlertController *alert = [[FieldAlertController alloc] initWithNibName:@"FieldAlertController" bundle:nil];
//    alert.delegate = self;
//    alert.titleString = [@"Model" localized];
//    alert.searchOption = SELECT_MODEL;
//    [alert presentInParentViewController:self];
}

- (IBAction)actionCity:(id)sender {
    if (arrCities.count <2) {
        [self.view makeToast:[@"Please try again later!" localized] duration:1.5 position:CSToastPositionCenter];
        [self performSelectorInBackground:@selector(getCities) withObject:nil];
        
    }else{
        SearchOptionsViewController *controller = [[SearchOptionsViewController alloc]initWithNibName:@"SearchOptionsViewController" bundle:nil];
        controller.delegate = self;
        controller.searchOption = SELECT_CITY;
        NSMutableArray *arrName = [[NSMutableArray alloc]init];
        for (CityObj *city in arrCities) {
            [arrName addObject:[Validator getSafeString:city.cityName]];
        }
        controller.arrayItems = arrName;
        [controller presentInParentViewController:self];
    }
}

- (IBAction)actionCountry:(id)sender {
    if (arrCountries.count <2) {
        [self.view makeToast:[@"Please try again later!" localized] duration:1.5 position:CSToastPositionCenter];
        [self performSelectorInBackground:@selector(setCountries) withObject:nil];
        
    }else{
        SearchOptionsViewController *controller = [[SearchOptionsViewController alloc]initWithNibName:@"SearchOptionsViewController" bundle:nil];
        controller.delegate = self;
        controller.searchOption = SELECT_COUNTRY;
        NSMutableArray *arrName = [[NSMutableArray alloc]init];
        for (CityObj *city in arrCountries) {
            [arrName addObject:[Validator getSafeString:city.cityName]];
        }
        controller.arrayItems = arrName;
        [controller presentInParentViewController:self];
    }
}

- (IBAction)actionCompany:(id)sender {
    SearchOptionsViewController *controller = [[SearchOptionsViewController alloc]initWithNibName:@"SearchOptionsViewController" bundle:nil];
    controller.delegate = self;
    controller.searchOption = SELECT_COMPANY;
    controller.arrayItems = @[[@"All Type" localized], [@"Individual" localized], [@"Company" localized]];
    [controller presentInParentViewController:self];
}

- (IBAction)yearsAction:(id)sender {
        FieldAlertController *alert = [[FieldAlertController alloc] initWithNibName:@"FieldAlertController" bundle:nil];
        alert.delegate = self;
        alert.titleString = [@"From Years" localized];
        alert.textTitle = _yearsLabel.text;
        alert.index = @"0";
        alert.searchOption = SELECT_FROM_YEARS;
        [alert presentInParentViewController:self];
}


- (IBAction)toAction:(id)sender {
   
    FieldAlertController *alert = [[FieldAlertController alloc] initWithNibName:@"FieldAlertController" bundle:nil];
    alert.delegate = self;
    alert.titleString = [@"To Years" localized];
    alert.textTitle = _toYear.text;
    alert.index = @"0";
    alert.searchOption = SELECT_TO_YEARS;
    [alert presentInParentViewController:self];
    
}

- (IBAction)priceAction:(id)sender {
    FieldAlertController *alert = [[FieldAlertController alloc] initWithNibName:@"FieldAlertController" bundle:nil];
    alert.delegate = self;
    alert.titleString = [@"Price" localized];
    alert.textTitle = _priceLabel.text;
    alert.searchOption = SELECT_PRICE;
    alert.index = @"0";
    [alert presentInParentViewController:self];
    
//    if (arrPrice.count <2) {
//        [self.view makeToast:@"Please try again late!" duration:1.5 position:CSToastPositionCenter];
//        [self performSelectorInBackground:@selector(initData) withObject:nil];
//
//    }else{
//        SearchOptionsViewController *controller = [[SearchOptionsViewController alloc]initWithNibName:@"SearchOptionsViewController" bundle:nil];
//        controller.delegate = self;
//        controller.searchOption = SELECT_PRICE;
//        NSMutableArray *arrName = [[NSMutableArray alloc]init];
//        for (CityObj *city in arrPrice) {
//            [arrName addObject:[Validator getSafeString:city.cityName]];
//        }
//        controller.arrayItems = arrName;
//        [controller presentInParentViewController:self];
//    }

}

-(void)didEnterValue:(NSString *)value forOption:(NSString *)option {
    if ([option isEqualToString:SELECT_PRICE]) {
        self.priceLabel.text = value;
        if(value.length == 0) {
            self.priceLabel.text = [@"Price" localized];
        }
    } else  if ([option isEqualToString:SELECT_MILEAGE]) {
        self.mileageLabel.text = value;
        if(value.length == 0) {
            self.mileageLabel.text = [@"Mileage" localized];
        }
    } else  if ([option isEqualToString:SELECT_MODEL]) {
        self.subCategoriesLabel.text = value;
        if(value.length == 0) {
            self.subCategoriesLabel.text = [@"Model" localized];
        }
    } else if ([option isEqualToString:SELECT_FROM_YEARS]) {
        self.yearsLabel.text = value;
        if(value.length == 0) {
            self.yearsLabel.text = [@"From Years" localized];
        }
    }
    else if ([option isEqualToString:SELECT_TO_YEARS]) {
        self.toYear.text = value;
        if(value.length == 0) {
            _toYear.text = [@"To Years" localized];
        }
    }
}

- (IBAction)mileageAction:(id)sender {
    FieldAlertController *alert = [[FieldAlertController alloc] initWithNibName:@"FieldAlertController" bundle:nil];
    alert.delegate = self;
    alert.titleString = [@"Mileage" localized];
    alert.textTitle = _mileageLabel.text;
    alert.searchOption = SELECT_MILEAGE;
    alert.index = @"0";
    [alert presentInParentViewController:self];
//    if (arrMileage.count <2) {
//        [self.view makeToast:@"Please try again late!" duration:1.5 position:CSToastPositionCenter];
//        [self performSelectorInBackground:@selector(initData) withObject:nil];
//
//    }else{
//        SearchOptionsViewController *controller = [[SearchOptionsViewController alloc]initWithNibName:@"SearchOptionsViewController" bundle:nil];
//        controller.delegate = self;
//        controller.searchOption = SELECT_MILEAGE;
//        NSMutableArray *arrName = [[NSMutableArray alloc]init];
//        for (CityObj *city in arrMileage) {
//            [arrName addObject:[Validator getSafeString:city.cityName]];
//        }
//        controller.arrayItems = arrName;
//        [controller presentInParentViewController:self];
//    }

}

- (IBAction)transmissionAction:(id)sender {
    if (arrTrans.count <2) {
        [self.view makeToast:[@"Please try again later!" localized] duration:1.5 position:CSToastPositionCenter];
        [self performSelectorInBackground:@selector(initData) withObject:nil];
        
    }else{
        SearchOptionsViewController *controller = [[SearchOptionsViewController alloc]initWithNibName:@"SearchOptionsViewController" bundle:nil];
        controller.delegate = self;
        controller.searchOption = SELECT_TRANS;
        NSMutableArray *arrName = [[NSMutableArray alloc]init];
        for (CityObj *city in arrTrans) {
            [arrName addObject:[Validator getSafeString:city.cityName]];
        }
        controller.arrayItems = arrName;
        [controller presentInParentViewController:self];
    }

}

- (IBAction)trimAction:(id)sender {
    if (arrTrim.count <2) {
        [self.view makeToast:[@"Please try again later!" localized] duration:1.5 position:CSToastPositionCenter];
        [self performSelectorInBackground:@selector(initData) withObject:nil];
        
    }else{
        SearchOptionsViewController *controller = [[SearchOptionsViewController alloc]initWithNibName:@"SearchOptionsViewController" bundle:nil];
        controller.delegate = self;
        controller.searchOption = SELECT_TRIM;
        NSMutableArray *arrName = [[NSMutableArray alloc]init];
        for (CityObj *city in arrTrim) {
            [arrName addObject:[Validator getSafeString:city.cityName]];
        }
        controller.arrayItems = arrName;
        [controller presentInParentViewController:self];
    }

}

- (IBAction)colorAction:(id)sender {
    if (arrColor.count <2) {
        [self.view makeToast:[@"Please try again later!" localized] duration:1.5 position:CSToastPositionCenter];
        [self performSelectorInBackground:@selector(initData) withObject:nil];
        
    }else{
        SearchOptionsViewController *controller = [[SearchOptionsViewController alloc]initWithNibName:@"SearchOptionsViewController" bundle:nil];
        controller.delegate = self;
        controller.searchOption = SELECT_COLOR;
        NSMutableArray *arrName = [[NSMutableArray alloc]init];
        for (CityObj *city in arrColor) {
            [arrName addObject:[Validator getSafeString:city.cityName]];
        }
        controller.arrayItems = arrName;
        [controller presentInParentViewController:self];
    }

}

- (IBAction)featureAction:(id)sender {
    if (arrFeatures.count <2) {
        [self.view makeToast:[@"Please try again later!" localized] duration:1.5 position:CSToastPositionCenter];
        [self performSelectorInBackground:@selector(initData) withObject:nil];
        
    }else{
        SearchOptionsViewController *controller = [[SearchOptionsViewController alloc]initWithNibName:@"SearchOptionsViewController" bundle:nil];
        controller.delegate = self;
        controller.searchOption = SELECT_FEATURE;
        NSMutableArray *arrName = [[NSMutableArray alloc]init];
        for (CityObj *city in arrFeatures) {
            [arrName addObject:[Validator getSafeString:city.cityName]];
        }
        controller.arrayItems = arrName;
        [controller presentInParentViewController:self];
    }

}

- (IBAction)actionDate:(id)sender {
    SearchOptionsViewController *controller = [[SearchOptionsViewController alloc]initWithNibName:@"SearchOptionsViewController" bundle:nil];
    controller.delegate = self;
    controller.searchOption = SELECT_DATE_TYPE;
    controller.arrayItems = @[[@"All" localized],[@"Before" localized],[@"After" localized]];
    [controller presentInParentViewController:self];
}

- (IBAction)actionShowCalendar:(id)sender {
    SearchDateViewController *controller = [[SearchDateViewController alloc]initWithNibName:@"SearchDateViewController" bundle:nil];
    controller.delegate = self;
    controller.dateSelected = selectedDate;
    [controller presentInParentViewController:self];
}

- (IBAction)actionSearch:(id)sender {
//    if (self.segmentControler.selectedSegmentIndex == 0) {
//        selectedType = @"1";
//    }else{
//        selectedType = @"2";
//    }
    NSTimeInterval timestamp = [selectedDate timeIntervalSince1970];
    NSString *strTime = [NSString stringWithFormat:@"%f",timestamp];
    if (selectedDate == nil) {
        strTime =@"";
    }
//    if ([beforeOrAfter isEqualToString:@""]) {
//        strTime =@"";
//        _dateLabel.text =@"All";
//    }
    if ([_priceLabel.text isEqualToString:@"Price"]) {
        Price = @"";
    }else{
        Price = _priceLabel.text ;
    }
    
    if ([_mileageLabel.text isEqualToString:@"Mileage"] ) {
        Mileage = @"";
    }else{
         Mileage = _mileageLabel.text;
    }
    
    if ( [_countryLabel.text isEqualToString:@"Countries"]) {
        Countries = @"";
    }else{
        Countries = _countryLabel.text;
    }
    
    if ( [_yearsLabel.text isEqualToString:@"From Years"]) {
        From = @"";
    }else{
         From = _yearsLabel.text;
    }
    
    if ( [_toYear.text isEqualToString:@"To Years"]) {
        To = @"";
    }else{
        To = _toYear.text;
    }
   
    if ( [_cityLabel.text isEqualToString:@"Cities"]) {
        Cities = @"";
    }else{
        Cities = _cityLabel.text;
    }
    
    if ( [_transmissionLabel.text isEqualToString:@"Transmission"]) {
        Transmission = @"";
    }else{
        Transmission = _transmissionLabel.text;
    }
    
    
    if ( [_colorLabel.text isEqualToString:@"colors"]) {
        colors = @"";
    }else{
        colors = _colorLabel.text;
    }



    NSDictionary *param = @{
                            @"page":@"1",
                            @"sortBy":@"",
                            @"sortType":@"",
                            @"keyword":_inputTextField.text,
                            @"type":selectedType,
                            @"categoryId":[Validator getSafeString:catParentId],
                            @"subCate":[Validator getSafeString:catSubId],
                            @"city":[Validator getSafeString:_cityLabel.text],
                            @"country":[Validator getSafeString:Countries],
                            @"start_date":[Validator getSafeString:From],
                            @"end_date":[Validator getSafeString:To],
                            @"price":[Validator getSafeString:Price],
                            @"mileage":[Validator getSafeString:Mileage],
                            @"transmission":[Validator getSafeString:Transmission],
                            @"trim":[Validator getSafeString:trimId],
                            @"color":[Validator getSafeString:colors],
                            @"feature":[Validator getSafeString:featureId],
                            @"individual":[Validator getSafeString:companyType],
                            @"date":strTime,
                            @"time_option":[Validator getSafeString:beforeOrAfter]};

    NSLog(@"Param = %@",param);
    if(self.fromHome == YES) {
        // [ModelManager searchAdsWithParam:_paramDic withSuccess:^(NSDictionary *dicReturn) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [ModelManager getAdsInPage:@"1" sortType:@"" sortBy:@"" searchBy:@"" params:param withSuccess:^(NSDictionary *dicReturn) {
            if (self) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                    NSLog(@"Response = %@", dicReturn[@"arrAds"]);
                    NSArray* arr = dicReturn[@"arrAds"];
                    if(arr.count > 0) {
                        [self.delegate filterApplyAction:param.mutableCopy];
                        [self.navigationController popViewControllerAnimated:YES];
                    } else {
                        [self.view makeToast:[@"No result found" localized] duration:3.0 position:CSToastPositionCenter];
                    }
                });
            }
        } failure:^(NSString *error) {
            if (self) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                    [self.view makeToast:error duration:3.0 position:CSToastPositionCenter];
                });
            }
        }];
    } else {
        SearchAdsResultVC *searchResultVC = [[SearchAdsResultVC alloc]init];
        searchResultVC.paramDic = param.mutableCopy;
        [self.navigationController pushViewController:searchResultVC animated:YES];
    }

}

- (IBAction)toggleMenu:(id)sender {
    if(self.fromHome == YES) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self.revealViewController revealToggle:nil];
    }
}


#pragma mark ==> delegate dropdown search list
- (void)optionsSearchViewControllerDidSelectedItem:(int)rowSelected forOption:(NSString *)option {
    if ([option isEqualToString:SELECT_FROM_YEARS]) {
        CityObj *cityObj = [arrYears objectAtIndex:rowSelected];
        _yearsLabel.text = cityObj.cityName;
        yearsId = cityObj.cityId;
        return;
    }
}
#pragma mark ==> delegate dropdown list
-(void)searchOptionsViewControllerDidSelectedItem:(int)rowSelected forOption:(NSString *)option{
    if ([option isEqualToString:SELECT_PARENT_CATEGORY]) {
        CategoryPr *cateSelected = [arrParentCategories objectAtIndex:rowSelected];
        _categoryLabel.text = cateSelected.categoryName;
        catParentId = cateSelected.categoryId;
        _subCategoriesLabel.text = [@"Model" localized] ; 
        catSubId = nil;
       // NSString *cateSelected = [arrParentCategories objectAtIndex:rowSelected];
        //_categoryLabel.text = cateSelected;
        return;
    }
    if ([option isEqualToString:SELECT_SUB_CATEGORY]) {
        CategoryPr *cateSelected = [arrSubCategories objectAtIndex:rowSelected];
        _subCategoriesLabel.text = cateSelected.categoryName;
        catSubId = cateSelected.categoryId;
        return;
    }
    if ([option isEqualToString:SELECT_CITY]) {
        CityObj *cityObj = [arrCities objectAtIndex:rowSelected];
        _cityLabel.text = cityObj.cityName;
        cityId = cityObj.cityId;
        
        return;
    }
    
    if ([option isEqualToString:SELECT_COUNTRY]) {
        CityObj *cityObj = [arrCountries objectAtIndex:rowSelected];
        _countryLabel.text = cityObj.cityName;
        countryId = [Util stringForKey:@"COUNTRYID"];
        NSLog(@"%@",countryId);
       // [self getCities];
        return;
    }
    
    if ([option isEqualToString:SELECT_FROM_YEARS]) {
        CityObj *cityObj = [arrYears objectAtIndex:rowSelected];
        _yearsLabel.text = cityObj.cityName;
        yearsId = cityObj.cityId;
        return;
    }
    
    if ([option isEqualToString:SELECT_TO_YEARS]) {
        CityObj *cityObj = [arrYears objectAtIndex:rowSelected];
        _toYear.text = cityObj.cityName;
        yearsId = cityObj.cityId;
        return;
    }
    
    if ([option isEqualToString:SELECT_PRICE]) {
        CityObj *cityObj = [arrPrice objectAtIndex:rowSelected];
        _priceLabel.text = cityObj.cityName;
        priceId = cityObj.cityId;
        return;
    }
    
    if ([option isEqualToString:SELECT_MILEAGE]) {
        CityObj *cityObj = [arrMileage objectAtIndex:rowSelected];
        _mileageLabel.text = cityObj.cityName;
        
        mileageId = cityObj.cityId;
        return;
    }
    
    if ([option isEqualToString:SELECT_TRANS]) {
        CityObj *cityObj = [arrTrans objectAtIndex:rowSelected];
        _transmissionLabel.text = cityObj.cityName;
        transId = cityObj.cityId;
        return;
    }
    
    if ([option isEqualToString:SELECT_TRIM]) {
        CityObj *cityObj = [arrTrim objectAtIndex:rowSelected];
        _trimLabel.text = cityObj.cityName;
        trimId = cityObj.cityId;
        return;
    }
    
    if ([option isEqualToString:SELECT_COLOR]) {
        CityObj *cityObj = [arrColor objectAtIndex:rowSelected];
        _colorLabel.text = cityObj.cityName;
        colorId = cityObj.cityId;
        return;
    }
    
    if ([option isEqualToString:SELECT_FEATURE]) {
        CityObj *cityObj = [arrFeatures objectAtIndex:rowSelected];
        _featureLabel.text = cityObj.cityName;
        featureId = cityObj.cityId;
        return;
    }
    
    if ([option isEqualToString:SELECT_COMPANY]) {
        if (rowSelected ==0) {
            _companyLabel.text = [@"All Type" localized];
            
        }
        if (rowSelected==1) {
            _companyLabel.text = [@"Individual" localized];
            companyType =@"1";
        }
        if (rowSelected==2) {
            _companyLabel.text = [@"Company" localized];
            companyType = @"2";
        }
        return;
    }
    if ([option isEqualToString:SELECT_DATE_TYPE]) {
        if (rowSelected==0) {
            _dateExtensionLabel.text = [@"All" localized];
            _dateLabel.text = [@"All" localized];
            selectedDate = nil;
            beforeOrAfter = @"";
        }
        if (rowSelected==1) {
            _dateExtensionLabel.text = [@"Before" localized];
            beforeOrAfter = @"1";
        }
        if (rowSelected==2) {
            _dateExtensionLabel.text = [@"After" localized];
            beforeOrAfter = @"2";
        }
        return;
    }
}

-(void)searchDateViewControllerDidSelectedDate:(NSDate *)item{
    selectedDate = item;
    self.dateLabel.text = [self formatDate:selectedDate];
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    textSearch = textField.text;
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString *oldText = textField.text;
    NSString *newText = [oldText stringByReplacingCharactersInRange:range withString:string];
    
    textSearch = newText;
    
    return YES;
}

- (NSString *)formatDate:(NSDate *)theDate
{
    static NSDateFormatter *formatter = nil;
    if (formatter == nil) {
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
        [formatter setTimeStyle:NSDateFormatterNoStyle];
        formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ja_JP"];
        
    }
    
    return [formatter stringFromDate:theDate];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}



@end
