//
//  SearchDateViewController.h
//  Real Estate
//
//  Created by Hicom on 2/20/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SearchDateViewControllerDelegate <NSObject>

-(void)searchDateViewControllerDidSelectedDate:(NSDate*)item;

@end

@interface SearchDateViewController : UIViewController
@property (nonatomic, weak) id<SearchDateViewControllerDelegate> delegate;
@property (nonatomic, strong) NSDate *dateSelected;

- (void)presentInParentViewController:(UIViewController *)parentViewController;
- (void)dismissFromParentViewController;


@end
