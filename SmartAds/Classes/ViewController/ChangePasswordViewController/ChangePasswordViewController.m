//
//  ChangePasswordViewController.m
//  Real Ads
//
//  Created by De Papier on 4/23/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "ModelManager.h"
#import "MBProgressHUD.h"
#import "UIView+Toast.h"
#import "UIKeyboardViewController.h"

@interface ChangePasswordViewController ()<UIKeyboardViewControllerDelegate>
{
    UIKeyboardViewController *keyboard;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgNaviBg;
@property (weak, nonatomic) IBOutlet UILabel *lblCheckPassWhenLoginFB;
    @property (weak, nonatomic) IBOutlet UILabel *headerLabel;
    
@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
        _imgNaviBg.backgroundColor = COLOR_DARK_PR_MARY;
    self.view.backgroundColor = [UIColor whiteColor];
    [self setRevealBtn];
    [self layoutView];
    keyboard = [[UIKeyboardViewController alloc]initWithControllerDelegate:self];
    [keyboard addToolbarToKeyboard];
    self.headerLabel.text = [@"Change Password" localized];
    self.lblCheckPassWhenLoginFB.text = [@"You cannot change password when login by Facebook" localized];
    self.txtCurrentPass.placeholder = [@"Current Password" localized];
    self.txtNewPass.placeholder = [@"New Password" localized];
    self.txtCorfirmPass.placeholder = [@"Confirm Password" localized];
    [self.btnChangePass setTitle:[@"CHANGE PASSWORD" localized] forState:UIControlStateNormal];
}


-(void)setRevealBtn{
    SWRevealViewController *revealController = self.revealViewController;
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    [_revealBtn addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
}
-(void)layoutView
{
    NSString *loginType = [Util stringForKey:KEY_LOGIN];
    if ([loginType isEqualToString:@"1"]) {
        self.ViewChange.hidden = NO;
        _lblCheckPassWhenLoginFB.hidden =YES;
    }else{
        self.ViewChange.hidden = YES;
        _lblCheckPassWhenLoginFB.hidden =NO;
        _lblCheckPassWhenLoginFB.text = [@"You cannot change password when login by Facebook" localized];
    }

    [[self.ViewChange layer]setCornerRadius:6];
}
- (IBAction)OnChangePass:(id)sender {
    if (self.txtCurrentPass.text.length>0) {
        if (self.txtNewPass.text.length>0) {
            if ([self.txtCorfirmPass.text isEqualToString:self.txtNewPass.text]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                [ModelManager changePasswordWithUserId:gUser.usId currentPass:_txtCurrentPass.text newPass:_txtNewPass.text withSuccess:^(NSString *success) {
                    [Util setObject:_txtNewPass.text forKey:@"password"];
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                    [self.view makeToast:success duration:2.0 position:CSToastPositionCenter];
                } failure:^(NSString *error) {
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                    [self.view makeToast:error duration:2.0 position:CSToastPositionCenter];
                    
                }];


    }else{
        [self.view makeToast:[@"password not match. Please try again!" localized] duration:3.0 position:CSToastPositionCenter];
        }
        }else{
            [self.view makeToast:[@"Please insert password!" localized] duration:3.0 position:CSToastPositionCenter];
        }
    }else{
        [self.view makeToast:[@"Please insert password!" localized] duration:3.0 position:CSToastPositionCenter];
    }
}
@end
