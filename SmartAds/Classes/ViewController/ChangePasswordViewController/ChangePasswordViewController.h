//
//  ChangePasswordViewController.h
//  Real Ads
//
//  Created by De Papier on 4/23/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextField.h"

@interface ChangePasswordViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *revealBtn;
@property (strong, nonatomic) IBOutlet UIView *ViewChange;

@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *txtCurrentPass;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *txtNewPass;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *txtCorfirmPass;
@property (strong, nonatomic) IBOutlet UIButton *btnChangePass;
- (IBAction)OnChangePass:(id)sender;
@end
