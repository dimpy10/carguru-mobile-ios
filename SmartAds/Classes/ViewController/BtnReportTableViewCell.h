//
//  BtnReportTableViewCell.h
//  Real Estate
//
//  Created by Hicom on 3/22/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BtnReportTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnReport;
@property (weak, nonatomic) IBOutlet UIButton *addEstimatePrice;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;

@end
