//
//  MessagesDetailViewController.h
//  Real Ads
//
//  Created by Mac on 5/14/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Messages.h"
#import "Ads.h"
#import "UIKeyboardViewController.h"
#import "CBAutoScrollLabel.h"

@interface MessagesDetailViewController : UIViewController<UITextFieldDelegate,UIKeyboardViewControllerDelegate>
{
    UIKeyboardViewController *keyboard;
}

@property (strong, nonatomic) IBOutlet CBAutoScrollLabel *viewTittle;
@property (strong, nonatomic) IBOutlet CBAutoScrollLabel *ViewNamePhone;
@property (strong, nonatomic) IBOutlet CBAutoScrollLabel *ViewPhone;
@property (strong, nonatomic) IBOutlet CBAutoScrollLabel *viewEmail;
@property (strong, nonatomic) IBOutlet CBAutoScrollLabel *ViewDate;
@property (strong, nonatomic) IBOutlet UIImageView *Avartar;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;

- (IBAction)onBack:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *textViewContent;
@property (strong, nonatomic) Messages *message;
@property (strong, nonatomic) IBOutlet UIButton *btnBookmark;
@property (strong, nonatomic) IBOutlet UIButton *btnReply;
@property (strong, nonatomic) IBOutlet UIButton *btnCall;
- (IBAction)OnBookmark:(id)sender;
- (IBAction)OnReply:(id)sender;
- (IBAction)OnCall:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *ViewDetailMess;
@property (strong, nonatomic) Ads *est;
@property (strong, nonatomic) NSMutableArray *ArrBook;
@end
