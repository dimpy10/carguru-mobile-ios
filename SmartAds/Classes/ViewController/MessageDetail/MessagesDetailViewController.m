//
//  MessagesDetailViewController.m
//  Real Ads
//
//  Created by Mac on 5/14/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "MessagesDetailViewController.h"
#import "UIView+Toast.h"
#import "Util.h"
#import "UIView+Toast.h"
#import "UIImageView+WebCache.h"

@interface MessagesDetailViewController ()
{
     int favoriteInt;
}
@property (weak, nonatomic) IBOutlet UILabel *messageDetail;
@property (weak, nonatomic) IBOutlet UIImageView *imgNaviBg;

@end

@implementation MessagesDetailViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
        _imgNaviBg.backgroundColor = COLOR_DARK_PR_MARY;

    self.messageDetail.text = [@"Message Detail" localized];
    [self.btnCall setTitle:[@"CALL" localized] forState:UIControlStateNormal];
    [self.btnReply setTitle:[@"EMAIL" localized] forState:UIControlStateNormal];
    [self setdata];
    favoriteInt = 0;
}

-(void)setdata{
    [[self.ViewDetailMess layer]setCornerRadius:6];
    [[self.btnBookmark layer]setCornerRadius:4];

    self.viewTittle.text = self.message.title;
    
    self.viewTittle.font = [UIFont boldSystemFontOfSize:18];
    self.ViewNamePhone.font = [UIFont boldSystemFontOfSize:18];
    self.viewEmail.font = [UIFont systemFontOfSize:14];
    self.viewEmail.textColor = [UIColor darkGrayColor];
    self.ViewDate.font = [UIFont systemFontOfSize:14];
    self.ViewDate.textColor = [UIColor darkGrayColor];
    self.ViewPhone.font = [UIFont systemFontOfSize:14];
    self.ViewPhone.textColor = [UIColor darkGrayColor];
    self.textViewContent.font = [UIFont systemFontOfSize:14];
    self.textViewContent.textColor = [UIColor darkGrayColor];
    
    self.ViewNamePhone.text = [NSString stringWithFormat:@"%@",self.message.name];
    self.ViewPhone.text = [NSString stringWithFormat:@"%@",self.message.phone];
    self.textViewContent.text = self.message.content;
    self.viewEmail.text = self.message.email;
    self.ViewDate.text = self.message.dateCreated;
    
   // self.Avartar.imageURL = [NSURL URLWithString:self.message.image];
    keyboard = [[UIKeyboardViewController alloc]initWithControllerDelegate:self];
    [keyboard addToolbarToKeyboard];
    self.ArrBook = [[NSMutableArray alloc]init];
    [_Avartar setImageWithURL:[NSURL URLWithString:_message.image] placeholderImage:IMAGE_HODER];
   
    [self.Avartar setContentMode:UIViewContentModeScaleAspectFill];
    self.Avartar.clipsToBounds = YES;
}


- (IBAction)OnBookmark:(id)sender {
//    NSMutableArray *arr = [Util objectForKey:@"Mark"];
//    self.ArrBook = [[NSMutableArray alloc]init];
//    for (NSObject *o in arr) {
//        [self.ArrBook addObject:o];
//    }
////    self.ArrBook = [Util objectForKey:@"Mark"];
//    for (int i=0;i<self.ArrBook.count;i++) {
//        NSString *ab = [self.ArrBook objectAtIndex:i];
//        if ([ab isEqualToString:_message.MessageId]) {
//            [self.ArrBook removeObject:ab];
//            [Util setObject:self.ArrBook forKey:@"Mark"];
//        }else{
//            [self.ArrBook addObject:_message.MessageId];
//            [Util setObject:self.ArrBook forKey:@"Mark"];
//        }
//    }
    
}

- (IBAction)OnReply:(id)sender {
    NSString *customURL = [NSString stringWithFormat:@"Mailto://%@",_message.email];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:customURL]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:customURL]];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:[NSString stringWithFormat:[@"Setting your email account first!" localized]]
                                                       delegate:self cancelButtonTitle:[@"Ok" localized]
                                              otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)OnCall:(id)sender {
    [Util callPhoneNumber:self.message.phone];
}
- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
