//
//  FavoriteViewController.h
//  Real Estate
//
//  Created by De Papier on 4/14/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewCell.h"
#import "LoadMoreTableViewCell.h"
#import "MarqueeLabel.h"
@interface MoreFieldDrawMenu : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *revealBtn;
@property (strong, nonatomic) NSString *strPresent;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) NSString *strTitle;
@property (weak, nonatomic) IBOutlet MarqueeLabel *lblTitle;
@property (strong, nonatomic ) NSString* strBack;
@property (weak, nonatomic) IBOutlet UIImageView *imgNavi;

@end
