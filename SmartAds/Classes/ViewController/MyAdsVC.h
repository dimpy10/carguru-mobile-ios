

#import <UIKit/UIKit.h>
#import "MarqueeLabel.h"

@interface MyAdsVC : UIViewController
@property (weak, nonatomic) IBOutlet MarqueeLabel *lblTitle;
@property (nonatomic, strong) UIRefreshControl *topRefreshTable;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *revealBtn;
@property (strong, nonatomic) NSString *sellerId;
@property (weak, nonatomic) IBOutlet UIView *viewTblView;

@end
