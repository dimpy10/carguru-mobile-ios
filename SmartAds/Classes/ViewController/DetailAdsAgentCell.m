//
//  DetailAdsAgentCell.m
//  Real Estate
//
//  Created by Hicom on 3/5/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import "DetailAdsAgentCell.h"

@implementation DetailAdsAgentCell

- (void)awakeFromNib {
    [super awakeFromNib];
     _thumnails.contentMode = UIViewContentModeScaleAspectFill;
    _thumnails.clipsToBounds = YES;
    _viewCell.layer.cornerRadius = 6;
    _viewCell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _viewCell.layer.borderWidth = 0.5;
    
    self.memberSince.text = [@"Member since" localized];
    [self.btnContact setTitle:[@"CONTACT" localized] forState:UIControlStateNormal];
    [self.btnAds setTitle:[@"ADS" localized] forState:UIControlStateNormal];
}


@end
