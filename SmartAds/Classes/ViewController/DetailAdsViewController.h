//
//  DetailAdsViewController.h
//  Real Estate
//
//  Created by Hicom on 3/5/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ads.h"

@interface DetailAdsViewController : UIViewController
@property (strong, nonatomic) Ads *objAds;
@end
