//
//  DetailAdsThumnailCell.m
//  Real Estate
//
//  Created by Hicom on 3/5/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import "DetailAdsThumnailCell.h"
#import "ImageObj.h"
#import "UIImageView+WebCache.h"


@implementation DetailAdsThumnailCell

- (void)awakeFromNib {
   [_collectionView registerNib:[UINib nibWithNibName:@"DetailAdsCollectionCellImg" bundle:nil] forCellWithReuseIdentifier:@"DetailAdsCollectionCellImgcell"];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    // Initialization code
//    _feeLabel.layer.masksToBounds = YES;
//    _feeLabel.layer.cornerRadius = 6;
    _feeLabel.backgroundColor = COLOR_BTN_SMALL;
//    _thumbnail.layer.borderColor = [UIColor blackColor].CGColor;
//    _thumbnail.layer.borderWidth = 0.5;
     _thumbnail.contentMode = UIViewContentModeScaleAspectFill;
    _thumbnail.clipsToBounds = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
#pragma mark ==> Collectionview datasource and delegate
//
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _arrPhoto.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(_collectionView.frame.size.height*16/9 , _collectionView.frame.size.height);
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    DetailAdsCollectionCellImg *cell =[collectionView dequeueReusableCellWithReuseIdentifier:@"DetailAdsCollectionCellImgcell" forIndexPath:indexPath];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"DetailAdsCollectionCellImg" owner:self options:nil]objectAtIndex:0];
    }
    ImageObj *imgObj = [[ImageObj alloc]init];
    imgObj = [_arrPhoto objectAtIndex:indexPath.row];
    [cell.imgInCell setImageWithURL:[NSURL URLWithString:imgObj.imgName] placeholderImage:IMAGE_HODER];
    
  
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    ImageObj *objImg = [[ImageObj alloc]init];
    objImg = [_arrPhoto objectAtIndex:indexPath.row];
    [_thumbnail setImageWithURL:[NSURL URLWithString:objImg.imgName] placeholderImage:IMAGE_HODER];
    
    
    
}

@end
