//
//  MessagesViewController.m
//  Real Ads
//
//  Created by Mac on 5/13/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "MessagesViewController.h"
#import "ManagePropertionTableViewCell.h"
#import "ModelManager.h"
#import "MBProgressHUD.h"
#import "MessagesDetailViewController.h"
#import "UIImageView+WebCache.h"
#import "UIView+Toast.h"
#import "UITableView+DragLoad.h"
@interface MessagesViewController ()<UITableViewDragLoadDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imgNaviBg;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

@end

@implementation MessagesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.headerLabel.text = [@"Message" localized];
    self.view.backgroundColor = COLOR_BACKGROUD_VIEW;
        _imgNaviBg.backgroundColor = COLOR_DARK_PR_MARY;
    // Do any additional setup after loading the view from its nib.
    [self setRevealBtn];
     self.messageArr = [[NSMutableArray alloc]init];
    [MBProgressHUD showHUDAddedTo:self.viewTblView animated:YES];
     page = 1;
    [self setdata];
    [_tblView setDragDelegate:self refreshDatePermanentKey:@"MessageList"];
    _tblView.showLoadMoreView = YES;
    _tblView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
}

-(void)viewWillAppear:(BOOL)animated{
    if (gArrBookMark.count>0) {
        [self.tblView reloadData];
    }
}
#pragma mark - Control datasource

- (void)finishRefresh
{
    [self.tblView reloadData];
    [_tblView finishRefresh];
    
}
#pragma mark - Drag delegate methods

- (void)dragTableDidTriggerRefresh:(UITableView *)tableView
{
    //send refresh request(generally network request) here
    [MBProgressHUD showHUDAddedTo:self.viewTblView animated:YES];
    page = 1;
   // [self.messageArr removeAllObjects];
        NSThread* myThread = [[NSThread alloc] initWithTarget:self
                                                     selector:@selector(setdata)
                                                       object:nil];
        [myThread start];  // Actually create the thread
    [self performSelector:@selector(finishRefresh) withObject:nil afterDelay:0.5];
}

- (void)dragTableRefreshCanceled:(UITableView *)tableView
{
    //cancel refresh request(generally network request) here
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(finishRefresh) object:nil];
}

- (void)dragTableDidTriggerLoadMore:(UITableView *)tableView{
    page ++;
    // start_index++;
    if (page<=[self.numPage intValue]) {
        NSThread* myThread = [[NSThread alloc] initWithTarget:self
                                                     selector:@selector(setdata)
                                                       object:nil];
        [myThread start];  // Actually create the thread
    }else{
        [self.view makeToast:[@"No Data" localized] duration:2.0 position:CSToastPositionCenter];
        [self.tblView stopLoadMore];
    }

    
}

- (void)dragTableLoadMoreCanceled:(UITableView *)tableView{
    
}
-(void)setdata{
    [MBProgressHUD showHUDAddedTo:self.viewTblView animated:YES];
    [ModelManager getListMessageWithPage:[NSString stringWithFormat:@"%d",page] withSuccess:^(NSDictionary *dicReturn) {
        [MBProgressHUD hideAllHUDsForView:self.viewTblView animated:YES];
        if ([Validator getSafeInt:dicReturn[@"allpage"]] ==1) {
            [_messageArr removeAllObjects];
        }
        [_messageArr addObjectsFromArray:dicReturn[@"arrMess"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [_tblView reloadData];
            [_tblView stopRefresh];
            [_tblView stopLoadMore];
        });
        
    } failure:^(NSString *err) {
        [MBProgressHUD hideAllHUDsForView:self.viewTblView animated:YES];
        [self.view makeToast:err duration:2.0 position:CSToastPositionCenter];
        [_tblView stopRefresh];
        [_tblView stopLoadMore];
    }];

}

-(void)setRevealBtn{
    SWRevealViewController *revealController = self.revealViewController;
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    [_btnBack addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark UITABALEVIEW DATASOURCE

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.messageArr.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 131;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ManagePropertionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ManagePropertionTableViewCell"];
    
    if (!cell) {
        
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ManagePropertionTableViewCell" owner:nil options:nil] objectAtIndex:0];
    }
    Messages *mes = [self.messageArr objectAtIndex:indexPath.row];
    cell.descriptionScrollLabel.text = [NSString stringWithFormat:@"%@",mes.name];
    
    //cell.detailScrollLabel.text = [NSString stringWithFormat:@"%@",mes.phone];
    cell.addressScrollLabel.text = [NSString stringWithFormat:@"%@: %@",mes.title,mes.content];
    cell.expiredScrollLabel.text = mes.dateCreated;
//    gArrBookMark = [Util objectForKey:@"Mark"];
//    if (gArrBookMark.count>indexPath.row) {
//        NSString *idBook = [gArrBookMark objectAtIndex:indexPath.row];
//        if ([mes.MessageId isEqualToString:idBook]) {
//            cell.ImgBookMark.hidden = NO;
//        }
//    }
    
    [cell.image setImageWithURL:[NSURL URLWithString:mes.image] placeholderImage:IMAGE_HODER];
    [cell.image setContentMode:UIViewContentModeScaleAspectFill];
    cell.image.clipsToBounds = YES;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [[cell.ViewCell layer]setCornerRadius:6];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Messages *mes = [self.messageArr objectAtIndex:indexPath.row];
    MessagesDetailViewController *detail = [[MessagesDetailViewController alloc]initWithNibName:@"MessagesDetailViewController" bundle:nil];
    detail.message = mes;
    [self.navigationController pushViewController:detail animated:YES];
}
@end
