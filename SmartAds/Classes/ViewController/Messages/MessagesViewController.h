//
//  MessagesViewController.h
//  Real Ads
//
//  Created by Mac on 5/13/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessagesViewController : UIViewController
{
    int page;
}
@property (strong, nonatomic) IBOutlet UIButton *btnBack;
@property (strong, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) NSString *numPage;
@property (strong, nonatomic) NSMutableArray *messageArr;
@property (strong, nonatomic) NSMutableArray *BookMarkArr;
@property (weak, nonatomic) IBOutlet UIView *viewTblView;
@end
