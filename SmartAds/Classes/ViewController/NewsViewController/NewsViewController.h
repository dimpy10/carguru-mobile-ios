//
//  NewsViewController.h
//  Real Ads
//
//  Created by De Papier on 4/10/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsTableViewCell.h"
@interface NewsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) IBOutlet UIButton *revealBtn;
@property (strong , nonatomic)NSString* numPage;
@property (weak, nonatomic) IBOutlet UITextField *tfSearch;
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;
@property (strong, nonatomic) IBOutlet UIView *viewSearch;
@property (nonatomic, strong) UIRefreshControl *topRefreshTable;

@property (weak, nonatomic) IBOutlet UIView *viewTblView;

@end
