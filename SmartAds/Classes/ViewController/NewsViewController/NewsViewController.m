//
//  NewsViewController.m
//  Real Ads
//
//  Created by De Papier on 4/10/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "NewsViewController.h"
#import "WebViewViewController.h"
#import "News.h"
#import "UIView+Toast.h"
#import "UIScrollView+BottomRefreshControl.h"
#import "UIView+Toast.h"
#import "UITableView+DragLoad.h"
#import "UIImageView+WebCache.h"
#import "SortViewController.h"
@interface NewsViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, SortViewDelegate>
{
    NSMutableArray *arrNews;
    int allPage,currentPage;
    BOOL isLoading;
    NSString *sortType,*sortBy;
    NSArray *arrSortType;
}
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIImageView *imgNaviBg;
@end

@implementation NewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.headerLabel.text = [@"News" localized];
    self.view.backgroundColor = COLOR_BACKGROUD_VIEW;
    _imgNaviBg.backgroundColor = COLOR_DARK_PR_MARY;
    [self setRevealBtn];
    [self setupPullToRefresh];
    [self initSortFunction];
    _indicator.hidden = YES;
    [_indicator startAnimating];
    _viewSearch.clipsToBounds = YES;
    _viewSearch.layer.cornerRadius = 6;
    _viewSearch.layer.borderColor = COLOR_DEVIDER.CGColor;;
    _viewSearch.layer.borderWidth = 0.5;
    //_viewSearch.layer.shadowColor = [[UIColor blackColor] CGColor];
    //_viewSearch.layer.shadowOffset = CGSizeMake(0, 2);
    //_viewSearch.layer.shadowOpacity = 0.5;
    [self performSelectorInBackground:@selector(onSearch:) withObject:nil];
    
    
    

}
-(void)initSortFunction{
    arrSortType = @[[@"Sort by Name asc" localized],
                    [@"Sort by Name desc" localized],
                    [@"Sort by Date asc" localized],
                    [@"Sort by Date desc" localized]];
    sortBy = SORT_BY_ALL;
    sortType = SORT_TYPE_NORMAL;
}

- (IBAction)onSort:(id)sender {
    
    SortViewController *sortVC = [[SortViewController alloc]initWithNibName:@"SortViewController" bundle:nil];
    sortVC.delegate = self;
    sortVC.arrDataSource = arrSortType;
    [sortVC presentInParentViewController:self];
}

-(void)setupPullToRefresh{
    currentPage =1;
    allPage =1;
    arrNews = [[NSMutableArray alloc]init];
    sortBy =sortType=@"0";
    isLoading = false;
    _imgNaviBg.backgroundColor = COLOR_DARK_PR_MARY;
    _topRefreshTable = [UIRefreshControl new];
    _topRefreshTable.attributedTitle = [[NSAttributedString alloc] initWithString:[@"Pull down to reload!" localized] attributes:@{NSForegroundColorAttributeName:COLOR_PRIMARY_DEFAULT}];
    _topRefreshTable.tintColor = COLOR_PRIMARY_DEFAULT;
    [_topRefreshTable addTarget:self action:@selector(onRefreshData) forControlEvents:UIControlEventValueChanged];
    [_tableView addSubview:_topRefreshTable];
    
    UIRefreshControl *refreshControl = [UIRefreshControl new];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:[@"Pull up to load more!" localized] attributes:@{NSForegroundColorAttributeName:COLOR_PRIMARY_DEFAULT}];
    //refreshControl.triggerVerticalOffset = 60;
    [refreshControl addTarget:self action:@selector(onLoadmore) forControlEvents:UIControlEventValueChanged];
    refreshControl.tintColor = COLOR_PRIMARY_DEFAULT;
    self.tableView.bottomRefreshControl = refreshControl;

}

-(void)onRefreshData{
    if (isLoading) {
        return;
    }else
        currentPage = 1;
    isLoading = true;
    [self performSelectorInBackground:@selector(onSearch:) withObject:nil];

}
-(void)onLoadmore{
    if (isLoading) {
        return;
    }else{
        isLoading = true;
    }
    if (currentPage==allPage) {
        [self.view makeToast:[@"No more products" localized]  duration:2.0 position:CSToastPositionCenter];
        [self finishLoading];
        return;
    }
    currentPage+=1;
    [self performSelectorInBackground:@selector(onSearch:) withObject:nil];

}

-(void)setRevealBtn{
    SWRevealViewController *revealController = self.revealViewController;
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    [_revealBtn addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
}
//Table News

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  arrNews.count;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 130;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NewsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NewsTableViewCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"NewsTableViewCell" owner:nil options:nil] objectAtIndex:0];
    }
    News *n = [[News alloc]init];
    n = [arrNews objectAtIndex:indexPath.row];
    cell.titleLabel.text = n.newsTitle;
    [cell.imageNew setImageWithURL:[NSURL URLWithString:n.newsImage] placeholderImage:IMAGE_HODER];
    if (n.newsDateCreated.length >4) {
        cell.lblPosted.text = [@"Posted:" localized];
    }
    cell.imageNew.clipsToBounds = YES;
    if (n.newsDateCreated.length >10) {
        cell.lblPosted.text = [n.newsDateCreated substringWithRange:NSMakeRange(0, 10)];
    }
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    News *n = [[News alloc]init];
    n = [arrNews objectAtIndex:indexPath.row];
    WebViewViewController *vc = [[WebViewViewController alloc]initWithNibName:@"WebViewViewController" bundle:nil];
    vc.newss = n;
    [self.navigationController pushViewController:vc animated:YES];
}


- (IBAction)onSearch:(id)sender {
    [self.view endEditing:YES];
    currentPage = 1;
    [self getDataWithSearch];

}

-(void)getDataWithSearch{
    [MBProgressHUD showHUDAddedTo:self.viewTblView animated:YES];
    [ModelManager getListNewsInpage:[NSString stringWithFormat:@"%d",currentPage] sortType:sortType sortBy:sortBy searchKey:_tfSearch.text withSuccess:^(id dicReturn) {
        if (currentPage == 1) {
            [arrNews removeAllObjects];
        }
        if (self) {
            arrNews = [arrNews arrayByAddingObjectsFromArray:dicReturn[@"arrNews"]].mutableCopy;
            allPage = [dicReturn[@"allpage"]intValue];
            [_tableView reloadData];
            [self finishLoading];
        }
        
    } failure:^(NSString *error) {
        if (self) {
            [self.view makeToast:error duration:2.0 position:CSToastPositionCenter];
            [self finishLoading];
        }
        
    }];
}


-(void)finishLoading{
    [MBProgressHUD hideAllHUDsForView:self.viewTblView animated:YES];
    
       // [_tableView reloadData];
        [_topRefreshTable endRefreshing];
        [self.tableView.bottomRefreshControl endRefreshing];
        isLoading = false;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view endEditing:YES];
    currentPage = 1;
    [self getDataWithSearch];
    return YES;
}

-(void)sortViewDidSelectedItemAtIndex:(int)rowSelected{
    switch (rowSelected) {
        case 0:
            sortBy = SORT_BY_NAME;
            sortType = SORT_TYPE_ASC;
            break;
        case 1:
            sortBy = SORT_BY_NAME;
            sortType = SORT_TYPE_DESC;
            break;
        case 2:
            sortBy = SORT_BY_POSTED_DATE;
            sortType = SORT_TYPE_ASC;
            break;
        case 3:
            sortBy = SORT_BY_POSTED_DATE;
            sortType = SORT_TYPE_DESC;
            break;
            
        default:
            break;
    }
    currentPage = 1;
    [self getDataWithSearch];
    
}



@end
