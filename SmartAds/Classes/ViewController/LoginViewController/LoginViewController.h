//
//  LoginViewController.h
//  Real Ads
//
//  Created by De Papier on 4/21/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIKeyboardViewController.h"
#import "UIView+Toast.h"
#import "HomeVC.h"
#import "User.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "JVFloatLabeledTextView.h"
#import "JVFloatLabeledTextField.h"
@interface LoginViewController : UIViewController<UIKeyboardViewControllerDelegate,SWRevealViewControllerDelegate>
{
    UIKeyboardViewController *keyboard;
}
- (IBAction)onBack:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
- (IBAction)onLogin:(id)sender;
- (IBAction)onFacebook:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *facebookBtn;
@property (weak, nonatomic) IBOutlet UIButton *revealBtn;
@property (weak, nonatomic) IBOutlet UIImageView *checkImg;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) User *user;
@property (weak, nonatomic) IBOutlet UIImageView *ViewLogin;
@property (strong, nonatomic) IBOutlet UIButton *btnRegister;
- (IBAction)OnRegister:(id)sender;
- (IBAction)OnForgetPass:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *ViewForgetpass;
@property (strong, nonatomic) IBOutlet UIView *bgForgetPass;
@property (strong, nonatomic) IBOutlet UIButton *btnOKForgot;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *txtEmailForot;


- (IBAction)OnOkForgotPass:(id)sender;

@end
