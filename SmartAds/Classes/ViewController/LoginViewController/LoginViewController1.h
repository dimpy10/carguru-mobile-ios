//
//  LoginViewController.h
//  Real Estate
//
//  Created by De Papier on 4/21/15.
//  Copyright (c) 2015 Mr Lemon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIKeyboardViewController.h"
#import "UIView+Toast.h"
#import "HomeVC.h"
@interface LoginViewController : UIViewController<UIKeyboardViewControllerDelegate>
{
    UIKeyboardViewController *keyboard;
}
- (IBAction)onBack:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imageView1;
@property (weak, nonatomic) IBOutlet UIImageView *imageView2;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
- (IBAction)onLogin:(id)sender;
- (IBAction)onFacebook:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *facebookBtn;
@property (weak, nonatomic) IBOutlet UIButton *revealBtn;
@property (weak, nonatomic) IBOutlet UIImageView *checkImg;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@end
