//
//  LoginViewController.m
//  Real Estate
//
//  Created by De Papier on 4/21/15.
//  Copyright (c) 2015 Mr Lemon. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()
{
    int remember;
}
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    remember = 1;
    keyboard = [[UIKeyboardViewController alloc]initWithControllerDelegate:self];
    [keyboard addToolbarToKeyboard];
    [self layoutView];
    [self setRevealBtn];
}
-(void)setRevealBtn{
    SWRevealViewController *revealController = self.revealViewController;
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    [_revealBtn addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onLogin:(id)sender {
    if ([_emailTextField.text isEqualToString:@"user"]&&[_passwordTextField.text isEqualToString:@"1234"]) {
        login_already = @"1";

        HomeVC *frontController = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
        RevealViewController *rearViewController = [[RevealViewController alloc] initWithNibName:@"RevealViewController" bundle:nil];
        SWRevealViewController *mainRevealController = [[SWRevealViewController alloc] initWithRearViewController:rearViewController frontViewController:frontController];
        
        mainRevealController.rearViewRevealWidth = self.view.frame.size.width*3/4;
        mainRevealController.rearViewRevealOverdraw = 0;
        mainRevealController.bounceBackOnOverdraw = NO;
        mainRevealController.stableDragOnOverdraw = YES;
        [mainRevealController setFrontViewPosition:FrontViewPositionLeft];
        [frontController.view makeToast:@"Login Successfull" duration:3.0 position:CSToastPositionCenter];
        [self.navigationController pushViewController:mainRevealController animated:YES];
    } else
    {
        [self.view makeToast:@"Wrong email or password!" duration:3.0 position:CSToastPositionCenter];
    }
    login_already = @"1";
}

- (IBAction)onFacebook:(id)sender {
}

- (IBAction)onRemember:(id)sender {
    if (remember == 1) {
        _checkImg.hidden = YES;
        remember = 0;
    } else
    {
        _checkImg.hidden = NO;
        remember = 1;
    }
}



-(void)layoutView
{
    UIColor *color = [UIColor colorWithRed:230/255.00 green:96/255.00 blue:6/255.00 alpha:1.0];
    [Util borderAndcornerImagewith:5.0 andBorder:1.0 andColor:color withImage:_imageView1];
    [Util borderAndcornerImagewith:5.0 andBorder:1.0 andColor:color withImage:_imageView2];
    [Util borderAndcornerButtonwith:5.0 andBorder:0.0 andColor:nil withButton:_loginBtn];
    [Util borderAndcornerButtonwith:5.0 andBorder:0.0 andColor:nil withButton:_facebookBtn];
}

@end
