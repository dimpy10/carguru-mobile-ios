//
//  LoginViewController.m
//  Real Ads
//
//  Created by De Papier on 4/21/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "LoginViewController.h"
#import "MBProgressHUD.h"
#import "RegisterViewController.h"
#import "Validator.h"
#import "UIView+Toast.h"
#import "MoreFieldDrawMenu.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface LoginViewController ()
{
    int remember;
    NSArray *keyArray,*objectArray;
}
@property (weak, nonatomic) IBOutlet UIButton *forgotButton;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imgNaviBg;
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    _imgNaviBg.backgroundColor = COLOR_DARK_PR_MARY;
    self.headerLabel.text = [@"Login" localized];
    self.emailTextField.placeholder = [@"Username" localized];
    self.passwordTextField.placeholder = [@"Password" localized];
    [self.forgotButton setTitle:[@"Forgot Password?" localized] forState:UIControlStateNormal];
    [self.loginBtn setTitle:[@"LOGIN" localized]  forState:UIControlStateNormal];
    [self.btnRegister setTitle:[@"REGISTER" localized] forState:UIControlStateNormal];
    
    // Do any additional setup after loading the view from its nib.
    remember = 1;
    keyboard = [[UIKeyboardViewController alloc]initWithControllerDelegate:self];
    [keyboard addToolbarToKeyboard];
    [self layoutView];
    [self setRevealBtn];
    self.user = [[User alloc]init];
    keyArray = [[NSArray alloc]init];
    objectArray = [[NSArray alloc]init];
    keyArray = [NSArray arrayWithObjects:@"user",@"pass", nil];
    
    _facebookBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    _facebookBtn.layer.shadowOffset = CGSizeMake(0 ,1);
    _facebookBtn.layer.shadowOpacity = 0.3;
    
    //    self.emailTextField.text = [Util objectForKey:@"email"];
    //    self.passwordTextField.text = [Util objectForKey:@"pass"];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(doSingleTap:)];
    tap.numberOfTapsRequired = 1;
    [self.ViewForgetpass addGestureRecognizer:tap];
    self.ViewForgetpass.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
    self.ViewForgetpass.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

}

- (void) doSingleTap :(UIGestureRecognizer *)gesture
{
    [self.ViewForgetpass removeFromSuperview];
}

-(void)setRevealBtn{
    SWRevealViewController *revealController = self.revealViewController;
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    [_revealBtn addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onLogin:(id)sender {

    [self.emailTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    if (!self.emailTextField.text.length || !self.passwordTextField.text.length) {
        [self.view makeToast:[@"Some field is missing. Please fill all required field!" localized] duration:2.0 position:CSToastPositionCenter];
    }else{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [ModelManager loginbyDefaultWithUserName:_emailTextField.text password:_passwordTextField.text withSuccess:^(id userInfo) {
            login_already =@"1";
            gUser = userInfo;
            [self performSelectorInBackground:@selector(getListBookMark) withObject:nil];
            [Util saveObjectWithEncode:gUser forkey:KEY_SAVE_gUSER];
            [Util setObject:@"1" forKey:KEY_LOGIN];
            [Util setObject:_passwordTextField.text forKey:@"password"];
            [Util setObject:[Validator getSafeString:_passwordTextField.text] forKey:@"password"];
            HomeVC *frontController = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
            RevealViewController *rearViewController = [[RevealViewController alloc] initWithNibName:@"RevealViewController" bundle:nil];
            SWRevealViewController *mainRevealController = [[SWRevealViewController alloc] initWithRearViewController:rearViewController frontViewController:frontController];
            mainRevealController.rearViewRevealWidth = self.view.frame.size.width*3/4;
            mainRevealController.rearViewRevealOverdraw = 0;
            mainRevealController.bounceBackOnOverdraw = NO;
            mainRevealController.stableDragOnOverdraw = YES;
            [mainRevealController setFrontViewPosition:FrontViewPositionLeft];
            mainRevealController.delegate = self;
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [self.navigationController pushViewController:mainRevealController animated:YES];
        } failure:^(NSString *err) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [self.view makeToast:err duration:2.0 position:CSToastPositionCenter];
            
        }];
        
    }
}

-(void)fblogin{
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"public_profile", @"email", @"user_friends"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             NSLog(@"Logged in");
             if ([result  grantedPermissions]) {
                 if ([[result grantedPermissions] containsObject:@"email"]) {
                     [self getFBUserData];
                     [login logOut];
                 }
                 
             }
         }
     }];
}

-(void)getFBUserData{
    
    if ([FBSDKAccessToken currentAccessToken]) {
        FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, first_name, last_name, picture.type(large), email"}];
        
        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            if (error == nil){
                NSString *email = result[@"email"];
                if (email) {
                    
//                    NSDictionary *params = @{
//                                             @"email" : result[@"email"],
//                                             @"name" : result[@"name"],
//                                             @"sign_up_method" : @"1",
//                                             @"profile_image" : result[@"picture"][@"data"][@"url"],@"mobile_no" : @0123,
//                                             @"user_type" : @2, @"fcm_id" :[[NSUserDefaults standardUserDefaults]objectForKey:@"FCMtoken"], @"device_type": @2};
                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    [Util setObject:result forKey:@"FacebookDetail"];
                    NSString *avartar = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [result objectForKey:@"id"]];
                    [ModelManager loginFbWithName:result[@"name"] email:result[@"email"] fbId:result[@"id"] andImage:avartar withsuccess:^(id userInfo) {
                        login_already = @"2";
                        gUser = userInfo;
                        //gUser.usImage = avartar;
                        [self performSelectorInBackground:@selector(getListBookMark) withObject:nil];
                        [Util saveObjectWithEncode:gUser forkey:KEY_SAVE_gUSER];
                        [Util setObject:@"2" forKey:KEY_LOGIN];
                        
                        HomeVC *frontController = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
                        RevealViewController *rearViewController = [[RevealViewController alloc] initWithNibName:@"RevealViewController" bundle:nil];
                        
                        SWRevealViewController *mainRevealController = [[SWRevealViewController alloc] initWithRearViewController:rearViewController frontViewController:frontController];
                        mainRevealController.rearViewRevealWidth = self.view.frame.size.width*3/4;
                        mainRevealController.rearViewRevealOverdraw = 0;
                        mainRevealController.bounceBackOnOverdraw = NO;
                        mainRevealController.stableDragOnOverdraw = YES;
                        [mainRevealController setFrontViewPosition:FrontViewPositionLeft];
                        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                        [self.navigationController pushViewController:mainRevealController animated:YES];

                    } failure:^(NSString *err) {
                        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                        [self.view makeToast:err duration:2.0 position:CSToastPositionCenter];
                    }];
                    
//                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//                    [ModelManager loginbyDefaultWithUserName:_emailTextField.text password:_passwordTextField.text withSuccess:^(id userInfo) {
//                        login_already =@"1";
//                        gUser = userInfo;
//                        [self performSelectorInBackground:@selector(getListBookMark) withObject:nil];
//                        [Util saveObjectWithEncode:gUser forkey:KEY_SAVE_gUSER];
//                        [Util setObject:@"1" forKey:KEY_LOGIN];
//                        [Util setObject:_passwordTextField.text forKey:@"password"];
//                        [Util setObject:[Validator getSafeString:_passwordTextField.text] forKey:@"password"];
//                        HomeVC *frontController = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
//                        RevealViewController *rearViewController = [[RevealViewController alloc] initWithNibName:@"RevealViewController" bundle:nil];
//                        SWRevealViewController *mainRevealController = [[SWRevealViewController alloc] initWithRearViewController:rearViewController frontViewController:frontController];
//
//                        mainRevealController.rearViewRevealWidth = self.view.frame.size.width*3/4;
//                        mainRevealController.rearViewRevealOverdraw = 0;
//                        mainRevealController.bounceBackOnOverdraw = NO;
//                        mainRevealController.stableDragOnOverdraw = YES;
//                        [mainRevealController setFrontViewPosition:FrontViewPositionLeft];
//                        mainRevealController.delegate = self;
//                        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//                        [self.navigationController pushViewController:mainRevealController animated:YES];
//                    } failure:^(NSString *err) {
//                        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//                        [self.view makeToast:err duration:2.0 position:CSToastPositionCenter];
//
//                    }];
                }else {
                    
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                    [self.view makeToast:@"No email found in your social." duration:2.0 position:CSToastPositionCenter];
                }
                
            }
        }];
    }
}

- (IBAction)onFacebook:(id)sender {
    [self fblogin];
   // NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
   // for(NSHTTPCookie *cookie in [storage cookies])
    //{
     //   NSString *domainName = [cookie domain];
     //   NSRange domainRange = [domainName rangeOfString:@"facebook"];
      //  if(domainRange.length > 0)
     //   {
       //     [storage deleteCookie:cookie];
      //  }
   // }
    
   // NSArray *permissions = @[@"public_profile",@"email"];
  //  // if the session is closed, then we open it here, and establish a handler for state changes
   // FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
   // login.loginBehavior = FBSDKLoginBehaviorNative;
  //  [login logOut];
   /* [login logInWithReadPermissions:permissions fromViewController:self handler:^(FBSDKLoginManagerLoginResult /*result, NSError *error) {
        if (error) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        } else if (result.isCancelled) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        } else {
            
            if ([FBSDKAccessToken currentAccessToken])
            {
                [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, email, gender, picture"}] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
                 {
                     if (!error)
                     {
                         [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                         [Util setObject:result forKey:@"FacebookDetail"];
                         NSString *avartar = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [result objectForKey:@"id"]];
                         [ModelManager loginFbWithName:result[@"name"] email:result[@"email"] fbId:result[@"id"] andImage:avartar withsuccess:^(id userInfo) {
                             login_already = @"2";
                             gUser = userInfo;
                             //gUser.usImage = avartar;
                             [self performSelectorInBackground:@selector(getListBookMark) withObject:nil];
                             [Util saveObjectWithEncode:gUser forkey:KEY_SAVE_gUSER];
                             [Util setObject:@"2" forKey:KEY_LOGIN];
                             HomeVC *frontController = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
                             RevealViewController *rearViewController = [[RevealViewController alloc] initWithNibName:@"RevealViewController" bundle:nil];
                             SWRevealViewController *mainRevealController = [[SWRevealViewController alloc] initWithRearViewController:rearViewController frontViewController:frontController];
                             mainRevealController.rearViewRevealWidth = self.view.frame.size.width*3/4;
                             mainRevealController.rearViewRevealOverdraw = 0;
                             mainRevealController.bounceBackOnOverdraw = NO;
                             mainRevealController.stableDragOnOverdraw = YES;
                             [mainRevealController setFrontViewPosition:FrontViewPositionLeft];
                             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                             [self.navigationController pushViewController:mainRevealController animated:YES];
                             
                         } failure:^(NSString *err) {
                             [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                             [self.view makeToast:err duration:2.0 position:CSToastPositionCenter];
                         }];
                     }
                     else{
                         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                     }
                 }];
            }
            else
            {
                NSLog(@"Not granted");
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            }
        }
        
    }];*/
}


- (IBAction)onRemember:(id)sender {
    if (remember == 1) {
        _checkImg.hidden = YES;
        remember = 0;
    } else
    {
        _checkImg.hidden = NO;
        remember = 1;
    }
}



-(void)layoutView
{
    [Util borderAndcornerButtonwith:5.0 andBorder:0.0 andColor:nil withButton:_btnRegister];
    [Util borderAndcornerButtonwith:5.0 andBorder:0.0 andColor:nil withButton:_loginBtn];
    [Util borderAndcornerButtonwith:5.0 andBorder:0.0 andColor:nil withButton:_facebookBtn];
    
}

- (IBAction)OnRegister:(id)sender {
    RegisterViewController *regis = [[RegisterViewController alloc]initWithNibName:@"RegisterViewController" bundle:nil];
    [self.navigationController pushViewController:regis animated:YES];
}

- (IBAction)OnForgetPass:(id)sender {
    [self.view endEditing:YES];
    
    [self.view addSubview:self.ViewForgetpass];
}
- (IBAction)OnOkForgotPass:(id)sender {
    if ([Validator validateEmail:_txtEmailForot.text]) {
        [self.ViewForgetpass removeFromSuperview];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [ModelManager forgotPasswordWithEmail:_txtEmailForot.text withSuccess:^(NSString *successStr) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            UIAlertView *alert = [[UIAlertView alloc]
                                                  initWithTitle: @""
                                                  message:[@"Forgot Password Successfull" localized]
                                                  delegate: self
                                                  cancelButtonTitle:[@"OK" localized]
                                                  otherButtonTitles:nil];
                            alert.tag = 10;
                            [alert show];
        } failure:^(NSString *err) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self.view makeToast:err duration:2.0 position:CSToastPositionCenter];
        }];

    }else{
        [self.view makeToast:@"Invalid email" duration:2.0 position:CSToastPositionCenter];
    }
}
- (IBAction)onTearmAndConditions:(id)sender {
    MoreFieldDrawMenu *vc = [[MoreFieldDrawMenu alloc]initWithNibName:@"MoreFieldDrawMenu" bundle:nil];
    vc.strTitle = [@"Terms & Conditions"  localized] ;
    vc.strBack =[@"YES" localized];
    vc.strPresent = strTerm;
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)onPrivacy:(id)sender {
    MoreFieldDrawMenu *vc = [[MoreFieldDrawMenu alloc]initWithNibName:@"MoreFieldDrawMenu" bundle:nil];
    vc.strTitle = [@"Privacy Policy" localized];
    vc.strBack =[@"YES" localized];
    vc.strPresent = strPrivacy;
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)getListBookMark{
    [ModelManager getBookmarksAdsByUserId:gUser.usId andPage:@"0" sortType:@"" sortBy:@"" withSuccess:^(NSDictionary *dicReturn) {
     //   NSArray *arrBookMark = dicReturn[@"arrAds"];
//        [gArrBookMark removeAllObjects];
//        for (Ads *adObj in arrBookMark) {
//            [gArrBookMark addObject:adObj.adsId];
//        }
    } failure:^(NSString *error) {
        
    }];

}

@end
