//
//  MySubscriptionViewController.h
//  Real Ads
//
//  Created by hieund@wirezy on 4/22/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MySubscriptionViewController : UIViewController
{
    int page;
}
@property (weak, nonatomic) IBOutlet UIButton *revealBtn;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) NSMutableArray *MysubArr;
@property (strong, nonatomic) NSString *numPage;
@end
