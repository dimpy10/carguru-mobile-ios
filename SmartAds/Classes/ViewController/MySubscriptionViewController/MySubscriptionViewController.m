//
//  MySubscriptionViewController.m
//  Real Ads
//
//  Created by hieund@wirezy on 4/22/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "MySubscriptionViewController.h"
#import "MySubscriptionTableViewCell.h"
#import "ModelManager.h"
#import "MySubcription.h"

#import "UIView+Toast.h"
#import "UITableView+DragLoad.h"
#import "UIImageView+WebCache.h"
@interface MySubscriptionViewController ()<UITableViewDragLoadDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imgNaviBg;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

@end

@implementation MySubscriptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.headerLabel.text = [@"My Subcriptions" localized];
    self.view.backgroundColor = COLOR_BACKGROUD_VIEW;
        _imgNaviBg.backgroundColor = COLOR_DARK_PR_MARY;
    self.MysubArr = [[NSMutableArray alloc]init];
    // Do any additional setup after loading the view from its nib.
    [self setRevealBtn];
  
    [NSTimer scheduledTimerWithTimeInterval:1.0
                                     target:self
                                   selector:@selector(targetMethod:)
                                   userInfo:nil
                                    repeats:NO];
    page = 1;

    [_tblView setDragDelegate:self refreshDatePermanentKey:@"MySubList"];
    _tblView.showLoadMoreView = YES;
}

#pragma mark - Control datasource

- (void)finishRefresh
{
    [self.tblView reloadData];
    [_tblView finishRefresh];
    
}
#pragma mark - Drag delegate methods

- (void)dragTableDidTriggerRefresh:(UITableView *)tableView
{
    //send refresh request(generally network request) here
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    page = 1;
    //self.MysubArr = [[NSMutableArray alloc]init];
    //[self.MysubArr removeAllObjects];
    
        NSThread* myThread = [[NSThread alloc] initWithTarget:self
                                                     selector:@selector(getdata)
                                                       object:nil];
        [myThread start];  // Actually create the thread
    
    [self performSelector:@selector(finishRefresh) withObject:nil afterDelay:2];
}

- (void)dragTableRefreshCanceled:(UITableView *)tableView
{
    //cancel refresh request(generally network request) here
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(finishRefresh) object:nil];
}
- (void)dragTableDidTriggerLoadMore:(UITableView *)tableView{
    page ++;
    // start_index++;
    if (page<=[self.numPage intValue]) {
        NSThread* myThread = [[NSThread alloc] initWithTarget:self
                                                     selector:@selector(getdata)
                                                       object:nil];
        [myThread start];  // Actually create the thread
    }else{
        [self.view makeToast:[@"No Data" localized] duration:2.0 position:CSToastPositionCenter];
        [self.tblView stopLoadMore];
    }

    
    
}

- (void)dragTableLoadMoreCanceled:(UITableView *)tableView{
    
}
-(void)targetMethod:(NSTimer *)time{
    [self getdata];
    
}
-(void)getdata{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [ModelManager mySubcriptionWithPage:[NSString stringWithFormat:@"%d",page] withSuccess:^(NSDictionary *dicReturn) {
                NSMutableArray *arr =dicReturn[@"data"];
                self.numPage = dicReturn[@"allpage"];
                if (page ==1) {
                    [self.MysubArr removeAllObjects];
                }
                for(int i=0;i < arr.count;i++)
                {
                    MySubcription *est = [[MySubcription alloc]init];
                    NSDictionary *dic = [arr objectAtIndex:i];
                    est.duration = [Validator getSafeString:dic[@"duration"]];
                    est.adsId = [Validator getSafeString:dic[@"estateId"]];
                    est.endDate = [Validator getSafeString:dic[@"endDate"]];
                    est.payment = [Validator getSafeString:dic[@"payment"]];
                    est.paidDate = [Validator getSafeString:dic[@"paidDate"]];
                    est.value = [Validator getSafeString:dic[@"value"]];
                    est.image = [Validator getSafeString:dic[@"image"]];
                    est.titleAds = [Validator getSafeString:dic[@"titleAds"]];
                    [self.MysubArr addObject:est];
                }
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                [self.tblView reloadData];
    } failure:^(NSString *err) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [self.view makeToast:err duration:2.0 position:CSToastPositionCenter];
    }];

}



-(void)setRevealBtn{
    SWRevealViewController *revealController = self.revealViewController;
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    [_revealBtn addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
}
#pragma mark UITABALEVIEW DATASOURCE

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.MysubArr.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 130;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MySubscriptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MySubscriptionTableViewCell"];
    
    if (!cell) {
        
        cell = [[[NSBundle mainBundle] loadNibNamed:@"MySubscriptionTableViewCell" owner:nil options:nil] objectAtIndex:0];
    }
    MySubcription *ess = [self.MysubArr objectAtIndex:indexPath.row];
    //cell.
    [[cell.ViewCell layer]setCornerRadius: 6];
    [cell.imgCell setImageWithURL:[NSURL URLWithString:ess.image] placeholderImage:IMAGE_HODER];

    cell.descriptionScrollLabel.text = ess.titleAds;
    cell.fromScrollLabel.text = [NSString stringWithFormat:@"%@ to %@",ess.paidDate,ess.endDate];
    cell.reachScrollLabel.text =[NSString stringWithFormat:@"%@ Days",ess.duration];
    cell.spentScrollLabel.text = [NSString stringWithFormat:@"$%@",ess.value];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

@end
