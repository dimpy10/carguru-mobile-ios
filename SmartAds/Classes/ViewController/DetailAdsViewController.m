//
//  DetailAdsViewController.m
//  Real Estate
//
//  Created by Hicom on 3/5/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import "DetailAdsViewController.h"
#import "BtnReportTableViewCell.h"
#import "DetailAdsThumnailCell.h"
#import "DetailAdsMainInfoCell.h"
#import "DetailAdsInfoCell.h"
#import "DetailAdsAgentCell.h"
#import "UIImageView+WebCache.h"
#import "User.h"
#import "UIView+Toast.h"
#import "ContactViewController.h"
#import "ImageObj.h"
#import "ContactUsVC.h"
#import "AllAdsViewController.h"
#import "FieldAlertController.h"

@interface DetailAdsViewController ()<UITableViewDataSource, UITableViewDelegate, FieldAlertDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btnFavourite;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *topNaviView;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

@end

@implementation DetailAdsViewController{
    NSMutableArray *listPhotos;
    UIColor *currentColor;
    User *user;
    ImageObj *imgObj;
}
- (void)reloadRowsAtIndexPaths:(NSArray *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation{

}
-(void)getSellerDetail{
    [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
    [ModelManager getSellerDetailWithId:_objAds.adsAccountId withSuccess:^(id successObj) {
        user = (User*)successObj;
        dispatch_async(dispatch_get_main_queue(), ^{
         //   NSIndexPath *index = [NSIndexPath indexPathForRow:3 inSection:1];
            if (self) {
//                DetailAdsAgentCell *cell = [_tableView cellForRowAtIndexPath:index];
//                [cell.thumnails setImageWithURL:user.usImage placeholderImage:IMAGE_HODER];
                [_tableView reloadData];
                [MBProgressHUD hideAllHUDsForView:self.tableView animated:YES];
            }
        });
    } failure:^(NSString *err) {
        [self.view makeToast:err duration:2.0 position:CSToastPositionCenter];
    }];

}

- (IBAction)onFavourite:(id)sender {
    if ([login_already isEqualToString:@"0"]) {
        [self.view makeToast:[@"Please login to use this function" localized] duration:2.0 position:CSToastPositionCenter];
        return;
    }
    if (_btnFavourite.isSelected) {
        for (NSString *idAds in gArrBookMark) {
            if ([idAds isEqualToString:_objAds.adsId]) {
                [gArrBookMark removeObject:idAds];
                break;
            }
        }
    }else{
        [gArrBookMark addObject:_objAds.adsId];
    }
    _btnFavourite.selected = !_btnFavourite.selected;
    
}
- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)initLayout{


}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.headerLabel.text = [@"Detail" localized];
    self.view.backgroundColor = COLOR_BACKGROUD_VIEW;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        [ModelManager increaseViewAdsCount:_objAds.adsId withSuccess:^(NSString *succ) {
        } failure:^(NSString *err) {
        }];
    });
    
    
    [self performSelectorInBackground:@selector(getSellerDetail) withObject:nil];
    listPhotos = [[NSMutableArray alloc]init];
    imgObj = [[ImageObj alloc]init];
    imgObj.imgId = @"0";
    imgObj.imgName = _objAds.adsImage;
    [listPhotos addObject:imgObj];
    for (NSString *idAds in gArrBookMark) {
        if ([idAds isEqualToString:_objAds.adsId]) {
            if (![login_already isEqualToString:@"0"]) {
                _btnFavourite.selected =YES;
            }
            
        }
    }
    listPhotos = [listPhotos arrayByAddingObjectsFromArray:_objAds.adsGallery].mutableCopy;
//    user = [[User alloc]init];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44.0;
    self.topNaviView.backgroundColor = COLOR_DARK_PR_MARY;
    UINib *nib = [UINib nibWithNibName:@"DetailAdsPhotoViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"DetailAdsPhotoViewCell"];
    
    nib = [UINib nibWithNibName:@"DetailAdsThumnailCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"DetailAdsThumnailCell"];
    
    nib = [UINib nibWithNibName:@"BtnReportTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"BtnReportTableViewCell"];
    
    nib = [UINib nibWithNibName:@"DetailAdsMainInfoCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"DetailAdsMainInfoCell"];

    nib = [UINib nibWithNibName:@"DetailAdsInfoCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"DetailAdsInfoCell"];
    
    nib = [UINib nibWithNibName:@"DetailAdsAgentCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"DetailAdsAgentCell"];
    user = [[User alloc]init];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}
-(void)inCreaseViewAdsCount{
    [ModelManager increaseViewAdsCount:_objAds.adsId withSuccess:^(NSString *succ) {
    } failure:^(NSString *err) {
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        DetailAdsThumnailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailAdsThumnailCell"];
        cell.thumbnail.backgroundColor = currentColor;
        [cell.thumbnail setImageWithURL:[NSURL URLWithString:_objAds.adsImage] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
            
        }];
        cell.arrPhoto = listPhotos.copy;
        
        [cell.thumbnail setImageWithURL:[NSURL URLWithString:imgObj.imgName] placeholderImage:IMAGE_HODER];

//        if ([_objAds.adsPrice isEqualToString:@"3"]) {
//            cell.feeLabel.text = [NSString stringWithFormat:@"$%@/%@",[Util convertPriceString:_objAds.adsPriceValue],_objAds.adsPriceUnit];
//        }
//        if ([_objAds.adsPrice isEqualToString:@"1"]) {
//            cell.feeLabel.text =  [@"Free" localized]; //[NSString stringWithFormat:@"Free"];
//        }
//        
//        if ([_objAds.adsPrice isEqualToString:@"2"]) {
//            cell.feeLabel.text = [@"Negotiate" localized];
//        }
    
        if ([_objAds.adsIsAvailable isEqualToString:@"1"]) {
            cell.lblAvailable.hidden = YES;
        } else if ([_objAds.adsIsAvailable isEqualToString:@"0"])
        {
            if([_objAds.adsForRent isEqualToString:@"1"]){
                cell.lblAvailable.text = [@"RENTED" localized];
            }else if([_objAds.adsForSale isEqualToString:@"1"]){
                cell.lblAvailable.text = [@"SOLD" localized];
            }
        }
        [cell.lblAvailable setTransform:CGAffineTransformMakeRotation(-M_PI / 4)];
        [cell awakeFromNib ];
        return cell;
    }else if (indexPath.row == 1) {
        
        DetailAdsMainInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailAdsMainInfoCell"];
        cell.titleLabel.text = _objAds.adsTitle;
        if (_objAds.adsCategory) {
            cell.lblCategory.text = _objAds.adsCategory;
        }else{
            cell.lblCategory.text = [@"All Categories" localized] ;
        }
        
        if (_objAds.adsSub) {
            cell.lblSubCat.text = _objAds.adsSub;
        }else{
            cell.lblSubCat.text = [@"All Sub Categories" localized] ;
        }
        
        if (_objAds.adsCity) {
            if ([_objAds.adsCountry isEqualToString:@""]) {
                cell.lblCity.text = _objAds.adsCity;
            } else {
                if([_objAds.adsCity isEqualToString:@""]) {
                    cell.lblCity.text = _objAds.adsCountry;
                } else {
                    cell.lblCity.text = [NSString stringWithFormat:@"%@, %@", _objAds.adsCity, _objAds.adsCountry];
                }
            }
            
        }else{
            cell.lblCity.text = [@"All cities" localized] ;
        }
        
        cell.lblPosted.text = [_objAds.adsDateCreated substringWithRange:NSMakeRange(0, 10)];

//        if ([_objAds.adsCategoryId isEqualToString:@"0"]) {
//            cell.categoryLabel.text =[@"All Categories" localized] ;
//        }
//        if ([_objAds.adsSubCatId isEqualToString:@"0"]) {
//            cell.lblSubcat.text =[@"All Sub Categories" localized];
//        }
//        if ([_objAds.adsForSale isEqualToString:@"1"]) {
//            cell.typeLabel.text = [@"For sale" localized];
//        }else{
//            cell.typeLabel.text = [@"For rent" localized];
//        }
        
        if([_objAds.adsMileage isEqualToString:@""]) {
            cell.mileageLabel.text = @"-";
        } else {
            cell.mileageLabel.text = _objAds.adsMileage;
        }
        
//        if (_objAds.adsDateCreated.length>10) {
//            cell.lblPosted.text = _objAds.adsDateCreated;
//        }
        
        // if (![est.estimate_price isEqualToString:@""] && ![est.adsPrice isEqualToString:@"1"] ) {
        cell.estimatePriceLabel.text = [NSString stringWithFormat:@"%@ %.2f",_objAds.adsCurrencyCode,_objAds.estimate_price.doubleValue];   // NSString(@"")est.estimate_price;
        // } else {
        //      cell.estimatePriceLabel.text = @" - ";
        //  }
        
        //   if (![est.adsPriceValue isEqualToString:@""] && ![est.adsPrice isEqualToString:@"1"]) {
        cell.actualPriceLabel.text = [NSString stringWithFormat:@"%@ %@",_objAds.adsCurrencyCode,_objAds.adsPriceValue];
        //   } else {
        //       cell.actualPriceLabel.text = @" - ";
        //   }
        
        return cell;
    }else if (indexPath.row == 2) {
        
        DetailAdsInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailAdsInfoCell"];
        cell.detailLabel1.text = _objAds.adsDescription;
        
        
        return cell;
    }else if (indexPath.row == 3) {
        
        DetailAdsAgentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailAdsAgentCell"];
        if ([_objAds.adsAccountType isEqualToString:@"0"]) {
            cell.agentIndividualLabel.text = [@"Invidual" localized];
        }else{
            cell.agentIndividualLabel.text = [@"Company" localized];
        }
        cell.agentNameLabel.text = user.usName;
        if (user) {
            [cell.thumnails setImageWithURL:[NSURL URLWithString:user.usImage] placeholderImage:IMAGE_HODER];
        }else{
        cell.thumnails.backgroundColor = [UIColor lightGrayColor];
        }
        
        if (_objAds.adsAccountDateCreated.length >3) {
            cell.agentTimeStaredLabel.text = [_objAds.adsAccountDateCreated substringWithRange:NSMakeRange(0, 4)];
        }
        if (user.usDateCreated.length >= 10) {
            cell.agentTimeStaredLabel.text = [user.usDateCreated substringWithRange:NSMakeRange(0, 10)];
        }
        
        
        [cell.btnContact addTarget:self action:@selector(onContact) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnAds addTarget:self action:@selector(onAds) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }else if (indexPath.row == 4){
        BtnReportTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BtnReportTableViewCell"];
        [cell.btnReport addTarget:self action:@selector(onReport) forControlEvents:UIControlEventTouchUpInside];
        
        NSString* thumbLogin_already = [Util stringForKey:KEY_LOGIN];
        login_already = [Util stringForKey:KEY_LOGIN];
        if ([thumbLogin_already isEqualToString:@"1"] ||[thumbLogin_already isEqualToString:@"2"]) {
            cell.addEstimatePrice.hidden = false;
            cell.topConstraint.constant = 60;
            [cell.addEstimatePrice addTarget:self action:@selector(addEstimatePrice) forControlEvents:UIControlEventTouchUpInside];
            
        } else {
            cell.topConstraint.constant = 20;
            cell.addEstimatePrice.hidden = true;
        }
        
        return cell;
    }
    return nil;
}

-(void)addEstimatePrice {
    FieldAlertController *alert = [[FieldAlertController alloc] initWithNibName:@"FieldAlertController" bundle:nil];
    alert.delegate = self;
    alert.titleString = [@"Estimate Price"  localized];
    alert.textTitle = [NSString stringWithFormat:@"Enter %@ Here",[@"Estimate Price" localized]];
    alert.searchOption = @"ESTIMATE PRICE";
    alert.index = @"0";
    [alert presentInParentViewController:self];
}

-(void)didEnterValue:(NSString *)value forOption:(NSString *)option {
    NSLog(@"%@",value);
    if ([value isEqualToString:@""]) {
        [self.view makeToast:[@"Please enter price" localized] duration:2.0 position:CSToastPositionCenter];
        return;
    }
    
    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
    paramDic[@"userId"] = gUser.usId;
    paramDic[@"id"] = _objAds.adsId;
    paramDic[@"price"] = value;
    NSLog(@"%@", paramDic);
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [ModelManager addEstimation:paramDic withSuccess:^(NSDictionary *successDic) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    } failure:^(NSString *err) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.view makeToast:err duration:2.0 position:CSToastPositionCenter];
        
    }];
}

-(void)onReport{
    ContactUsVC *contacVC = [[ContactUsVC alloc]initWithNibName:@"ContactUsVC" bundle:nil];
    contacVC.strTitle = CONTACT_REPORT_ADS;
    //contacVC.strContent = [NSString stringWithFormat:@"%@%@%@%@",@"Ads ID : ",_objAds.adsId,@", name: ",_objAds.adsTitle];
    contacVC.strContent = [NSString stringWithFormat:@"%@%@%@%@",@"Seller ID : ",user.usId,@", name: ",user.usName];
    [self.navigationController pushViewController:contacVC animated:YES];
}

-(void)onAds{

    AllAdsViewController *allAdsVC = [[AllAdsViewController alloc] initWithNibName:@"AllAdsViewController" bundle:nil];
    allAdsVC.sellerId = _objAds.adsAccountId;
    allAdsVC.lblTitle.text = [NSString stringWithFormat:@"%@'s Ads",_objAds.adsAccountName];
    [self.navigationController pushViewController:allAdsVC animated:YES];

}

-(void)onContact{
    ContactViewController *contactVC = [[ContactViewController alloc]initWithNibName:@"ContactViewController" bundle:nil];
    contactVC.adsId = _objAds.adsId;
    if (user) {
        contactVC.seller = user;
    }else{
        contactVC.sellerId = _objAds.adsAccountId;
    }
    [self.navigationController pushViewController:contactVC animated:YES];
}
-(void)viewWillDisappear:(BOOL)animated{
    if (![login_already isEqualToString:@"0"]) {
    NSString *type =@"";
    if (_btnFavourite.selected) {
        type =@"1";
    }else{
    type =@"2";
    }
[ModelManager bookmarkClickWithUserId:gUser.usId adsId:_objAds.adsId type:type withSuccess:^(NSString *strSuccess) {
    
} failure:^(NSString *err) {
    
}];
    }
}

@end
