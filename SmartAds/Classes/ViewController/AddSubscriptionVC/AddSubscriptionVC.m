//
//  SearchOptionsViewController.m
//  Real Estate
//
//  Created by Hicom on 2/20/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import "AddSubscriptionVC.h"
#import "SubcriberViewController.h"
#import "UIView+Toast.h"
#import "BSDropDown.h"

@interface AddSubscriptionVC ()<BSDropDownDelegate>
{
    NSString *itemName;
    NSInteger itemDays;
    NSInteger itemPrice;
    NSInteger itemDuration;
    DLRadioButton* selectedButton;
    NSMutableArray *arrAds;
    int page,start_index;
    NSString *sortBy,*sortType;
    BSDropDown *ddView;
}

@property (strong, nonatomic) IBOutlet UILabel *lblheader;
@property (strong, nonatomic) IBOutlet DLRadioButton *btnOneDay;
@property (strong, nonatomic) IBOutlet DLRadioButton *btnThreedays;
@property (strong, nonatomic) IBOutlet DLRadioButton *btnSevenDays;
@property (strong, nonatomic) IBOutlet DLRadioButton *btnOneMonth;
@property (strong, nonatomic) IBOutlet UIButton *revealBtn;
@property (strong, nonatomic) IBOutlet UIButton *selectAds;
@property (strong, nonatomic) IBOutlet UIView *dropDownView;


@end

@implementation AddSubscriptionVC

- (void)viewDidLoad {
    [self decorateUI];
}

-(void)decorateUI{
    [_selectAds setTitle:_adsTitle forState:UIControlStateNormal];
    arrAds = [[NSMutableArray alloc]init];
    start_index = 1;
    sortBy = SORT_BY_ALL;
    sortType = SORT_TYPE_NORMAL;
    [self setRevealBtn];
    [self getData];
    // Set up payPalConfig
    _payPalConfig = [[PayPalConfiguration alloc] init];
    _payPalConfig.acceptCreditCards = YES;
    _payPalConfig.merchantName = @"سيارتك";
    _payPalConfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/privacy-full"];
    _payPalConfig.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];
    self.environment = PayPalEnvironmentSandbox;
    
    [self.btnOneDay setTitle:[@"One Day" localized] forState:UIControlStateNormal];
    [self.btnThreedays setTitle:[@"Three Days" localized] forState:UIControlStateNormal];
    [self.btnSevenDays setTitle:[@"Seven Days" localized] forState:UIControlStateNormal];
    [self.btnOneMonth setTitle:[@"One Month" localized] forState:UIControlStateNormal];
    
    for (DLRadioButton *button in _ButtonArray){
        button.layer.borderWidth = 2.0;
        button.layer.cornerRadius = 8.0;
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        button.iconSquare = YES;
        NSArray *value = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
        if([value.firstObject isEqualToString:@"en"]) {
            [button setImageEdgeInsets:UIEdgeInsetsMake(0, 20, 0, 0)];
        } else {
            [button setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 20)];
        }
        NSLog(@"%ld",[[NSUserDefaults standardUserDefaults] integerForKey:@"SelectedButton"]);
        
        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"SelectedButton"] == 0){
            [self.btnSubmit setTitle:[@"SUBMIT" localized] forState:UIControlStateNormal];
            button.enabled = YES;
            button.layer.borderColor = [UIColor colorWithRed:120/255.00 green:120/255.00 blue:120/255.00 alpha:1.0].CGColor;
            [button setTitleColor:[UIColor colorWithRed:120/255.00 green:120/255.00 blue:120/255.00 alpha:1.0] forState:UIControlStateNormal];
            button.selected = NO;
            
        }else{
            [self.btnSubmit setTitle:[@"CHANGE PACKAGE" localized] forState:UIControlStateNormal];
            if (button.tag == [[NSUserDefaults standardUserDefaults] integerForKey:@"SelectedButton"]){
                button.enabled = YES;
                button.layer.borderColor = [UIColor colorWithRed:76/255.00 green:175/255.00 blue:80/255.00 alpha:1.0].CGColor;
                [button setTitleColor:[UIColor colorWithRed:76/255.00 green:175/255.00 blue:80/255.00 alpha:1.0] forState:UIControlStateNormal];
                button.selected = YES;
                [button setTitle:[button.titleLabel.text stringByAppendingString:[@" (Current Package)" localized]]  forState:UIControlStateNormal];
                NSLog(@"%@",button.titleLabel.text);
            }else{
                button.enabled = NO;
                button.layer.borderColor = [UIColor colorWithRed:120/255.00 green:120/255.00 blue:120/255.00 alpha:1.0].CGColor;
                [button setTitleColor:[UIColor colorWithRed:120/255.00 green:120/255.00 blue:120/255.00 alpha:1.0] forState:UIControlStateNormal];
                button.selected = NO;
                [button setTitle:button.titleLabel.text  forState:UIControlStateNormal];
            }
        }
    }
}

-(void)setRevealBtn{
    SWRevealViewController *revealController = self.revealViewController;
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    [_revealBtn addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)getData{
    
    ///YES DATA
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [ModelManager getAdsByUserId:gUser.usId andPage:[NSString stringWithFormat:@"%d",start_index] sortType:sortType sortBy:sortBy withSuccess:^(NSDictionary *dicReturn) {
        if (start_index == 1) {
            [arrAds removeAllObjects];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            page = [dicReturn[@"allpage"] intValue];
            [arrAds addObjectsFromArray:dicReturn[@"arrAds"]];
            for (int i = 0; i < arrAds.count; i++)
            {
                Ads *est = [arrAds objectAtIndex:i];
                NSLog(@"%@",est.adsTitle);
                ddView = [[BSDropDown alloc] initWithWidth:_dropDownView.frame.size.width withHeightForEachRow:50 originPoint:CGPointMake(_dropDownView.frame.origin.x, _dropDownView.frame.origin.y +  110) withOptions:@[est.adsTitle] withOptionsIndex:@[est.adsId]];
                ddView.delegate = self;
                ddView.dropDownBGColor = [UIColor groupTableViewBackgroundColor];
                ddView.dropDownTextColor = [UIColor blackColor];
                ddView.dropDownFont = [UIFont systemFontOfSize:13];
             }
            [self finishLoading];
        });
        
    } failure:^(NSString *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self finishLoading];
        });
    }];
}

-(void)finishLoading{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
- (void)viewDidLayoutSubviews{
  
    //_selectAds
    _selectAds.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_selectAds setTitleColor:[UIColor colorWithRed:120/255.00 green:120/255.00 blue:120/255.00 alpha:1.0] forState:UIControlStateNormal];
    _selectAds.layer.borderColor = [UIColor colorWithRed:120/255.00 green:120/255.00 blue:120/255.00 alpha:1.0].CGColor;
    _selectAds.layer.borderWidth = 2.0;
    _selectAds.layer.cornerRadius = 8.0;
   
    NSArray *value = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    if([value.firstObject isEqualToString:@"en"]) {
        [_selectAds setTitleEdgeInsets:UIEdgeInsetsMake(0, 20, 0, 0)];
        [_selectAds setImageEdgeInsets:UIEdgeInsetsMake(0,  _selectAds.frame.size.width - 30, 0, 0)];
    } else {
        [_selectAds setTitleEdgeInsets:UIEdgeInsetsMake(0, _selectAds.frame.size.width - 30, 0, 0)];
         [_selectAds setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 20)];
        
    }

    
    _btnSubmit.layer.cornerRadius = 8.0;
     self.lblheader.text = [@"Promotion Packages" localized];

}

#pragma mark - DropDown Delegate
-(void)dropDownView:(UIView*)ddView AtIndex:(NSInteger)selectedIndex text : (NSString *)txtTitle textID : (NSString *)txtID{
   
    [_selectAds setTitle:txtTitle forState:UIControlStateNormal];
    _adsId = txtID;
    [[NSUserDefaults standardUserDefaults] setObject:_adsId forKey:@"SelectedID"];
    [[NSUserDefaults standardUserDefaults] setObject:txtTitle forKey:@"SelectedItem"];
}

- (IBAction)onTapSubmit:(UIButton *)sender {
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"SelectedButton"] == 0){
  
        [self.view makeToast:@"Please select package first"];
        
    }else{
    if ([sender.titleLabel.text isEqualToString:[@"CHANGE PACKAGE" localized]]){
        for (DLRadioButton *button in _ButtonArray) {
        button.enabled = YES;
        [button setTitle:[button.titleLabel.text stringByReplacingOccurrencesOfString:[@" (Current Package)" localized] withString:@""]  forState:UIControlStateNormal];
        [self.btnSubmit setTitle:[@"SUBMIT" localized] forState:UIControlStateNormal];
        button.layer.borderColor = [UIColor colorWithRed:120/255.00 green:120/255.00 blue:120/255.00 alpha:1.0].CGColor;
        [button setTitleColor:[UIColor colorWithRed:120/255.00 green:120/255.00 blue:120/255.00 alpha:1.0] forState:UIControlStateNormal];
        button.selected = NO;
             
         }
    }else{
        [self requirePaypalPayment];
    }
    }
    
}

- (IBAction)showDropDown:(id)sender {
    [ddView addAsSubviewTo:self.view];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    // Preconnect to PayPal early
    [self setPayPalEnvironment:self.environment];

}



- (void)setPayPalEnvironment:(NSString *)environment {
    self.environment = environment;
    [PayPalMobile preconnectWithEnvironment:environment];
}

- (IBAction)logSelectedButton:(DLRadioButton *)radioButton {
    if ([_adsId isEqualToString:@""]){
        for (DLRadioButton *button in _ButtonArray) {
            button.selected = NO;
        }
        [self.view makeToast:@"Please select Advertisement first"];
         return;
   
    }else{
   
        for (DLRadioButton *button in _ButtonArray) {
         button.layer.borderWidth = 2.0;
         button.layer.cornerRadius = 8.0;
            if (button == radioButton){
                button.selected = YES;
                button.layer.borderColor = [UIColor colorWithRed:76/255.00 green:175/255.00 blue:80/255.00 alpha:1.0].CGColor;
                [button setTitleColor:[UIColor colorWithRed:76/255.00 green:175/255.00 blue:80/255.00 alpha:1.0] forState:UIControlStateNormal];
                itemName = button.titleLabel.text;
                if ([button.titleLabel.text isEqualToString: [@"One Day" localized]]){
                    itemDays = 1;
                    itemPrice = 2;
                    itemDuration = 1;
                    
                }else if ([button.titleLabel.text isEqualToString: [@"Three Days" localized]]){
                    itemDays = 3;
                     itemPrice = 2;
                     itemDuration = 2;
                }else if ([button.titleLabel.text isEqualToString: [@"Seven Days" localized]]){
                    itemDays = 7;
                     itemPrice = 2;
                     itemDuration = 3;
                }else if ([button.titleLabel.text isEqualToString: [@"One Month" localized]]){
                    itemDays = 30;
                     itemPrice = 2;
                     itemDuration = 4;
                }
                selectedButton = button;
                [[NSUserDefaults standardUserDefaults] setInteger:selectedButton.tag forKey:@"SelectedButton"];
            }else{
                button.selected = NO;
                button.layer.borderColor = [UIColor colorWithRed:120/255.00 green:120/255.00 blue:120/255.00 alpha:1.0].CGColor;
                [button setTitleColor:[UIColor colorWithRed:120/255.00 green:120/255.00 blue:120/255.00 alpha:1.0] forState:UIControlStateNormal];

            }
    }
    
    }
}
    
    - (void)requirePaypalPayment {
        self.completedPayment = nil;
        PayPalItem *items = [PayPalItem itemWithName:itemName
                                        withQuantity:1
                                           withPrice:[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%ld",(long)itemPrice]]
                                        withCurrency:@"USD"
                                             withSku:@""];

        PayPalPayment *payment = [[PayPalPayment alloc] init];
        payment.amount = [[NSDecimalNumber alloc] initWithString:[NSString stringWithFormat:@"%ld",(long)itemPrice]];;
        payment.currencyCode = @"USD";
        payment.shortDescription = @"Payment for Order in App";
        payment.items = @[items];  // if not including multiple items, then leave payment.items as nil
        
        if (!payment.processable) {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
        }
        
        PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                                                    configuration:self.payPalConfig
                                                                                                         delegate:self];
        [self presentViewController:paymentViewController animated:YES completion:nil];
    }
    
#pragma mark - Proof of payment validation
    
    - (void)sendCompletedPaymentToServer:(PayPalPayment *)completedPayment {
        // TODO: Send completedPayment.confirmation to server
        NSLog(@"Here is your proof of payment:\n\n%@\n\nSend this to your server for confirmation and fulfillment.", completedPayment.confirmation);
    }
    
#pragma mark - PayPalPaymentDelegate methods
    
    - (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment {
        NSLog(@"PayPal Payment Success!");
        self.completedPayment = completedPayment;
        [self sendCompletedPaymentToServer:completedPayment]; // Payment was processed successfully; send to server for verification and fulfillment
        [self dismissViewControllerAnimated:YES completion:nil];
        [self sendOrderToServer];
    }
    - (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
        NSLog(@"PayPal Payment Canceled");
        self.completedPayment = nil;
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    -(void)sendOrderToServer{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSArray* keyArray = [NSArray arrayWithObjects:@"adsId",@"paidDate",@"endDate",@"value",@"payment",@"duration",nil];

        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy/MM/dd";
        NSString *startText = [formatter stringFromDate:[NSDate date]];
        NSDate *startDate = [formatter dateFromString:startText];
       
        NSDate *endDate = [startDate dateByAddingTimeInterval:60*60*24*itemDays];
        //duration = [NSString stringWithFormat:@"%ld",(24*60*60 + itemDays) * 1000]
        
        NSString *resultStart = [NSString stringWithFormat:@"%f",startDate.timeIntervalSince1970];
        NSString *resultEnd = [NSString stringWithFormat:@"%f",endDate.timeIntervalSince1970];

        NSArray* objectArray = [NSArray arrayWithObjects:_adsId,resultStart,resultEnd,[NSString stringWithFormat:@"%ld",(long)itemPrice],@"true",[NSString stringWithFormat:@"%ld",(long)itemDuration], nil];
        
        NSDictionary *dic = [NSDictionary dictionaryWithObjects:objectArray forKeys:keyArray];

        [ModelManager addSubCriptionWithParam:dic withSuccess:^(NSString *strSuccess) {
            NSLog(@"%@", strSuccess);
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            for (DLRadioButton *button in _ButtonArray) {
                if (button == selectedButton){
                    button.enabled = YES;
                    [button setTitle:[button.titleLabel.text stringByAppendingString:[@" (Current Package)" localized]] forState:UIControlStateNormal];
                    
                }else{
                    button.enabled = NO;
                }
                NSLog(@"%ld",(long)selectedButton.tag);
                [[NSUserDefaults standardUserDefaults] setInteger:selectedButton.tag forKey:@"SelectedButton"];
                [self.btnSubmit setTitle:[@"CHANGE PACKAGE" localized] forState:UIControlStateNormal];
            }
            
        } failure:^(NSString *err) {
            NSLog(@"%@", err);
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
           // [self.view makeToast:err duration:2.0 position:CSToastPositionCenter];
           
        }];

}





@end

