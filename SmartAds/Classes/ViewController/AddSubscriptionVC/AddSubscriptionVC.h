

#import <UIKit/UIKit.h>
#import "DLRadioButton.h"
#import "PayPalMobile.h"

@interface AddSubscriptionVC :  UIViewController<PayPalPaymentDelegate>
@property (nonatomic, strong) IBOutletCollection(DLRadioButton) NSArray *ButtonArray;
@property (nonatomic, strong) IBOutletCollection(UIView) NSArray *DaysView;
@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;
@property (strong, nonatomic)  PayPalConfiguration* payPalConfig;
@property(nonatomic, strong, readwrite) PayPalPayment *completedPayment;
@property(nonatomic, assign, readwrite) BOOL acceptCreditCards;
@property(nonatomic, strong, readwrite) NSString *environment;
@property(nonatomic, strong, readwrite) NSString *adsId;
@property(nonatomic, strong, readwrite) NSString *adsTitle;

@end
