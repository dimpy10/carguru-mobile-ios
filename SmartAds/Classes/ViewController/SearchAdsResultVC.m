//

//  Real Estate
//
//  Created by Hicom on 3/5/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import "SearchAdsResultVC.h"
#import "UITableView+DragLoad.h"
#import "AllAdsCell.h"
#import "NothingCell.h"
#import "DetailAdsViewController.h"
#import "UIScrollView+BottomRefreshControl.h"
#import "UIView+Toast.h"
#import "UIImageView+WebCache.h"
#import "SortViewController.h"
#import "HomeViewCell.h"

@interface SearchAdsResultVC () <UITableViewDragLoadDelegate, UITableViewDataSource, UITableViewDelegate, SortViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *topNaviView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end

@implementation SearchAdsResultVC{
    NSMutableArray *arrAds;
    BOOL isLoading;
    int page,start_index;
    NSArray *arrSortType;
    NSString *sortBy,*sortType;
    
}

-(void)initSortFunction{
    arrSortType = @[[@"Sort by Name asc" localized] ,
                    [@"Sort by Name desc" localized],
                    [@"Sort by Date asc" localized],
                    [@"Sort by Date desc" localized],
                    [@"Number of Viewed acs" localized],
                    [@"Number of Viewed desc"localized]];
    sortBy = SORT_BY_ALL;
    sortType = SORT_TYPE_NORMAL;
}

-(void)initData{
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44.0;
    self.view.backgroundColor = COLOR_BACKGROUD_VIEW;
    _topNaviView.backgroundColor = COLOR_DARK_PR_MARY;
    page =1;
    start_index =1;
    arrAds = [[NSMutableArray alloc]init];
    sortBy =@"0";
    sortType=@"0";
    _viewTblView.layer.masksToBounds = YES;
    _tableView.layer.masksToBounds = NO;
    
}

-(void)initLoadmoreAndPullToRefresh{
    isLoading = false;
    _topRefreshTable = [UIRefreshControl new];
    _topRefreshTable.attributedTitle = [[NSAttributedString alloc] initWithString:[@"Pull down to reload!" localized] attributes:@{NSForegroundColorAttributeName:COLOR_PRIMARY_DEFAULT }];
    _topRefreshTable.tintColor = COLOR_PRIMARY_DEFAULT;
    [_topRefreshTable addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    
    [_tableView addSubview:_topRefreshTable];
    
    UIRefreshControl *refreshControl = [UIRefreshControl new];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:[@"Pull up to load more!" localized] attributes:@{NSForegroundColorAttributeName:COLOR_PRIMARY_DEFAULT}];
    // refreshControl.triggerVerticalOffset = 60;
    [refreshControl addTarget:self action:@selector(loadMore) forControlEvents:UIControlEventValueChanged];
    refreshControl.tintColor = COLOR_PRIMARY_DEFAULT;
    self.tableView.bottomRefreshControl = refreshControl;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.lblTitle.text = [@"Search Result" localized];
    if (_titleText) {
        _lblTitle.text = _titleText;
    }
    [self initData];
    [self initSortFunction];
    [self initLoadmoreAndPullToRefresh];
    
    [self performSelectorInBackground:@selector(refreshData) withObject:nil];
}

-(void)refreshData{
    if (isLoading) {
        return;
    }else
        start_index = 1;
    isLoading = true;
    [self performSelectorInBackground:@selector(getData) withObject:nil];
}
-(void)loadMore{
    if (isLoading) {
        return;
    }else{
        isLoading = true;
    }
    if (start_index==page) {
        [self.view makeToast:[@"No more products" localized] duration:2.0 position:CSToastPositionCenter];
        [self finishLoading];
        return;
    }
    start_index+=1;
    [self performSelectorInBackground:@selector(getData) withObject:nil];
}

-(void)getData{
    _paramDic[@"sortBy"] = sortBy;
    _paramDic[@"sortType"]= sortType;
    _paramDic[@"page"] = [NSString stringWithFormat:@"%d",start_index];
    [MBProgressHUD showHUDAddedTo:self.viewTblView animated:YES];
    [ModelManager searchAdsWithParam:_paramDic withSuccess:^(NSDictionary *successDic) {
        if (start_index == 1) {
            [arrAds removeAllObjects];
        }
        page = [successDic[@"allpage"] intValue];
        arrAds = [[arrAds.copy arrayByAddingObjectsFromArray:successDic[@"arrAds"]] mutableCopy];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self finishLoading];
            [_tableView reloadData];
        });
    } failure:^(NSString *err) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [_topRefreshTable endRefreshing];
            [self.tableView.bottomRefreshControl endRefreshing];
            isLoading = false;
        });
        [self.view makeToast:err duration:2.0 position:CSToastPositionCenter];
        
    }];
    
}
-(void)finishLoading{
    [MBProgressHUD hideAllHUDsForView:self.viewTblView animated:YES];
    
    [_topRefreshTable endRefreshing];
    [self.tableView.bottomRefreshControl endRefreshing];
    isLoading = false;
    // [_tableView reloadData];
}


#pragma mark UITABALEVIEW DATASOURCE

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return arrAds.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HomeViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HomeViewCell"];
    
    if (!cell) {
        
        cell = [[[NSBundle mainBundle] loadNibNamed:@"HomeViewCell" owner:nil options:nil] objectAtIndex:0];
    }
    
    
    Ads *est = [arrAds objectAtIndex:indexPath.row];
    cell.lblTitle.text = est.adsTitle;
    [cell.imageAds setImageWithURL:[NSURL URLWithString:est.adsImage] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        
    }];
    //cell.lblStatusValue.hidden = YES;
    //cell.lblStatus.hidden = YES;
    //cell.imageView3.imageURL = [NSURL URLWithString:est.image];
    if ([est.adsPrice isEqualToString:@"3"]) {
        cell.lblPrice.text = [NSString stringWithFormat:@"$%@/%@",est.adsPriceValue,est.adsPriceUnit];
    }
    if ([est.adsPrice isEqualToString:@"1"]) {
        cell.lblPrice.text = [NSString stringWithFormat:[@"Free" localized]];
    }
    
    if ([est.adsPrice isEqualToString:@"2"]) {
        cell.lblPrice.text = [@"Negotiate" localized];
    }
    cell.lblAvaiable.hidden = NO;
    [cell.lblAvaiable setTransform:CGAffineTransformMakeRotation(-M_PI / 4)];
    if ([est.adsIsAvailable isEqualToString:@"1"]) {
        cell.lblAvaiable.hidden = YES;
    } else if ([est.adsIsAvailable isEqualToString:@"0"])
    {
        if([est.adsForRent isEqualToString:@"1"]){
            cell.lblAvaiable.text =  [@"RENTED" localized];
        }else if([est.adsForSale isEqualToString:@"1"]){
            cell.lblAvaiable.text =  [@"SOLD" localized];
        }
    }
    
    [cell.imageAds setContentMode:UIViewContentModeScaleAspectFill];
    cell.imageAds.clipsToBounds = YES;
    if ([est.adsForRent isEqualToString:@"1"]) {
        cell.lblRentOrSale.text = [@"For rent" localized];
    }
    if ([est.adsForSale isEqualToString:@"1"]) {
        cell.lblRentOrSale.text = [@"For sale" localized];
    }
    if (est.adsCategory) {
        cell.lblCategory.text = est.adsCategory;
    }else{
        cell.lblCategory.text = [@"All Categories" localized];
    }
    
    if (est.adsSub) {
        cell.lblSubCat.text = est.adsSub;
    }else{
        cell.lblSubCat.text = [@"All Sub Categories" localized];
    }
    
    if (est.adsCity) {
        cell.lblCity.text = est.adsCity;
    }else{
        cell.lblCity.text = [@"All cities" localized];
    }
    
    cell.lblPosted.text = [est.adsDateCreated substringWithRange:NSMakeRange(0, 10)];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if([est.adsIsFeatured isEqualToString:@"1"]){
        cell.isFeatured.hidden = NO;
    }else{
        cell.isFeatured.hidden = YES;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Ads *est = [arrAds objectAtIndex:indexPath.row];
    DetailAdsViewController *VC = [[DetailAdsViewController alloc]initWithNibName:@"DetailAdsViewController" bundle:nil];
    VC.objAds = est;
    [self.navigationController pushViewController:VC animated:YES];
}


-(void)readMore:(UIButton*)button
{
    int index =0;
    if (button.tag) {
        index = (int)button.tag;
    }
    Ads *est = [arrAds objectAtIndex:index];
    DetailAdsViewController *VC = [[DetailAdsViewController alloc]initWithNibName:@"DetailAdsViewController" bundle:nil];
    VC.objAds = est;
    [self.navigationController pushViewController:VC animated:YES];
}

- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onSort:(id)sender {
    SortViewController *sortVC = [[SortViewController alloc]initWithNibName:@"SortViewController" bundle:nil];
    sortVC.delegate = self;
    sortVC.arrDataSource = arrSortType;
    [sortVC presentInParentViewController:self];
}

-(void)sortViewDidSelectedItemAtIndex:(int)rowSelected{
    switch (rowSelected) {
        case 0:
            sortBy = SORT_BY_NAME;
            sortType = SORT_TYPE_ASC;
            break;
        case 1:
            sortBy = SORT_BY_NAME;
            sortType = SORT_TYPE_DESC;
            break;
        case 2:
            sortBy = SORT_BY_POSTED_DATE;
            sortType = SORT_TYPE_ASC;
            break;
        case 3:
            sortBy = SORT_BY_POSTED_DATE;
            sortType = SORT_TYPE_DESC;
            break;
        case 4:
            sortBy = SORT_BY_VIEW;
            sortType = SORT_TYPE_ASC;
            break;
        case 5:
            sortBy = SORT_BY_VIEW;
            sortType = SORT_TYPE_DESC;
            break;
            
        default:
            break;
    }
    start_index =1;
    isLoading =YES;
    [self getData];
    
    
}

@end
