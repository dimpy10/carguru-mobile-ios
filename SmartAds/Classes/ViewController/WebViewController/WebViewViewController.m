//
//  WebViewViewController.m
//  Real Ads
//
//  Created by De Papier on 4/13/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import "WebViewViewController.h"

@interface WebViewViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imgNaviBg;

@end

@implementation WebViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _imgNaviBg.backgroundColor = COLOR_DARK_PR_MARY;
    [self layoutView];
    _scrollLabel.text = _newss.newsTitle;
    // Do any additional setup after loading the view from its nib.
   // [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://vnre.reic.vn/2012/09/its-great-time-to-invest-in-hoa-lac-hi.html"]]];
    NSURL *url = [NSURL URLWithString:[_newss.newsUrl stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)layoutView
{
   // self.scrollLabel.text = _newss.title;
    self.scrollLabel.textColor = [UIColor whiteColor];
    self.scrollLabel.font = [UIFont fontWithName:@"OpenSans-Semibold" size:22];
}

- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
