//
//  WebViewViewController.h
//  Real Ads
//
//  Created by De Papier on 4/13/15.
//  Copyright (c) 2015 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBAutoScrollLabel.h"
#import "News.h"
@interface WebViewViewController : UIViewController
@property (strong, nonatomic) IBOutlet CBAutoScrollLabel *scrollLabel;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
- (IBAction)onBack:(id)sender;
@property (strong, nonatomic) News *newss;

@end
