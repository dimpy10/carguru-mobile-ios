//
//  DetailAdsCollectionCellImg.m
//  Real Estate
//
//  Created by Hicom on 3/21/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import "DetailAdsCollectionCellImg.h"

@implementation DetailAdsCollectionCellImg

- (void)awakeFromNib {
    // Initialization code
     _imgInCell.contentMode = UIViewContentModeScaleAspectFill;
    _imgInCell.clipsToBounds = YES;
}

@end
