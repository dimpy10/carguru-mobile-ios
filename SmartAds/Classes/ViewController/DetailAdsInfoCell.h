//
//  DetailAdsInfoCell.h
//  Real Estate
//
//  Created by Hicom on 3/5/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RTLabel.h"

@interface DetailAdsInfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet RTLabel *detailLabel1;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel2;

@end
