//
//  SearchOptionsViewController.h
//  Real Estate
//
//  Created by Hicom on 2/20/16.
//  Copyright © 2016 Hicom Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Macros.h"

@protocol CountryViewControllerDelegate <NSObject>

-(void)CountryViewControllerDidSelectedItem:(int)rowSelected;

@end

@interface CountryViewController : UIViewController

@property (nonatomic, weak) id<CountryViewControllerDelegate> delegate;
@property (nonatomic, strong) NSArray *arrayItems;

- (void)presentInParentViewController:(UIViewController *)parentViewController;
- (void)dismissFromParentViewController;


@end
