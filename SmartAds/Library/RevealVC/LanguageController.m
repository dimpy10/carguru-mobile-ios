//
//  LanguageController.m
//  SmartAds
//
//  Created by Rakesh Kumar on 02/07/18.
//  Copyright © 2018 Mr Lemon. All rights reserved.
//

#import "LanguageController.h"
#import "UIView+Toast.h"

@interface LanguageController ()
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UIButton *englishButton;
@property (weak, nonatomic) IBOutlet UIButton *arabicButton;

@end

@implementation LanguageController

- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeDetail)];
    tap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tap];
    
    self.headerLabel.text = [@"Select Language" localized];
    
    NSArray *value = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    if([value.firstObject isEqualToString:@"en"]) {
        self.englishButton.selected = YES;
    } else {
        self.arabicButton.selected = YES;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)englishSelectionAction:(UIButton *)sender {
    sender.selected = YES;
    self.arabicButton.selected = NO;
    NSArray *array = [NSArray arrayWithObject:@"en"];
    [[NSUserDefaults standardUserDefaults] setObject:array forKey:@"AppleLanguages"];
    [self.view makeToast:@"Language changes will take effect after restarting the application" duration:1.5 position:CSToastPositionCenter];
    //[self closeDetail];
}


- (IBAction)ArabicSelectionAction:(UIButton *)sender {
    sender.selected = YES;
    self.englishButton.selected = NO;
    NSArray *array = [NSArray arrayWithObject:@"ar"];
    [[NSUserDefaults standardUserDefaults] setObject:array forKey:@"AppleLanguages"];
        [self.view makeToast:@"Language changes will take effect after restarting the application" duration:1.5 position:CSToastPositionCenter];
   // [self closeDetail];
}

-(void)presentInParentViewController:(UIViewController *)parentViewController{
    
    self.view.frame = parentViewController.view.bounds;
    [parentViewController.view addSubview:self.view];
    [parentViewController addChildViewController:self];
    
}

-(void)dismissFromParentViewController{
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    [self didMoveToParentViewController:self.parentViewController];
}

-(void)closeDetail{
    [self dismissFromParentViewController];
}

@end
