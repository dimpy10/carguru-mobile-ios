//
//  LanguageController.h
//  SmartAds
//
//  Created by Rakesh Kumar on 02/07/18.
//  Copyright © 2018 Mr Lemon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LanguageController : UIViewController
- (void)presentInParentViewController:(UIViewController *)parentViewController;
- (void)dismissFromParentViewController;
@end
